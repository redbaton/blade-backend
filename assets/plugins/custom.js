$(document).ready(function () {

  ;
  // (function ($) {
  //   // Make your content a heroe
  //   $.fn.transformHeroes = function () {
  //     //Variables
  //     var perspective = '1000px',
  //       delta = 50,
  //       width = this.width(),
  //       height = this.height(),
  //       midWidth = width / 2,
  //       midHeight = height / 2;
  //     //Events
  //     $('body').on({
  //       mousemove: function (e) {
  //         var pos = $(this).offset(),
  //           cursPosX = e.pageX - pos.left,
  //           cursPosY = e.pageY - pos.top,
  //           cursCenterX = midWidth - cursPosX,
  //           cursCenterY = midHeight - cursPosY;

  //         // $('.banner-img').css('transform', 'perspective(' + perspective + ') rotateX(' + (cursCenterY / delta) + 'deg) rotateY(' + -(cursCenterX / delta) + 'deg) rotateZ(0deg)');
  //         // $('.banner-img').removeClass('is-out');
  //       },
  //       mouseleave: function () {
  //         // $('.banner-img').addClass('is-out');
  //       }
  //     });
  //     //Return
  //     return this;
  //   };
  // }(jQuery));

  // // Set plugin on cards
  // $('.banner-img').transformHeroes();

  // $('.navbar li a').click(function(e) {

  //     $('.navbar li.active').removeClass('active');

  //     var $parent = $(this).parent();
  //     $parent.addClass('active');
  //     e.preventDefault();
  // });
  $(".dropdown-menu li").click(function () {
    var selText = $(this).text();
    $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
  });

  $('.navbar-nav>li>a').on('click', function () {
    $('.navbar-collapse').collapse('hide');
  });

});
// $(window).bind("load", function() {
//   $(".zoom").elevateZoom({
//     borderSize: 1,
//     lensFadeIn: 200,
//     cursor: 'crosshair',
//     zoomWindowFadeIn: 200,
//     loadingIcon: true,
//     zoomWindowOffety: -50,
//     zoomWindowOffetx: 50,
//     zoomWindowHeight: 530,
//     responsive: true
//   });
// });
function zoom() {

}

function fadeIn() {
  window.sr = new ScrollReveal();
  var nodeListOne = document.querySelectorAll('.fade-in');
  sr.reveal(nodeListOne, {
    duration: 1000,
    origin: 'bottom',
    easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
    scale: 0.9
  });

}

function fadeInUp() {
  window.sr = new ScrollReveal();
  var nodeListOne = document.querySelectorAll('.fade-in-up');
  sr.reveal(nodeListOne, {
    duration: 1000,
    origin: 'bottom',
    easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
    scale: 1
  }, 100);

}

function fullpageFun() {
  window.sr = new ScrollReveal();
  var nodeListOne = document.querySelectorAll('.fade-in-one');
  var nodeListTwo = document.querySelectorAll('.fade-in-two');
  var nodeListThree = document.querySelectorAll('.fade-in-three');
  $('#fullpage').fullpage({
    navigation: true,
    navigationPosition: 'right',
    parallax: false,
    parallaxOptions: {
      type: 'reveal',
      percentage: 62,
      property: 'translate'
    },
    verticalCentered: true,
    scrollBar: true,
    afterLoad: function (anchorLink, index) {
      if (index == 1) {
        sr.reveal(nodeListOne, {
          duration: 2000,
          origin: 'bottom',
          easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
          scale: 0.9
        }, 300);
      }

    },
    onLeave: function (index, nextIndex, direction) {
      if (index == 1 && direction == 'down') {
        sr.reveal(nodeListTwo, {
          duration: 2000,
          origin: 'bottom',
          easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
          scale: 0.9
        }, 300);
      }
      if (index == 2 && direction == 'down') {
        sr.reveal(nodeListThree, {
          duration: 2000,
          origin: 'bottom',
          easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
          scale: 0.9
        }, 300);
      }
    }

  });

  if (window.innerWidth <= 990) {
    $.fn.fullpage.setResponsive(true);
  }
}

function remFullpage() {
  if(typeof($.fn.fullpage.destroy) == 'function')
    $.fn.fullpage.destroy('all');
}

function pSlider() {
  var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    slidesPerView: 1,
    effect: 'fade'
  });
  var galleryThumbs = new Swiper('.gallery-thumbs', {

    centeredSlides: true,
    slidesPerView: 'auto',
    touchRatio: 0.2,
    slideToClickedSlide: true,
    navigation: {
      nextEl: '.button-next',
      prevEl: '.button-prev',
    },
  });
  galleryTop.controller.control = galleryThumbs;
  galleryThumbs.controller.control = galleryTop;
}

function collectionSlider() {
  var swiper = new Swiper('.collection.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 30,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      480: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10
      }
    }
  })
}

function rViewSlider() {
  var rView = new Swiper('.rViewSlider .swiper-container', {
    slidesPerView: 4,
    spaceBetween: 30,
    navigation: {
      nextEl: '.button-next',
      prevEl: '.button-prev',
    },
    breakpoints: {
      480: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      960: {
        slidesPerView: 3,
        spaceBetween: 10
      }
    }
  });
}

// function isTabKey(evt) {
//   evt = evt ? evt : window.event;
//   var charCode = (evt.which) ? evt.which : evt.keyCode;
//   // Added this line. 9 is the code for the tab key.
//   if (charCode === 9) return;
//   else return true;
// }
