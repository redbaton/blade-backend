/********** custom script **********/

$(document).ready(function(){

	// to hide pop-up msg after a while
	setTimeout(function(){
		$('.err').slideUp('slow');
	}, 2000);

$("ul.navigation li a").each(function(index) {
		if(window.location.href == $(this).attr('href')){
			$(this).closest('li').addClass("active");
		}
	});
	
});

// to read and show img after selecting
function readURL(clsname, input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#' + clsname).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

// to remove section of page contents
$(document).on('click', '.rmv-sec', function() {
	$(this).closest('form').remove();
});

// to append section in a page content body
function sectionAppend(input) {

	switch(input.value) {
		
		case '1':
			$('.contents').append($("#section1").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '2':
			$('.contents').append($("#section2").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '3':
			$('.contents').append($("#section3").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '4':
			$('.contents').append($("#section4").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '5':
			$('.contents').append($("#section5").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '6':
			$('.contents').append('<form id="section" class="'+input.value+'" style="padding: 2em; border: 1px solid; margin-bottom: 2em;"><div class="row"><label><strong>Contact Form</strong></label></div><div id="section-body" class="row" style="padding:2em;"><input style="margin-bottom: 10px;" type="text" placeholder="Heading" name="heading" class="form-control"><input style="margin-bottom: 10px;" type="text" placeholder="Sub-Heading" name="subheading" class="form-control"><input type="hidden" name="section_id" value="6"><select class="form-control" name="contact"><option value="1">Contact Form</option><option value="2">Contact Form (With Upload File Option)</option></select></div><span class="rmv-sec" style="cursor: pointer;">Remove</span></form>');
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '7':
			$('.contents').append('<form id="section" class="'+input.value+'" style="padding: 2em; border: 1px solid; margin-bottom: 2em;"><div class="row"><label><strong>Google Map</strong></label></div><div id="section-body" class="row" style="padding:2em;"><input type="hidden" name="section_id" value="7"><input style="margin-bottom: 10px;" type="text" placeholder="Lat" name="lat" class="form-control"><input style="margin-bottom: 10px;" type="text" placeholder="Long" name="long" class="form-control"></div><span class="rmv-sec" style="cursor: pointer;">Remove</span></form>');
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '8':
			$('.contents').append('<form id="section" class="'+input.value+'" style="padding: 2em; border: 1px solid; margin-bottom: 2em;"><div class="row"><label><strong>Contact Details</strong></label></div><div id="section-body" class="row" style="padding:2em;"><label>Contact Details Section Included.</label><input type="hidden" name="section_id" value="8"></div><span class="rmv-sec" style="cursor: pointer;">Remove</span></form>');
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '9':
			$('.contents').append($("#section9").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '10':
			$('.contents').append('<form id="section" class="'+input.value+'" style="padding: 2em; border: 1px solid; margin-bottom: 2em;"><div class="row"><label><strong>Articles</strong></label></div><div id="section-body" class="row" style="padding:2em;"><label>Article Section Included.</label><input type="hidden" name="section_id" value="10"></div><span class="rmv-sec" style="cursor: pointer;">Remove</span></form>');
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '11':
			$('.contents').append($("#section11").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '12':
			$('.contents').append($("#section12").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '13':
			$('.contents').append('<form id="section" class="'+input.value+'" style="padding: 2em; border: 1px solid; margin-bottom: 2em;"><div class="row"><label><strong>Team</strong></label></div><div id="section-body" class="row" style="padding:2em;"><label>Team Section Included.</label><input type="hidden" name="section_id" value="13"></div><span class="rmv-sec" style="cursor: pointer;">Remove</span></form>');
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '14':
			$('.contents').append($("#section14").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '15':
			$('.contents').append($("#section15").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;

		case '16':
			$('.contents').append($("#section16").html());
			$('select[name=sections] option[value=0]').prop('selected', true);
			break;
	}
}

/********** custom script **********/