<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		header("cache-Control: no-store, no-cache, must-revalidate");
		header("cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

		// $this->load->library('pagination');

		$cur_cls = $this->router->fetch_class();
		$cur_mth = $this->router->fetch_method();

		$exeption_url = $cur_cls=='admin' && $cur_mth=='index' || $cur_mth=='login'?false:true;
		$exeption_url = $cur_cls=='admin' && $cur_mth=='forgot_password'?false:$exeption_url;
		$exeption_url = $cur_cls=='admin' && $cur_mth=='reset_password'?false:$exeption_url;

		if($exeption_url){
			if(!$this->session->userdata("admin_user_id")){
				$this->session->set_userdata('admin_redirect_link',current_url());
				return redirect('admin');
			}
		}
	}
}

?>