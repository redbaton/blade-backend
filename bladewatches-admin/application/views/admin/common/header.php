<?php  
header( 'Content-Type: text/html; charset=utf-8' );  

$cls = ucfirst($this->router->fetch_class()) . ' | ';
$mth = ucfirst($this->router->fetch_method() != 'index' ? $this->router->fetch_method() . ' | ' : '');

?>
<!DOCTYPE html>
<html lang="en">

   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?= $cls . $mth . $this->admin_model->details('site_title'); ?></title>
      <link rel="icon" href="<?= base_url('assets/admin/images/' . $this->admin_model->details('favicon')); ?>">
      <!-- Global stylesheets -->
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
      <?= link_tag('assets/admin/css/icons/icomoon/styles.css'); ?>
      <?= link_tag('assets/admin/css/minified/bootstrap.min.css'); ?>
      <?= link_tag('assets/admin/css/minified/core.min.css'); ?>
      <?= link_tag('assets/admin/css/minified/components.min.css'); ?>
      <?= link_tag('assets/admin/css/minified/colors.min.css'); ?>
      <!-- Core JS files -->
      <script type="text/javascript" src="<?= base_url('assets/admin/js/plugins/loaders/pace.min.js'); ?>"></script>
      <script type="text/javascript" src="<?= base_url('assets/admin/js/core/libraries/jquery.min.js'); ?>"></script>
      <script type="text/javascript" src="<?= base_url('assets/admin/js/core/libraries/jquery-ui.js'); ?>"></script>
      <script type="text/javascript" src="<?= base_url('assets/admin/js/core/libraries/bootstrap.min.js'); ?>"></script>
      <script type="text/javascript" src="<?= base_url('assets/admin/js/plugins/loaders/blockui.min.js'); ?>"></script>
      <script type="text/javascript" src="<?= base_url('assets/admin/js/plugins/notifications/sweet_alert.min.js'); ?>"></script>
      <script type="text/javascript" src="<?= base_url('assets/admin/js/custom.js'); ?>"></script>
      <!-- theme JS files -->
      <script type="text/javascript" src="<?php echo base_url('assets/admin/js/core/app.js'); ?>"></script>
   </head>  

   <body class="<?php echo $this->session->userdata('collapse_side_bar'); ?>">

   <!-- Main navbar -->
   <div class="navbar navbar-default header-highlight">
      <div class="navbar-header">
         <a class="navbar-brand" href="<?= site_url(); ?>" style="color:#fff;"><?= $this->admin_model->details('site_title'); ?></a>

         <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
         </ul>
      </div>

      <div class="navbar-collapse collapse" id="navbar-mobile">
         <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
         </ul>

         <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
               <a class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?= base_url('assets/admin/images/'. $this->admin_model->details('favicon')); ?>" alt="<?= $this->admin_model->details('main_logo'); ?>">
                  <span><?= $this->admin_model->details('admin_fname').' '.$this->admin_model->details('admin_lname'); ?></span>
                  <i class="caret"></i>
               </a>

               <ul class="dropdown-menu dropdown-menu-right">
                  <li><a href="<?php echo site_url('admin/profile'); ?>"><i class="icon-user-plus"></i> My profile</a></li>
                  <li class="divider"></li>
                  <li><a href="<?php echo site_url('admin/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
               </ul>
            </li>
         </ul>
      </div>
   </div>
   <!-- /main navbar -->


   <!-- Page container -->
   <div class="page-container">

      <!-- Page content -->
      <div class="page-content">

         <!-- Main sidebar -->
         <div class="sidebar sidebar-main">
            <div class="sidebar-content">

               <div class="sidebar-user">
                  <div class="category-content" style="background-color:#202629;">
                     <div class="media">
                        <div class="media-left"><img src="<?= base_url('assets/admin/images/'. $this->admin_model->details('main_logo')); ?>" style="width:80px;" alt="<?= $this->admin_model->details('main_logo'); ?>"></div>
                        <div class="media-body">
                           <span class="media-heading text-semibold"><?= $this->admin_model->details('admin_fname').' '.$this->admin_model->details('admin_lname'); ?></span>
                           <div class="text-size-mini text-muted">Administrator</div>
                        </div>
                     </div>
                  </div>
               </div>


               <div class="sidebar-category sidebar-category-visible">
                  <div class="category-content no-padding">
                     <ul class="navigation navigation-main navigation-accordion">
                        <li><a href="<?php echo site_url('popup'); ?>"><i class="icon-folder3"></i> Pop Up Management</a></li>
                        <li><a href="<?php echo site_url('menu'); ?>"><i class="icon-folder3"></i> Menu Management</a></li>
                        <li>
                           <a href="#"><i class="icon-stack2"></i> <span>Enquiries</span></a>
                           <ul>
                              <li><a href="<?php echo site_url('enquiries/offers'); ?>"><i class="icon-folder3"></i> Latest Offers</a></li>
                              <li><a href="<?php echo site_url('enquiries/newsletter'); ?>"><i class="icon-folder3"></i> Newsletter</a></li>
                              <li><a href="<?php echo site_url('enquiries/enquiry'); ?>"><i class="icon-folder3"></i> Contact Us</a></li>
                           </ul>
                        </li>
                        <li>
                           <a href="#"><i class="icon-stack2"></i> <span>Products</span></a>
                           <ul>
                              <li><a href="<?php echo site_url('categories'); ?>"><i class="icon-folder3"></i> Categories</a></li>
                              <li><a href="<?php echo site_url('products'); ?>"><i class="icon-folder3"></i> All Products</a></li>
                           </ul>
                        </li>
                        <li>
                           <a href="#"><i class="icon-stack2"></i> <span>Pages</span></a>
                           <ul>
                              <li><a href="<?php echo site_url('pages/home'); ?>"><i class="icon-folder3"></i> Home</a></li>
                              <li><a href="<?php echo site_url('pages/about'); ?>"><i class="icon-folder3"></i> About Us</a></li>
                              <li><a href="<?php echo site_url('pages/contact'); ?>"><i class="icon-folder3"></i> Contact Us</a></li>
                           </ul>
                        </li>
                        <li><a href="<?php echo site_url('socialfeed'); ?>"><i class="icon-folder3"></i> Social Feed</a></li>
                        <li><a href="<?php echo site_url('timeline'); ?>"><i class="icon-folder3"></i> Timeline</a></li>
                        <li><a href="<?php echo site_url('stores'); ?>"><i class="icon-folder3"></i> Stores / Services</a></li>
                        <li><a href="<?php echo site_url('admin/logout'); ?>"><i class="icon-switch2"></i> <span>Logout</span></a></li>
                     </ul>
                  </div>
               </div>

            </div>
         </div>
         <!-- /main sidebar -->

         <!-- Main content -->
         <div class="content-wrapper">

            