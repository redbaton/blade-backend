<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= ' Admin Login - ' . $meta_title; ?></title>
    <link rel="icon" href="<?= base_url('assets/admin/images/' . $fav_icon); ?>">
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <?= link_tag('assets/admin/css/icons/icomoon/styles.css'); ?>
    <?= link_tag('assets/admin/css/minified/bootstrap.min.css'); ?>
    <?= link_tag('assets/admin/css/minified/core.min.css'); ?>
    <?= link_tag('assets/admin/css/minified/components.min.css'); ?>
    <?= link_tag('assets/admin/css/minified/colors.min.css'); ?>
    <!-- Core JS files -->
    <script type="text/javascript" src="<?= base_url('assets/admin/js/plugins/loaders/pace.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/admin/js/core/libraries/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/admin/js/core/libraries/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/admin/js/plugins/loaders/blockui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/admin/js/custom.js'); ?>"></script>
    <!-- Theme JS files -->
    <script type="text/javascript" src="<?php echo base_url('assets/admin/js/core/app.js'); ?>"></script>
  </head>

  <body>
    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
      <div class="navbar-header">
        <a class="navbar-brand" href="<?= base_url(); ?>"><b>Visit Site</b></a>
      </div>
    </div>

    <!-- Page container -->
    <div class="page-container login-container">
      <!-- Page content -->
      <div class="page-content">
        <!-- Main content -->
        <div class="content-wrapper">
          <!-- Content area -->
          <div class="content">
            <!-- Simple login form -->
            <?= form_open('admin/login'); ?>
              <div class="panel panel-body login-form">
                <div class="text-center">
                  <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                  <h5 class="content-group">Login to your account</h5>
                </div>
                <?php if ($err = $this->session->flashdata('alert_error')) { ?>
                  <div class="text-danger text-center content-group err">Invalid Credentials</div>
                <?php } ?>
                <div class="form-group has-feedback has-feedback-left">
                  <?= form_input(array('type'=>'email', 'name'=>'email', 'class'=>'form-control', 'placeholder'=>'Email', 'autofocus'=>true)); ?>
                  <div class="form-control-feedback">
                    <i class="icon-user text-muted"></i>
                  </div>
                </div>
                <div class="form-group has-feedback has-feedback-left">
                  <?= form_password(array('name'=>'password', 'class'=>'form-control', 'placeholder'=>'Password')); ?>
                  <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                  </div>
                </div>
                <div class="form-group">
                  <?= form_submit(array('name'=>'logbtn', 'value'=>'Sign in', 'class'=>'btn btn-primary btn-block')); ?>
                </div>
                <div class="text-center">
                  <a href="<?php echo site_url('admin/forgot-password'); ?>">Forgot password?</a>
                </div>
              </div>
            <?= form_close(); ?>

            <!-- Footer -->
            <div class="footer text-muted">
              &copy; <?php echo date('Y') ?>. <a href="<?= base_url();  ?>"><?= $meta_title; ?></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>