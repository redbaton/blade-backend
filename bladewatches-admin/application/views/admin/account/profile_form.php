<!-- Page header -->
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-stack2 position-left"></i> <span class="text-semibold">Admin Profile</span></h4>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">
		<?php alert(); ?>
		<div class="row">
			<div class="col-lg-8">
				<form action="<?php echo site_url('admin/profile'); ?>" method="POST" enctype="multipart/form-data">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Personal Information <ul class="icons-list pull-right"><li><a data-action="collapse"></a></li></ul></h5>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-6">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label for="FirstName">Firstname</label>
												<input placeholder="Firstname" id="FirstName" name="firstname" type="text" class="form-control" value="<?= $this->admin_model->details('admin_fname'); ?>" required />
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label for="LastName">LastName</label>
												<input placeholder="Lastname" id="LastName" name="lastname" type="text" class="form-control" value="<?= $this->admin_model->details('admin_lname'); ?>" />
											</div>
										</div>
									</div>
									<div class="form-group">
										<label for="Email">Email</label>
										<input type="email" placeholder="Email" class="form-control" name="email" value="<?php echo $user_info['uemail']; ?>" required />
									</div>
									<div class="form-group">
										<label for="Email">Contact Email</label>
										<input type="email" placeholder="Contact Email" class="form-control" name="contemail" value="<?= $this->admin_model->details('contemail'); ?>" required />
									</div>
									<div class="form-group">
										<label for="Copyrights">Copyrights</label>
										<input type="text" placeholder="Copyrights" class="form-control" name="copyrights" value="<?= $this->admin_model->details('copyrights'); ?>" required />
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="Email">Site Title</label>
										<input placeholder="Site Title" id="sitetitle" name="sitetitle" type="text" class="form-control" value="<?= $this->admin_model->details('site_title'); ?>" required />
									</div>
									<div class="form-group" style="display: none;">
										<div class="row">
											<div class="col-lg-6">
												<label>Site Logo</label>
												<img class="img-thumbnail" id="sitelogo-thumb" src="<?= base_url('assets/admin/images/' . $this->admin_model->details('main_logo')); ?>" width="100%"/>
											</div>
											<div class="col-lg-6">
												<p>Dimension: 200 x 200<br>Type: JPG / PNG / JPEG / SVG<br></p>
												<button type="button" class="btn btn-default" onclick="$('[name=sitelogo]').trigger('click');" style="width:100%">Choose Picture</button>
												<input type='file' id="sitelogo" name="sitelogo" onchange="readURL('sitelogo-thumb', this);" accept="image/*" style="display:none;" />
											</div>
										</div>
									</div>
									<div class="form-group" style="display: none;">
										<div class="row">
											<div class="col-lg-6">
												<label>Site Scroll Logo</label>
												<img class="img-thumbnail" id="scrolllogo-thumb" src="<?= base_url('assets/admin/images/' . $this->admin_model->details('scroll_logo')); ?>" width="100%"/>
											</div>
											<div class="col-lg-6">
												<p>Type: JPG / PNG / SVG<br></p>
												<button type="button" class="btn btn-default" onclick="$('[name=scrolllogo]').trigger('click');" style="width:100%">Choose Picture</button>
												<input type='file' id="scrolllogo" name="scrolllogo" onchange="readURL('scrolllogo-thumb', this);" accept="image/*" style="display:none;" />
											</div>
										</div>
									</div>
									<div class="form-group" style="display: none;">
										<div class="row">
											<div class="col-lg-6">
												<label>Footer Logo</label>
												<img class="img-thumbnail" id="footlogo-thumb" src="<?= base_url('assets/admin/images/' . $this->admin_model->details('footer_logo')); ?>" width="100%"/>
											</div>
											<div class="col-lg-6">
												<p>Dimension: 200 x 200<br>Type: JPG / PNG / JPEG / SVG<br></p>
												<button type="button" class="btn btn-default" onclick="$('[name=footlogo]').trigger('click');" style="width:100%">Choose Picture</button>
												<input type='file' id="footlogo" name="footlogo" onchange="readURL('footlogo-thumb', this);" accept="image/*" style="display:none;" />
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-lg-6">
											<label>Favicon</label>
												<img class="img-thumbnail" id="favicon-thumb" src="<?= base_url('assets/admin/images/' . $this->admin_model->details('favicon')); ?>" />
											</div>
											<div class="col-lg-6">
												<p>Dimension: 25 x 25<br>Type: ICO<br></p>
												<button type="button" class="btn btn-default" onclick="$('[name=favlogo]').trigger('click');" style="width:100%">Choose Picture</button>
												<input type='file' id="favlogo" name="favlogo" onchange="readURL('favicon-thumb', this);" accept="image/*" style="display:none;" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<a href="<?php echo site_url('admin/profile'); ?>" class="btn btn-default pull-left">Cancel</a>
							<button type="submit" class="btn btn-success pull-right">Save Changes</button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-lg-4">
				<form action="<?php echo site_url('admin/password'); ?>" method="POST" enctype="multipart/form-data">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Change Password <ul class="icons-list pull-right"><li><a data-action="collapse"></a></li></ul></h5>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label>Old Password</label>
								<input type="password" placeholder="Old Password" class="form-control" name="old_password" required />
							</div>
							<div class="form-group">
								<label>New Password</label>
								<input type="password" placeholder="New Password" class="form-control" name="new_password" required />
							</div>
							<div class="form-group">
								<label>Re-type Password</label>
								<input type="password" placeholder="Re-type Password" class="form-control" name="conf_password" required />
							</div>
							<button type="reset" class="btn btn-default pull-left">Cancel</button>
							<button type="submit" class="btn btn-success pull-right">Change Password</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3">
				<form action="<?php echo site_url('admin/social'); ?>" method="POST">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Social Links <ul class="icons-list pull-right"><li><a data-action="collapse"></a></li></ul></h5>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label>Facebook</label>
								<input type="text" placeholder="Url Link" class="form-control" name="facebook" value="<?= $this->admin_model->details('facebook'); ?>" />
							</div>
							<div class="form-group">
								<label>Gmail</label>
								<input type="text" placeholder="Url Link" class="form-control" name="gmail" value="<?= $this->admin_model->details('gmail'); ?>" />
							</div>
							<div class="form-group">
								<label>Twitter</label>
								<input type="text" placeholder="Url Link" class="form-control" name="twitter" value="<?= $this->admin_model->details('twitter'); ?>" />
							</div>
							<div class="form-group">
								<label>Linked In</label>
								<input type="text" placeholder="Url Link" class="form-control" name="linkedin" value="<?= $this->admin_model->details('linkedin'); ?>" />
							</div>
							<div class="form-group">
								<label>Youtube</label>
								<input type="text" placeholder="Url Link" class="form-control" name="youtube" value="<?= $this->admin_model->details('youtube'); ?>" />
							</div>
							<div class="form-group">
								<label>Instagram</label>
								<input type="text" placeholder="Url Link" class="form-control" name="insta" value="<?= $this->admin_model->details('insta'); ?>" />
							</div>
							<div class="form-group">
								<label>Google Plus</label>
								<input type="text" placeholder="Url Link" class="form-control" name="gplus" value="<?= $this->admin_model->details('gplus'); ?>" />
							</div>
							<div class="form-group">
								<label>Pinterest</label>
								<input type="text" placeholder="Url Link" class="form-control" name="pinterest" value="<?= $this->admin_model->details('pinterest'); ?>" />
							</div>
							<button type="reset" class="btn btn-default pull-left">Cancel</button>
							<button type="submit" class="btn btn-success pull-right">Save Changes</button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-lg-3">
				<form action="<?php echo site_url('admin/contactdetail'); ?>" method="POST">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Contact Details<ul class="icons-list pull-right"><li><a data-action="collapse"></a></li></ul></h5>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label>Address</label>
								<input type="text" placeholder="Address" class="form-control" name="addr" value="<?= $this->admin_model->details('addr'); ?>" />
							</div>
							<div class="form-group">
								<label>Contact No.</label>
								<input type="text" placeholder="Contact No." class="form-control" name="cont" value="<?= $this->admin_model->details('cont'); ?>" />
							</div>
							<div class="form-group">
								<label>Fax</label>
								<input type="text" placeholder="Fax" class="form-control" name="fax" value="<?= $this->admin_model->details('fax'); ?>" />
							</div>
							<div class="form-group">
								<label>Email Address</label>
								<input type="email" placeholder="Email Address" class="form-control" name="email" value="<?= $this->admin_model->details('email'); ?>" />
							</div>
							<button type="reset" class="btn btn-default pull-left">Cancel</button>
							<button type="submit" class="btn btn-success pull-right">Save Changes</button>
						</div>
					</div>
				</form>
			</div>
		</div>