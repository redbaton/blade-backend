<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-image2 position-left"></i> <span class="text-semibold">Edit Product</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="<?php echo site_url('products'); ?>" class="btn btn-link btn-float has-text"><i class="icon-file-empty text-primary"></i><span>Go back</span></a>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">
        <div class="panel-body">

            <form action="<?php echo site_url('products/edit/'.$product['id']); ?>" method="POST" class="form-validate" enctype="multipart/form-data">
                <div class="row">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Product Image (1)</label>

                                <div class="thumbnail">
                                    <div class="thumb">
                                        <?php if(!empty($product['image1'])){  ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/products/'.$product['image1']); ?>" alt="<?= $product['image1']; ?>">
                                        <?php }else{ ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Product Thumb Image" />
                                        <?php } ?>
                                        <div class="caption-overflow">
                                            <span>
                                                <a href="javascript:void(0);" onclick="$('[name=image1]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                                <small class="text-muted"><small><small> Size: 315 x 379</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                            </span>
                                        </div>
                                    </div>
                                    <input type="file" name="image1" style="display:none;" onchange="prodImg(this)" />
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>On Hover Image</label>

                                <div class="thumbnail">
                                    <div class="thumb">
                                        <?php if(!empty($product['hover1'])){  ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/products/'.$product['hover1']); ?>" alt="<?= $product['hover1']; ?>">
                                        <?php }else{ ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Product Hover Image" />
                                        <?php } ?>
                                        <div class="caption-overflow">
                                            <span>
                                                <a href="javascript:void(0);" onclick="$('[name=hover1]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                                <small class="text-muted"><small><small> Size: 315 x 379</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                            </span>
                                        </div>
                                    </div>
                                    <input type="file" name="hover1" style="display:none;" onchange="prodImg(this)" />
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Product Image (2)</label>

                                <div class="thumbnail">
                                    <div class="thumb">
                                        <?php if(!empty($product['image2'])){  ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/products/'.$product['image2']); ?>" alt="<?= $product['image2']; ?>">
                                        <?php }else{ ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Product Hover Image" />
                                        <?php } ?>
                                        <div class="caption-overflow">
                                            <span>
                                                <a href="javascript:void(0);" onclick="$('[name=image2]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                                <small class="text-muted"><small><small> Size: 315 x 379</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                            </span>
                                        </div>
                                    </div>
                                    <input type="file" name="image2" style="display:none;" onchange="prodImg(this)" />
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>On Hover Image</label>

                                <div class="thumbnail">
                                    <div class="thumb">
                                        <?php if(!empty($product['hover2'])){  ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/products/'.$product['hover2']); ?>" alt="<?= $product['hover2']; ?>">
                                        <?php }else{ ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Product Hover Image" />
                                        <?php } ?>
                                        <div class="caption-overflow">
                                            <span>
                                                <a href="javascript:void(0);" onclick="$('[name=hover2]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                                <small class="text-muted"><small><small> Size: 315 x 379</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                            </span>
                                        </div>
                                    </div>
                                    <input type="file" name="hover2" style="display:none;" onchange="prodImg(this)" />
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Product Image (3)</label>

                                <div class="thumbnail">
                                    <div class="thumb">
                                        <?php if(!empty($product['image3'])){  ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/products/'.$product['image3']); ?>" alt="<?= $product['image3']; ?>">
                                        <?php }else{ ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Product Hover Image" />
                                        <?php } ?>
                                        <div class="caption-overflow">
                                            <span>
                                                <a href="javascript:void(0);" onclick="$('[name=image3]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                                <small class="text-muted"><small><small> Size: 315 x 379</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                            </span>
                                        </div>
                                    </div>
                                    <input type="file" name="image3" style="display:none;" onchange="prodImg(this)" />
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>On Hover Image</label>

                                <div class="thumbnail">
                                    <div class="thumb">
                                        <?php if(!empty($product['hover3'])){  ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/products/'.$product['hover3']); ?>" alt="<?= $product['hover3']; ?>">
                                        <?php }else{ ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Product Hover Image" />
                                        <?php } ?>
                                        <div class="caption-overflow">
                                            <span>
                                                <a href="javascript:void(0);" onclick="$('[name=hover3]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                                <small class="text-muted"><small><small> Size: 315 x 379</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                            </span>
                                        </div>
                                    </div>
                                    <input type="file" name="hover3" style="display:none;" onchange="prodImg(this)" />
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Product Image (4)</label>

                                <div class="thumbnail">
                                    <div class="thumb">
                                        <?php if(!empty($product['image4'])){  ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/products/'.$product['image4']); ?>" alt="<?= $product['image4']; ?>">
                                        <?php }else{ ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Product Hover Image" />
                                        <?php } ?>
                                        <div class="caption-overflow">
                                            <span>
                                                <a href="javascript:void(0);" onclick="$('[name=image4]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                                <small class="text-muted"><small><small> Size: 315 x 379</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                            </span>
                                        </div>
                                    </div>
                                    <input type="file" name="image4" style="display:none;" onchange="prodImg(this)" />
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>On Hover Image</label>

                                <div class="thumbnail">
                                    <div class="thumb">
                                        <?php if(!empty($product['hover4'])){  ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/products/'.$product['hover4']); ?>" alt="<?= $product['hover4']; ?>">
                                        <?php }else{ ?>
                                            <img style="width: 175px; height: 175px;" src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Product Hover Image" />
                                        <?php } ?>
                                        <div class="caption-overflow">
                                            <span>
                                                <a href="javascript:void(0);" onclick="$('[name=hover4]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                                <small class="text-muted"><small><small> Size: 315 x 379</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                            </span>
                                        </div>
                                    </div>
                                    <input type="file" name="hover4" style="display:none;" onchange="prodImg(this)" />
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <textarea name="name" class="form-control" rows="1" cols="1" required><?= $product['name']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Meta Description</label>
                        <textarea name="metadesc" class="form-control" rows="1" cols="1"><?= $product['metadesc']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Meta Keywords</label>
                        <textarea name="metakeys" class="form-control" rows="1" cols="1"><?= $product['metakeys']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Excerpt</label>
                        <textarea name="excerpt" class="form-control" rows="1" cols="1" required><?= $product['excerpt']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Features</label> <?php
                        $feature = json_decode($product['attributes']); ?>
                        <div class="features">
                            <div class="row" style="padding-bottom: 2em;">
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="key1" placeholder="E.g. Model" value="<?= $feature->key1; ?>">
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="value1" placeholder="E.g. 20-3356GSS-SWB" value="<?= $feature->value1; ?>">
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 2em;">
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="key2" placeholder="E.g. BUCKLE" value="<?= $feature->key2; ?>">
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="value2" placeholder="E.g. Strap" value="<?= $feature->value2; ?>">
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 2em;">
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="key3" placeholder="E.g. MOVEMENT" value="<?= $feature->key3; ?>">
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="value3" placeholder="E.g. Three Hands With Date" value="<?= $feature->value3; ?>">
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 2em;">
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="key4" placeholder="E.g. BAND" value="<?= $feature->key4; ?>">
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="value4" placeholder="E.g. Genuine Leather" value="<?= $feature->value4; ?>">
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 2em;">
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="key5" placeholder="E.g. CASE" value="<?= $feature->key5; ?>">
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="value5" placeholder="E.g. Stainless Steel" value="<?= $feature->value5; ?>">
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 2em;">
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="key6" placeholder="E.g. WATER RESISTANCE" value="<?= $feature->key6; ?>">
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="value6" placeholder="E.g. 5 ATM" value="<?= $feature->value6; ?>">
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 2em;">
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="key7" placeholder="E.g. CASE SIZE" value="<?= $feature->key7; ?>">
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="value7" placeholder="E.g. 42MM" value="<?= $feature->value7; ?>">
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 2em;">
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="key8" placeholder="E.g. Other" value="<?= $feature->key8; ?>">
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" name="value8" placeholder="E.g. Other" value="<?= $feature->value8; ?>">
                                </div>
                            </div>
                            <input type="hidden" class="attributes" name="attributes">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Collection Categories</label>
                        <div class="collection"> <?php $chkd = explode(',', $product['collections']);
                            foreach ($collections as $key => $c_cat) { ?>
                                <div class="cat">
                                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox" <?php if (in_array($c_cat['id'], $chkd)) { echo "checked"; } ?> ><?= $c_cat['name'] ?></label>
                                    <input type="hidden" class="cat_id" name="cat_id" value="<?= $c_cat['id'] ?>">
                                </div> <?php
                            } ?>
                        </div>
                        <input type="hidden" class="collections" name="collections">
                    </div>
                    <div class="form-group">
                        <label>Accessories Categories</label>
                        <div class="accessories"> <?php $chk = explode(',', $product['accessories']);
                            foreach ($accessories as $key => $a_cat) { ?>
                                <div class="cat">
                                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox" <?php if (in_array($a_cat['id'], $chk)) { echo "checked"; } ?> ><?= $a_cat['name'] ?></label>
                                    <input type="hidden" class="cat_id" name="cat_id" value="<?= $a_cat['id'] ?>">
                                </div> <?php
                            } ?>
                        </div>
                        <input type="hidden" class="accessoriesId" name="accessories">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Status</label>
                        <div class="checkbox checkbox-switch">
                            <input type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Active" data-off-text="Disabled" class="switch" <?php echo ($product['status']==1)?'checked="checked"':''; ?> name="status" />
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary pull-right" style="margin-left:10px;" type="submit">Submit <i class="icon-arrow-right14 position-right"></i></button>
                        <a href="<?php echo site_url('products'); ?>" class="btn btn-default pull-right">Cancel</a>
                    </div>
                    <div class="pull-right">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        function prodImg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(input).closest('div').find('img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function(){
            $(".switch").bootstrapSwitch();
        });

        $(".form-validate").submit(function(e){
            e.preventDefault();

            var attr = {
                key1:$('input[name="key1"]').val(), value1:$('input[name="value1"]').val(),
                key2:$('input[name="key2"]').val(), value2:$('input[name="value2"]').val(),
                key3:$('input[name="key3"]').val(), value3:$('input[name="value3"]').val(),
                key4:$('input[name="key4"]').val(), value4:$('input[name="value4"]').val(),
                key5:$('input[name="key5"]').val(), value5:$('input[name="value5"]').val(),
                key6:$('input[name="key6"]').val(), value6:$('input[name="value6"]').val(),
                key7:$('input[name="key7"]').val(), value7:$('input[name="value7"]').val(),
                key8:$('input[name="key8"]').val(), value8:$('input[name="value8"]').val()
            };
            var data = JSON.stringify(attr);
            $('.attributes').val(data);
            // console.log(attr);

            var collId = [];
            $( '.collection .cat' ).each(function( index ) {
                if ( $(this).find('input.checkbox').prop('checked') == true ) {
                    collId.push($(this).find('input.cat_id').val());
                }
            });
            $('.collections').val(collId);

            var accId = [];
            $( '.accessories .cat' ).each(function( index ) {
                if ( $(this).find('input.checkbox').prop('checked') == true ) {
                    accId.push($(this).find('input.cat_id').val());
                }
            });
            $('.accessoriesId').val(accId);
            $(".form-validate").unbind().submit();
        });
    </script>