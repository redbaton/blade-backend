<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-users4 position-left"></i> <span class="text-semibold">Products</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="<?php echo site_url('products/create'); ?>" class="btn btn-link btn-float has-text"><i class="icon-file-empty text-primary"></i><span>Add Product</span></a>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <?php alert(); ?>
        <?php if(!empty($products)){ ?>
    <div class="panel panel-flat">
        <div class="table-responsive">
            <table class="table table-bordered table-striped sort-tbl">
                <thead>
                    <tr>
                        <th style="width:20px;">#</th>
                        <th>Name</th>
                        <th>Collection Categories</th>
                        <th>Accessories Categories</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $key => $product) { ?>
                        <tr>
                            <td><?php echo $product['id']; ?></td>
                            <td style="max-width:400px;"><?php echo $product['name']; ?></td>
                            <td style="max-width:400px;"><?php 
                                $chkd = explode(',', $product['collections']);
                                foreach ($collections as $key => $c_cat) { 
                                    if (in_array($c_cat['id'], $chkd)) {
                                        echo $c_cat['name'].', ';
                                    }
                                } ?>
                            </td>
                            <td style="max-width:400px;"><?php 
                                $chk = explode(',', $product['accessories']);
                                foreach ($accessories as $key => $a_cat) { 
                                    if (in_array($a_cat['id'], $chk)) {
                                        echo $a_cat['name'].', ';
                                    }
                                } ?>
                            </td>
                            <td>
                                <a href="<?php echo site_url('products/edit/'.$product['id']); ?>" class="btn btn-xs btn-primary"><i class="icon-pencil6"></i> Edit</a>
                                <a href="<?php echo site_url('products/delete/'.$product['id']); ?>" class="btn btn-xs btn-danger"><i class="icon-chart"></i> Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php }else{ ?>
            <div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
                <h6 class="alert-heading text-semibold">No Products Added</h6>
            </div>
        <?php } ?>