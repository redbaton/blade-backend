<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-image2 position-left"></i> <span class="text-semibold">Header Menu Management</span></h4>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php alert(); ?>
    <div class="panel panel-flat">
        <div class="panel-body">

            <form action="<?php echo base_url('menu'); ?>" method="POST" class="form-validate">               

                <div class="row">
                    <div class="col-lg-3">
                        <label>Menu Name:</label>
                        <input type="text" name="menu_1" value="<?= $this->admin_model->details('menu_1'); ?>" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <label>Link Url:</label>
                        <input type="text" name="menu_1_link" value="<?= $this->admin_model->details('menu_1_link'); ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3">
                        <label>Menu Name:</label>
                        <input type="text" name="menu_2" value="<?= $this->admin_model->details('menu_2'); ?>" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <label>Link Url:</label>
                        <input type="text" name="menu_2_link" value="<?= $this->admin_model->details('menu_2_link'); ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3">
                        <label>Menu Name:</label>
                        <input type="text" name="menu_3" value="<?= $this->admin_model->details('menu_3'); ?>" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <label>Link Url:</label>
                        <input type="text" name="menu_3_link" value="<?= $this->admin_model->details('menu_3_link'); ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3">
                        <label>Menu Name:</label>
                        <input type="text" name="menu_4" value="<?= $this->admin_model->details('menu_4'); ?>" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <label>Link Url:</label>
                        <input type="text" name="menu_4_link" value="<?= $this->admin_model->details('menu_4_link'); ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3">
                        <label>Menu Name:</label>
                        <input type="text" name="menu_5" value="<?= $this->admin_model->details('menu_5'); ?>" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <label>Link Url:</label>
                        <input type="text" name="menu_5_link" value="<?= $this->admin_model->details('menu_5_link'); ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="row">
                    <button class="btn btn-primary pull-right" style="margin-left:10px;" type="submit">Submit <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-image2 position-left"></i> <span class="text-semibold">Footer Menu Management</span></h4>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">

            <form action="<?php echo base_url('menu/footer'); ?>" method="POST" class="form-validate">               

                <div class="row">
                    <div class="col-lg-3">
                        <label>Menu Name:</label>
                        <input type="text" name="foot_menu_1" value="<?= $this->admin_model->details('foot_menu_1'); ?>" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <label>Link Url:</label>
                        <input type="text" name="foot_menu_1_link" value="<?= $this->admin_model->details('foot_menu_1_link'); ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3">
                        <label>Menu Name:</label>
                        <input type="text" name="foot_menu_2" value="<?= $this->admin_model->details('foot_menu_2'); ?>" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <label>Link Url:</label>
                        <input type="text" name="foot_menu_2_link" value="<?= $this->admin_model->details('foot_menu_2_link'); ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3">
                        <label>Menu Name:</label>
                        <input type="text" name="foot_menu_3" value="<?= $this->admin_model->details('foot_menu_3'); ?>" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <label>Link Url:</label>
                        <input type="text" name="foot_menu_3_link" value="<?= $this->admin_model->details('foot_menu_3_link'); ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="row">
                    <button class="btn btn-primary pull-right" style="margin-left:10px;" type="submit">Submit <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
