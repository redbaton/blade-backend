<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-image2 position-left"></i> <span class="text-semibold">Social Feed Section</span></h4>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="slides"> <?php 
                if (!empty($images)) {
                    foreach ($images as $key => $singledata) { ?>
                        <form class="row" id="slide" style="padding: 2em; border: 1px solid; margin-bottom: 2em;">
                            <div class="col-md-3">
                                <img src="<?php echo base_url('assets/admin/images/social/' . $singledata['image']); ?>" alt="<?= $singledata['image']; ?>" width="100%" />
                                <input type="file" class="image" name="image" onchange="banImg(this)" />
                                <p>Size: 1000 x 1000</p>
                            </div>
                            <div class="col-md-8">
                                <input type="hidden" class="id" name="id" value="<?= $singledata['id'] ?>">
                                <input style="margin-bottom: 5px;" type="text" placeholder="Text" maxlength="200" name="img_heading" value="<?= $singledata['img_heading'] ?>" class="form-control">
                                <input style="margin-bottom: 5px;" type="text" placeholder="FB Link" name="fb_link" value="<?= $singledata['fb_link'] ?>" class="form-control">
                                <input style="margin-bottom: 5px;" type="text" placeholder="Twitter Link" name="twitter_link" value="<?= $singledata['twitter_link'] ?>" class="form-control">
                                <input style="margin-bottom: 5px;" type="text" placeholder="YouTube Link" name="ytube_link" value="<?= $singledata['ytube_link'] ?>" class="form-control">
                                <input style="margin-bottom: 5px;" type="text" placeholder="Insta Link" name="insta_link" value="<?= $singledata['insta_link'] ?>" class="form-control">
                            </div>
                            <span class="rmv-form" style="cursor: pointer;">Remove</span>
                        </form> <?php
                    }
                } ?>
            
            </div>
            <div class="row">
                <div class="col-md-3">
                    <button type="button" class="btn btn-default" style="width:100%" onclick="addSlide();">Add Slide</button>
                </div>
                <div class="col-md-3">
                    <button type="button" class="btn btn-success" onclick="saveSlider();" style="width:100%">Save</button>
                </div>
            </div>
            <label style="margin-top: 1em;">Note: Slides without Images in it will get removed automatically.</label>
        </div>
    </div>

<script>
    function banImg(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).parents('#slide').find('img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function addSlide() {
        $('.slides').append('<form class="row" id="slide" style="padding: 2em; border: 1px solid; margin-bottom: 2em;"><div class="col-md-3"><img src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Article Thumb Image" width="100%" /><input type="file" class="image" name="image" onchange="banImg(this)" /><p>Size: 1000 x 1000</p></div><div class="col-md-8"><input style="margin-bottom: 5px;" type="text" maxlength="200" placeholder="Text" name="img_heading" class="form-control"><input style="margin-bottom: 5px;" type="text" placeholder="FB Link" name="fb_link" class="form-control"><input style="margin-bottom: 5px;" type="text" placeholder="Twitter Link" name="twitter_link" class="form-control"><input style="margin-bottom: 5px;" type="text" placeholder="YouTube Link" name="ytube_link" class="form-control"><input style="margin-bottom: 5px;" type="text" placeholder="Insta Link" name="insta_link" class="form-control"></div><span class="rmv-form" style="cursor: pointer;">Remove</span></form>');
    }

    // to remove section of page contents
    $(document).on('click', '.rmv-form', function() {
        $sliderId = $(this).parents('form').find("input.id").val();
        if ($sliderId) {
            $.ajax({
                url: "<?= base_url('socialfeed/deletedata'); ?>",
                type: "post",
                data: {id: $sliderId},
                success: function (data) {
                    console.log("success");
                },
                error: function(data){
                    console.log("error");
                }
            });
            $(this).closest('form').remove();
        } else {
            $(this).closest('form').remove();
        }
    });

    function saveSlider() {
        var length = $( ".slides #slide" ).length;

        if (length == 0) { window.location.href="/bladewatches-admin/socialfeed"; }

        $( ".slides #slide" ).each(function( index ) {
            if ($(this).find("input.id").val()) {
                $.ajax({
                    url: "<?= base_url('socialfeed/upload_update'); ?>",
                    method: "post",
                    data: new FormData(this),
                    async: false,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        console.log("success");
                    },
                    error: function(data){
                        console.log("error");
                    }
                });
            } else {

                if ($(this).find("input.image").get(0).files.length == 0) {
                    // $(this).remove();
                } else {
                    $.ajax({
                        url: "<?= base_url('socialfeed/upload'); ?>",
                        method: "post",
                        data: new FormData(this),
                        async: false,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            console.log("success");
                        },
                        error: function(data){
                            console.log("error");
                        }
                    });
                }
            }

            if (index === (length - 1)) {
                window.location.href="/bladewatches-admin/socialfeed";
            }
        });
    }
</script>