<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-users4 position-left"></i> <span class="text-semibold">Records</span></h4>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
        <?php alert(); ?>
        <?php if(!empty($offers)){ ?>
        <div class="row">
    		<div class="col-12">
    			<button class="btn btn-primary pull-right" style="margin-bottom: 1em; margin-right: 1em;" onclick="exportfunc('offers');"> <i class="icon-file-excel"> </i> Export Excel</button>
    		</div>
        </div>
        <div class="panel panel-flat">
            <div class="table-responsive">
                <table class="table table-bordered table-striped sort-tbl">
                    <thead>
                        <tr>
                            <th style="width:20px;">#</th>
                            <th>Name</th>
                            <th>Phone No.</th>
                            <th>Email</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($offers as $key => $offer) { ?>
                            <tr>
                                <td><?php echo $offer['id']; ?></td>
                                <td style="max-width:400px;"><?php echo $offer['name']; ?></td>
                                <td><?php echo $offer['cont']; ?></td>
                                <td><?php echo $offer['email']; ?></td>
                                <td><?php echo date("Y-m-d h:i", strtotime('+330 minutes', strtotime($offer['time']))); ?></td>
                                <td>
                                    <a href="<?php echo site_url('enquiries/delete/offers/'.$offer['id']); ?>" class="btn btn-xs btn-danger"><i class="icon-chart"></i> Delete</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } elseif (!empty($newsletter)) { ?>
            <div class="row">
	    		<div class="col-12">
	    			<button class="btn btn-primary pull-right" style="margin-right: 1em; margin-bottom: 1em;" onclick="exportfunc('newsletter');"> <i class="icon-file-excel"> </i> Export Excel</button>
	    		</div>
	        </div>
            <div class="panel panel-flat">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped sort-tbl">
                        <thead>
                            <tr>
                                <th style="width:20px;">#</th>
                                <th>Email</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($newsletter as $key => $news) { ?>
                                <tr>
                                    <td><?php echo $news['id']; ?></td>
                                    <td><?php echo $news['email']; ?></td>
                                    <td><?php echo date("Y-m-d h:i", strtotime('+330 minutes', strtotime($news['time']))); ?></td>
                                    <td>
                                        <a href="<?php echo site_url('enquiries/delete/newsletter/'.$news['id']); ?>" class="btn btn-xs btn-danger"><i class="icon-chart"></i> Delete</a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        <?php } elseif (!empty($enquiries)) { ?>
            <div class="row">
	    		<div class="col-12">
	    			<button class="btn btn-primary pull-right" style="margin-bottom: 1em; margin-right: 1em;" onclick="exportfunc('enquiry');"> <i class="icon-file-excel"> </i> Export Excel</button>
	    		</div>
	        </div>
            <div class="panel panel-flat">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped sort-tbl">
                        <thead>
                            <tr>
                                <th style="width:20px;">#</th>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Email</th>
                                <th>Contact No.</th>
                                <th>City</th>
                                <th>Country</th>
                                <th>Type</th>
                                <th>Message</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($enquiries as $key => $enquiry) { ?>
                                <tr>
                                    <td><?php echo $enquiry['id']; ?></td>
                                    <td><?php echo $enquiry['name']; ?></td>
                                    <td><?php echo $enquiry['company']; ?></td>
                                    <td><?php echo $enquiry['email']; ?></td>
                                    <td><?php echo $enquiry['cont']; ?></td>
                                    <td><?php echo $enquiry['city']; ?></td>
                                    <td><?php echo $enquiry['country']; ?></td>
                                    <td><?php echo $enquiry['type']; ?></td>
                                    <td><?php echo $enquiry['message']; ?></td>
                                    <td><?php echo date("Y-m-d h:i", strtotime('+330 minutes', strtotime($enquiry['time']))); ?></td>
                                    <td>
                                        <a href="<?php echo site_url('enquiries/delete/enquiry/'.$enquiry['id']); ?>" class="btn btn-xs btn-danger"><i class="icon-chart"></i> Delete</a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        <?php } else { ?>
            <div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
                <h6 class="alert-heading text-semibold">No Records Available</h6>
            </div>
        <?php } ?>

        <script type="text/javascript">
            function exportfunc(table) {
                window.location = '<?php echo base_url('enquiries/downloadfile');?>/'+table;
            }
        </script>