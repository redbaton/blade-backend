<!-- Page header -->
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-stack2 position-left"></i> <span class="text-semibold">ContactUs Page Contents</span></h4>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">
		<?php alert(); $pagedata = json_decode($contact['contents']); ?>
		<div class="row">
			<div class="col-lg-12">
				<form action="<?php echo site_url('pages/contact'); ?>" method="POST" enctype="multipart/form-data">
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<input placeholder="Meta Description" id="metadesc" name="metadesc" type="text" class="form-control" value="<?= $pagedata->metadesc; ?>" />
									</div>
									<div class="form-group">
										<input placeholder="Meta Keywords" id="metakeys" name="metakeys" type="text" class="form-control" value="<?= $pagedata->metakeys; ?>" />
									</div>
								</div>
							</div>
							<hr>
							<div class="row" style="padding-bottom: 4em;">
								<div class="col-lg-12">
									<h5 class="panel-title">Page Contents</h5>
								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<label for="BannerText">Text (Paragraph)</label>
										<textarea name="contPara" id="contPara" class="form-control" rows="6" cols="6"><?= $pagedata->contPara; ?></textarea>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Image</label>
										<img class="img-thumbnail" id="cont-thumb" src="<?= base_url('assets/admin/images/pages/' . $pagedata->contImg); ?>" width="100%"/>

										<input name="contImg" type="hidden" value="<?= $pagedata->contImg; ?>" />
										<p>Size: 567 x 318<br></p>
										<p>Type: JPG / PNG / JPEG / SVG<br></p>
										<button type="button" class="btn btn-default" onclick="$('[name=contImg]').trigger('click');" style="width:100%">Choose Picture</button>
										<input type='file' id="contImg" name="contImg" onchange="readURL('cont-thumb', this);" accept="image/*" style="display:none;" />
									</div>
								</div>
							</div>
							<button type="reset" class="btn btn-default pull-left">Cancel</button>
							<button type="submit" class="btn btn-success pull-right">Save Changes</button>
						</div>
					</div>
				</form>
			</div>
		</div>