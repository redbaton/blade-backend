<!-- Page header -->
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-stack2 position-left"></i> <span class="text-semibold">Home Page Contents</span></h4>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">
		<?php alert(); $pagedata = json_decode($home['contents']); ?>
		<div class="row">
			<div class="col-lg-12">
				<form action="<?php echo site_url('pages/home'); ?>" method="POST" enctype="multipart/form-data">
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<input placeholder="Meta Description" id="metadesc" name="metadesc" type="text" class="form-control" value="<?= $pagedata->metadesc; ?>" />
									</div>
									<div class="form-group">
										<input placeholder="Meta Keywords" id="metakeys" name="metakeys" type="text" class="form-control" value="<?= $pagedata->metakeys; ?>" />
									</div>
								</div>
							</div>
							<hr>
							<div class="row" style="padding-bottom: 4em;">
								<div class="col-lg-12">
									<h5 class="panel-title">HomeBanner Contents</h5>
								</div>
								<hr>
								<div class="col-lg-8">
									<div class="form-group">
										<label for="BannerHeading2">Heading (H2)</label>
										<input placeholder="Heading (H2)" id="bannerH2" name="bannerH2" type="text" class="form-control" value="<?= $pagedata->bannerH2; ?>" required />
									</div>
									<div class="form-group">
										<label for="BannerHeading1">Heading (H1)</label>
										<input placeholder="Heading (H1)" id="bannerH1" name="bannerH1" type="text" class="form-control" value="<?= $pagedata->bannerH1; ?>" required />
									</div>
									<div class="form-group">
										<label for="BannerText">Text (Paragraph)</label>
										<textarea name="bannerPara" id="bannerPara" class="form-control" rows="6" cols="6"><?= $pagedata->bannerPara; ?></textarea>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Banner Image</label>
										<img class="img-thumbnail" id="banner-thumb" src="<?= base_url('assets/admin/images/pages/' . $pagedata->bannerImg); ?>" width="100%"/>

										<input name="bannerImg" type="hidden" value="<?= $pagedata->bannerImg; ?>" />
										<p>Size: 955 x 1095<br></p>
										<p>Type: JPG / PNG / JPEG / SVG<br></p>
										<button type="button" class="btn btn-default" onclick="$('[name=bannerImg]').trigger('click');" style="width:100%">Choose Picture</button>
										<input type='file' id="bannerImg" name="bannerImg" onchange="readURL('banner-thumb', this);" accept="image/*" style="display:none;" />
									</div>
								</div>
							</div>
							<hr>
							<div class="row" style="padding-bottom: 4em;">
								<div class="col-lg-12">
									<h5 class="panel-title">Collection Section Contents</h5>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label for="CollectionHeading">Heading</label>
										<input placeholder="Heading" id="collHead" name="collHead" type="text" class="form-control" value="<?= $pagedata->collHead; ?>" required />
									</div>
									<div class="form-group">
										<label for="CollectionText">Text (Paragraph)</label>
										<textarea name="collPara" id="collPara" class="form-control" rows="4" cols="4"><?= $pagedata->collPara; ?></textarea>
									</div>
								</div>
							</div>
							<hr>
							<div class="row" style="padding-bottom: 4em;">
								<div class="col-lg-12">
									<h5 class="panel-title">Accessories Section Contents</h5>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label for="AccessoriesHeading">Heading</label>
										<input placeholder="Heading" id="accesHead" name="accesHead" type="text" class="form-control" value="<?= $pagedata->accesHead; ?>" required />
									</div>
									<div class="form-group">
										<label for="AccessoriesText">Text (Paragraph)</label>
										<textarea name="accesPara" id="accesPara" class="form-control" rows="4" cols="4"><?= $pagedata->accesPara; ?></textarea>
									</div>
								</div>
							</div>
							<hr>
							<div class="row" style="padding-bottom: 4em;">
								<div class="col-lg-12">
									<h5 class="panel-title">Social Feed Section Contents</h5>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label for="SocialFeedHeading">Heading</label>
										<input placeholder="Heading" id="socHead" name="socHead" type="text" class="form-control" value="<?= $pagedata->socHead; ?>" required />
									</div>
									<div class="form-group">
										<label for="SocialFeedText">Text (Paragraph)</label>
										<textarea name="socPara" id="socPara" class="form-control" rows="4" cols="4"><?= $pagedata->socPara; ?></textarea>
									</div>
								</div>
							</div>
							<button type="reset" class="btn btn-default pull-left">Cancel</button>
							<button type="submit" class="btn btn-success pull-right">Save Changes</button>
						</div>
					</div>
				</form>
			</div>
		</div>