<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-users4 position-left"></i> <span class="text-semibold">Stores & Services</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="<?php echo site_url('stores/create'); ?>" class="btn btn-link btn-float has-text"><i class="icon-file-empty text-primary"></i><span>Add Store Or Service</span></a>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <?php alert(); ?>
        <?php if(!empty($stores)){ ?>
    <div class="panel panel-flat">
        <div class="table-responsive">
            <table class="table table-bordered table-striped sort-tbl">
                <thead>
                    <tr>
                        <th style="width:20px;">#</th>
                        <th>Name</th>
                        <th>Store / Service</th>
                        <th>Country</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($stores as $key => $store) { ?>
                        <tr>
                            <td><?php echo $store['id']; ?></td>
                            <td style="max-width:400px;"><?php echo $store['name']; ?></td>
                            <td><?php echo $store['category']; ?></td>
                            <td><?php echo ucfirst($store['country']); ?></td>
                            <td>
                                <a href="<?php echo site_url('stores/edit/'.$store['id']); ?>" class="btn btn-xs btn-primary"><i class="icon-pencil6"></i> Edit</a>
                                <a href="<?php echo site_url('stores/delete/'.$store['id']); ?>" class="btn btn-xs btn-danger"><i class="icon-chart"></i> Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php }else{ ?>
            <div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
                <h6 class="alert-heading text-semibold">No Stores / Services Added</h6>
            </div>
        <?php } ?>