<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-image2 position-left"></i> <span class="text-semibold">Create Store / Service</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="<?php echo site_url('stores'); ?>" class="btn btn-link btn-float has-text"><i class="icon-file-empty text-primary"></i><span>Go back</span></a>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">
        <div class="panel-body">

            <form action="<?php echo site_url('stores/create'); ?>" method="POST" class="form-validate" enctype='multipart/form-data'>               

                <div class="row">
                    <div class="form-group">
                        <label>Name</label>
                        <textarea name="name" class="form-control" rows="1" cols="1" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea name="addr" class="form-control" rows="4" cols="4" required></textarea>
                    </div> 
                    <div class="form-group">
                        <label>Telephone</label>
                        <textarea name="tel" class="form-control" rows="1" cols="1"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Fax</label>
                        <textarea name="fax" class="form-control" rows="1" cols="1"></textarea>
                    </div> 
                    <div class="form-group">
                        <label>Email</label>
                        <textarea name="email" class="form-control" rows="1" cols="1"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                Country:
                                <select name="country">
                                    <option value="">Select Country</option> <?php 
                                    if (!empty($stores_country)) {
                                        foreach ($stores_country as $key => $country) { ?>
                                            <option value="<?= $country['country']; ?>"><?= $country['country']; ?></option> <?php
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <label>OR</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                Enter Country Name:
                                <input type="text" name="newcountry" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                Store / Service:
                                <select name="category">
                                    <option value="Store">Store</option>
                                    <option value="Service">Service</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary pull-right" style="margin-left:10px;" type="submit">Submit <i class="icon-arrow-right14 position-right"></i></button>
                            <a href="<?php echo site_url('stores'); ?>" class="btn btn-default pull-right">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>