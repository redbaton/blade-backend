<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-image2 position-left"></i> <span class="text-semibold">Create Category</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="<?php echo site_url('categories'); ?>" class="btn btn-link btn-float has-text"><i class="icon-file-empty text-primary"></i><span>Go back</span></a>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">
        <div class="panel-body">

            <form action="<?php echo site_url('categories/create'); ?>" method="POST" class="form-validate" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Thumbnail Image</label>

                            <div class="thumbnail">
                                <div class="thumb">
                                    <img src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Collection Thumb Image" />
                                    <div class="caption-overflow">
                                        <span>
                                            <a href="javascript:void(0);" onclick="$('[name=image]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                            <small class="text-muted"><small><small> Size: 315 x 379</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                        </span>
                                    </div>
                                </div>
                                <input type="file" name="image" style="display:none;" />
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Banner Image</label>

                            <div class="thumbnail banimage">
                                <div class="thumb">
                                    <img src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Collection Banner Image" />
                                    <div class="caption-overflow">
                                        <span>
                                            <a href="javascript:void(0);" onclick="$('[name=banimage]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                            <small class="text-muted"><small><small> Size: 1195 x 595</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                        </span>
                                    </div>
                                </div>
                                <input type="file" name="banimage" style="display:none;" />
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Mobile Banner Image</label>

                            <div class="thumbnail mbanimage">
                                <div class="thumb">
                                    <img src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="Mobile Banner Image" />
                                    <div class="caption-overflow">
                                        <span>
                                            <a href="javascript:void(0);" onclick="$('[name=mbanimage]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                            <small class="text-muted"><small><small> Size: 420 x 300</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                        </span>
                                    </div>
                                </div>
                                <input type="file" name="mbanimage" style="display:none;" />
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                	<div class="form-group">
                        <label>Meta Description</label>
                        <textarea name="metadesc" class="form-control" rows="2" cols="2"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Meta Keywords</label>
                        <textarea name="metakeys" class="form-control" rows="2" cols="2"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <textarea name="name" class="form-control" rows="1" cols="1" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Excerpt</label>
                        <textarea name="excerpt" class="form-control" rows="1" cols="1" required></textarea>
                    </div>
                    <div class="form-group">
                        Link to either Collection or Accessories:
                        <select name="type">
                            <option value="0">Select Option</option>
                            <option value="1">Collection</option>
                            <option value="2">Accessories</option>
                        </select>
                    </div>
                    <div class="form-group">
                        Link this Category to Footer:
                        <select name="linktofooter">
                            <option value="0">Select Block</option>
                            <option value="1">Block 1</option>
                            <option value="2">Block 2</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Status</label>
                        <div class="checkbox checkbox-switch">
                            <input type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Active" data-off-text="Disabled" class="switch" name="status" />
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary pull-right" style="margin-left:10px;" type="submit">Submit <i class="icon-arrow-right14 position-right"></i></button>
                        <a href="<?php echo site_url('categories'); ?>" class="btn btn-default pull-right">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(function(){
            $(".switch").bootstrapSwitch();
        });

        $(function(){
            $(document).on('change','[name="image"]',function(){
                var $tis = $(this);
                if (this.files && this.files[0]){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $tis.parents('.thumbnail').find('img').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });

            $(document).on('change','[name="banimage"]',function(){
                var $tis = $(this);
                if (this.files && this.files[0]){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $tis.parents('.banimage').find('img').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });

            $(document).on('change','[name="mbanimage"]',function(){
                var $tis = $(this);
                if (this.files && this.files[0]){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $tis.parents('.mbanimage').find('img').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>