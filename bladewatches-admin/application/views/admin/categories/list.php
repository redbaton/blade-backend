<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-users4 position-left"></i> <span class="text-semibold">Product Categories</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="<?php echo site_url('categories/create'); ?>" class="btn btn-link btn-float has-text"><i class="icon-file-empty text-primary"></i><span>Add Category</span></a>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <?php alert(); ?>
        <?php if(!empty($categories)){ ?>
    <div class="panel panel-flat">
        <div class="table-responsive">
            <table class="table table-bordered table-striped sort-tbl">
                <thead>
                    <tr>
                        <th style="width:20px;">#</th>
                        <th>Title</th>
                        <th>Collection / Accessories</th>
                        <th>Linked To Footer</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($categories as $key => $cat) { ?>
                        <tr>
                            <td><?php echo $cat['id']; ?></td>
                            <td style="max-width:400px;"><?php echo $cat['name']; ?></td>
                            <td style="max-width:400px;"><?php if ($cat['type']==1) { echo "Collection"; } elseif($cat['type']==2) { echo "Accessories"; } ?></td>
                            <td style="max-width:400px;"><?php if ($cat['linktofooter']==1) { echo "Yes (Block 1)"; } elseif($cat['linktofooter']==2) { echo "Yes (Block 2)"; } else { echo "No"; } ?></td>
                            <td style="max-width:400px;"><?php if ($cat['status']==1) { echo "Active"; } else { echo "Inactive"; } ?></td>
                            <td>
                                <a href="<?php echo site_url('categories/edit/'.$cat['id']); ?>" class="btn btn-xs btn-primary"><i class="icon-pencil6"></i> Edit</a>
                                <a href="<?php echo site_url('categories/delete/'.$cat['id']); ?>" class="btn btn-xs btn-danger"><i class="icon-chart"></i> Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php }else{ ?>
            <div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
                <h6 class="alert-heading text-semibold">No Categories Added</h6>
            </div>
        <?php } ?>