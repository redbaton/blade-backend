<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-image2 position-left"></i> <span class="text-semibold">Pop-Up Management</span></h4>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?= alert(); ?>
    <div class="panel panel-flat">
        <div class="panel-body">

            <form action="<?php echo site_url('popup'); ?>" method="POST" class="form-validate" enctype='multipart/form-data'>               

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Image</label>

                            <div class="thumbnail">
                                <div class="thumb">
                                    <?php if(!empty($details['img'])){  ?>
                                        <img src="<?php echo base_url('assets/admin/images/'.$details['img']); ?>" alt="<?= $details['img']; ?>">
                                    <?php }else{ ?>
                                        <img src="<?php echo base_url('assets/admin/images/placeholder.jpg'); ?>" alt="">
                                    <?php } ?>
                                    <div class="caption-overflow">
                                        <span>
                                            <a href="javascript:void(0);" onclick="$('[name=image]').trigger('click');" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">Change Image</a><br>
                                            <small class="text-muted"><small><small>Size: 471 x 522</small><br/><small>PNG, JPEG, JPG</small></small></small>
                                        </span>
                                    </div>
                                </div>
                                <input type="file" name="image" accept="image/*" style="display:none;" />
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Heading</label>
                            <textarea name="heading" class="form-control" rows="4" cols="4" required><?php if(!empty($details['heading'])){ echo $details['heading']; } ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Sub-Heading</label>
                            <textarea name="subhead" class="form-control" rows="4" cols="4" required><?php if(!empty($details['subhead'])){ echo $details['subhead']; } ?></textarea>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label class="control-label">Status</label>
                            <div class="checkbox checkbox-switch">
                                    <input type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Active" data-off-text="Disabled" class="switch" <?php echo ($details['status']==1)?'checked="checked"':''; ?> name="status" />
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <input name="id" type="hidden" value="<?php if(!empty($details['id'])){ echo $details['id']; } ?>">
                                <button class="btn btn-primary pull-right" style="margin-left:10px;" type="submit">Submit <i class="icon-arrow-right14 position-right"></i></button>
                                <button type="reset" class="btn btn-default pull-left">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<script>
    $(function(){
        $(".switch").bootstrapSwitch();

        $(document).on('change','[type="file"]',function(){
            var $tis = $(this);
            if (this.files && this.files[0]){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $tis.parents('.thumbnail').find('img').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>