<table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;border-spacing:0;padding-right:0;padding-bottom:0;padding-left:0;margin-left:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;height:100%;width:100%;background-color:#edeff0">
  <tbody><tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td style="padding-right:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;margin-left:0;border-collapse:collapse;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;min-width:16px;background-color:#edeff0">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" align="center" style="margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;border-collapse:collapse;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;padding-top:0;width:100%;padding-left:0;padding-right:0;height:100%;background-color:#edeff0;padding-bottom:0px"><br><br>
                     
          <table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding-right:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;margin-left:0;border-collapse:collapse;border-spacing:0;width:100%;min-width:576px">
        <tbody>
        <tr>
          <td style="padding-right:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;margin-left:0;border-collapse:collapse;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;min-width:16px;background-color:#edeff0">&nbsp;</td>
          <td align="center" style="border-collapse:collapse;margin-top:0;margin-right:0;margin-bottom:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;border-spacing:0;margin-left:0;width:512px;padding-right:32px;padding-left:32px;padding-bottom:4px;padding-top:32px;background-color:#ffffff;border-left:1px solid #d4dce2;border-right:1px solid #d4dce2;border-top:1px solid #d4dce2;"><table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding-left:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;margin-left:0;padding-right:0;border-collapse:collapse;border-spacing:0">
              <tbody><tr>
                <td align="center" style="border-collapse:collapse;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;margin-left:0;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;height:38px;font-weight:bold;color:#ffffff;font-size:0"><a style="font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;padding-top:0;padding-left:0;padding-right:0;display:inline-block;text-decoration:none;padding-bottom:0;min-height:38px;text-align:center;font-size:18px;font-weight:bold;line-height:0;color:#ffffff" href="<?php echo site_url(); ?>"><img style="padding-left:0;margin-left:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;margin-top:0;padding-right:0;text-decoration:none;line-height:100%;outline:none;border:none;display:block;vertical-align:top;" src="<?php echo base_url('assets/admin/images/black-logo.png'); ?>" alt=""></a></td>
                
              </tr>
            </tbody></table></td>
          <td style="padding-right:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;margin-left:0;border-collapse:collapse;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;min-width:16px;background-color:#edeff0">&nbsp;</td>
        </tr>
        <tr>
          <td style="padding-right:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;margin-left:0;border-collapse:collapse;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;min-width:16px;background-color:#edeff0">&nbsp;</td>
          <td style="border-spacing:0;margin-top:0;margin-right:0;margin-bottom:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;margin-left:0;border-collapse:collapse;padding-bottom:0;width:512px;background-color:#ffffff;color:#586778;padding-right:32px;padding-left:32px;border-left:1px solid #d4dce2;border-right:1px solid #d4dce2;font-size:16px;line-height:24px;padding-top:24px">
           <p style="border-top:1px solid #e6eef4;padding-top:24px;">[message]<p><br>
Regards,<br>
<a style="color:#586778;text-decoration:none;">Blade Watches</a></p></td><td style="padding-right:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;margin-left:0;border-collapse:collapse;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;min-width:16px;background-color:#edeff0">&nbsp;</td>
        </tr>        
        <tr><td style="padding-right:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;margin-left:0;border-collapse:collapse;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;min-width:16px;background-color:#edeff0">&nbsp;</td>
          <td style="border-spacing:0;margin-top:0;margin-right:0;margin-bottom:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;margin-left:0;border-collapse:collapse;padding-bottom:0;width:512px;background-color:#ffffff;color:#586778;padding-right:32px;padding-left:32px;border-left:1px solid #d4dce2;border-right:1px solid #d4dce2;border-bottom:1px solid #d4dce2;font-size:16px;line-height:24px;padding-top:0px"><br><br></td>
          <td style="padding-right:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;margin-left:0;border-collapse:collapse;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;min-width:16px;background-color:#edeff0">&nbsp;</td>
        </tr>
  <tr>
    <td style="padding-right:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;margin-left:0;border-collapse:collapse;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;min-width:16px;background-color:#edeff0">&nbsp;</td>
    <td style="border-spacing:0;margin-top:0;margin-right:0;margin-bottom:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;padding-bottom:0;padding-left:0;padding-right:0;border-collapse:collapse;margin-left:0;padding-top:14px;text-align:center;font-size:12px;line-height:21px;width:544px;color:#7f8a97;background-color:#edeff0">
      &copy; Blade Watches. All Rights Reserved.
      <br>
      <br>
    </td>
    <td style="padding-right:0;margin-top:0;margin-right:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-left:0;margin-left:0;border-collapse:collapse;border-spacing:0;font-family:Tahoma,'Lucida Grande','Lucida Sans',Helvetica,Arial,sans-serif;min-width:16px;background-color:#edeff0">&nbsp;</td>
  </tr>
      </tbody></table>
</td></tr></tbody></table>