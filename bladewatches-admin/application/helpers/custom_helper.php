<?php 

function alert()
{
    $CI =& get_instance();
    
    if($CI->session->flashdata('alert_success')){
        // echo '<div class="alert alert-success alert-auto-close"><button data-dismiss="alert" class="close" type="button">[close 10]</button> '.$CI->session->flashdata('alert_success')."</div>";

        echo '<script type="text/javascript">
                $(function(){
                    swal({
                        title: "Success!",
                        text: "'.$CI->session->flashdata('alert_success').'",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                    });
                });
            </script>';
    }

    if($CI->session->flashdata('alert_msg_success')){
        echo '<div class="alert alert-success alert-auto-close"><button data-dismiss="alert" class="close" type="button">[close 10]<span class="sr-only">Close</span></button>
        '.$CI->session->flashdata('alert_msg_success')."</div>";
        echo "<script>
            var rem = 10;
            function closealert(){
                if(rem==0){
                    $('.alert-auto-close').fadeOut();
                }else{
                    setTimeout(function(){
                        rem = rem - 1;
                        $('.alert-auto-close .close').html('[Close '+rem+']');
                        closealert();
                    },1000);
                }
            }
            closealert();
        </script>";
    }

    if($CI->session->flashdata('alert_error')){
        echo '<div class="alert alert-danger alert-auto-close"><button data-dismiss="alert" class="close" type="button">[close 10]<span class="sr-only">Close</span></button>
        '.$CI->session->flashdata('alert_error')."</div>";
        echo "<script>
            var rem = 10;
            function closealert(){
                if(rem==0){
                    $('.alert-auto-close').fadeOut();
                }else{
                    setTimeout(function(){
                        rem = rem - 1;
                        $('.alert-auto-close .close').html('[Close '+rem+']');
                        closealert();
                    },1000);
                }
            }
            closealert();
        </script>";
    }

}

function timeago($date) {
   $timestamp = strtotime($date);   
   
   $strTime = array("SECOND", "MINUTE", "HOUR", "DAY", "MONTH", "YEAR");
   $length = array("60","60","24","30","12","10");

   $currentTime = time();
   if($currentTime >= $timestamp) {
        $diff     = time()- $timestamp;
        for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
        $diff = $diff / $length[$i];
        }

        $diff = round($diff);
        return $diff . "</p><p>" . $strTime[$i] . "(S) AGO ";
   }
}

// function setting($key)
// {
//     $CI =& get_instance();
//     return $CI->settings_model->get_setting($key);
// }

// function process_paging($total_records,$page,$url,$admin=false)
// {
//     $CI = get_instance();

//     $config['base_url'] = $url;
//     $config['total_rows'] = $total_records;
//     $config['per_page'] = $page; 

//     $CI->pagination->initialize($config); 

//     return $CI->pagination->create_links();

//     // $CI->pagination->total = $total_records;
//     // $CI->pagination->page = $page;
//     //     $CI->pagination->two_col = true;

//     // if($admin){
//     //     $CI->pagination->limit = $CI->config->item("admin_items_perpage");
//     // }
//     // else
//     //     $CI->pagination->limit = $CI->config->item("items_perpage");
    
//     // $CI->pagination->text = 'Showing {start} to {end} of {total} ({pages} Pages)';
//     // $CI->pagination->url = $url;
    
//     // return $CI->pagination->render();
// }

// function admin_details($field)
// {
//     $CI =& get_instance();
//     $admin_user_id = $CI->session->userdata("admin_user_id");

//     $data['user_info'] = $CI->admin_model->admin_user($admin_user_id);
    
//     if($field=='image'){
//         if(!empty($data['user_info']['image']))
//             return base_url('assets/admin/images/'.$data['user_info']['image']);
//         else
//             return base_url('assets/admin/images/placeholder.jpg');
//     }else{
//         return $data['user_info'][$field];
//     }
// }

function url_params_to_url()
{
    $get_params = '';
    if(!empty($_GET)){
        $i = 0;
        foreach ($_GET as $key => $value) {
            if($key=='start' && empty($value))
            {}else{
                $get_params .= ($i==0?"?":"&").$key."=".$value;
                $i++;
            }
        }
    }
    return $get_params;
}

?>
