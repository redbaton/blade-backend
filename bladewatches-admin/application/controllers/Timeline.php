<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Timeline extends AdminController 
{
	function __construct()
    {
        parent::__construct();
	}

	function index()
	{
		$data['timelines'] = $this->timeline_model->get_data();
    	
        $this->load->view('admin/common/header');
		$this->load->view('admin/timeline/edit', $data);
		$this->load->view('admin/common/footer');
	}

    function deletedata(){
        $this->db->where("id", $this->input->post('id'))->delete("timeline");
    }

	function upload($id=false) {
		$save = $this->input->post();

		if(!empty($_FILES['image']['name']))
		{
			$config['upload_path'] = './assets/admin/images/timeline/';
            $config['allowed_types'] = 'jpg|jpeg|png|svg';
            $config['file_name'] = $_FILES['image']['name'];
			$config['overwrite']     = FALSE;

            $this->load->library('upload', $config);
            
            if($this->upload->do_upload('image'))
            {
                $image_details = $this->upload->data();
                $save['image'] = $image_details['file_name'];
            }
		}

		$id = $this->timeline_model->save_timeline_data_model($save);
	}

	function upload_update() {
		$save = $this->input->post();
		$save['id'] = $this->input->post('id');
		

		if(!empty($_FILES['image']['name']))
		{
			$config['upload_path'] = './assets/admin/images/timeline/';
            $config['allowed_types'] = 'jpg|jpeg|png|svg';
            $config['file_name'] = $_FILES['image']['name'];
			$config['overwrite']     = FALSE;

            $this->load->library('upload', $config);
            
            if($this->upload->do_upload('image'))
            {
                $image_details = $this->upload->data();
                $save['image'] = $image_details['file_name'];
            }
		}

		$id = $this->timeline_model->update_timeline_data_model($save);
	}
}

?>