<?php 

class Products extends AdminController 
{
	function __construct()
    {
        parent::__construct();
	}

	function index()
	{
		$data['products'] = $this->products_model->get_data();
		$data['collections'] = $this->products_model->get_cat("1");
		$data['accessories'] = $this->products_model->get_cat("2");

        $this->load->view('admin/common/header');
		$this->load->view('admin/products/list',$data);
		$this->load->view('admin/common/footer');
	}

	function create()
	{	
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save['name'] = $this->input->post('name');
			$save['slug'] = strtolower(preg_replace('/-+/', '-', preg_replace('/[^\wáéíóú]/', '-', $save['name'])));
			$save['metadesc'] = $this->input->post('metadesc');
			$save['metakeys'] = $this->input->post('metakeys');
			$save['excerpt'] = $this->input->post('excerpt');
			$save['attributes'] = $this->input->post('attributes');
			$save['collections'] = $this->input->post('collections');
			$save['accessories'] = $this->input->post('accessories');
			$save['status'] = $this->input->post('status')=='on' ? 1 : 0;

			if(!empty($_FILES['image1']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image1']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image1'))
                {
                    $image_details = $this->upload->data();
                    $save['image1'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['hover1']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['hover1']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('hover1'))
                {
                    $image_details = $this->upload->data();
                    $save['hover1'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['image2']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image2']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image2'))
                {
                    $image_details = $this->upload->data();
                    $save['image2'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['hover2']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['hover2']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('hover2'))
                {
                    $image_details = $this->upload->data();
                    $save['hover2'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['image3']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image3']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image3'))
                {
                    $image_details = $this->upload->data();
                    $save['image3'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['hover3']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['hover3']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('hover3'))
                {
                    $image_details = $this->upload->data();
                    $save['hover3'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['image4']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image4']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image4'))
                {
                    $image_details = $this->upload->data();
                    $save['image4'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['hover4']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['hover4']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('hover4'))
                {
                    $image_details = $this->upload->data();
                    $save['hover4'] = $image_details['file_name'];
                }
			}


			$id = $this->products_model->save_product_model($save);

			if ($id) {
				$this->session->set_flashdata('alert_success','Product Added successfully!');
				return redirect('products');
			}
		}
		$data['collections'] = $this->products_model->get_cat("1");
		$data['accessories'] = $this->products_model->get_cat("2");

        $this->load->view('admin/common/header');
		$this->load->view('admin/products/create', $data);
		$this->load->view('admin/common/footer');
	}
    
    function delete($id){
        $id = $this->products_model->delete_product_model($id);
        if($id)
			$this->session->set_flashdata('alert_success','Product Deleted successfully!');
        return redirect('products');
    }
    
	function edit($id)
	{	
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save['id'] = $id;
			$save['name'] = $this->input->post('name');
			$save['slug'] = strtolower(preg_replace('/-+/', '-', preg_replace('/[^\wáéíóú]/', '-', $save['name'])));
			$save['metadesc'] = $this->input->post('metadesc');
			$save['metakeys'] = $this->input->post('metakeys');
			$save['excerpt'] = $this->input->post('excerpt');
			$save['attributes'] = $this->input->post('attributes');
			$save['collections'] = $this->input->post('collections');
			$save['accessories'] = $this->input->post('accessories');
			$save['status'] = $this->input->post('status')=='on' ? 1 : 0;

			if(!empty($_FILES['image1']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image1']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image1'))
                {
                    $image_details = $this->upload->data();
                    $save['image1'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['hover1']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['hover1']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('hover1'))
                {
                    $image_details = $this->upload->data();
                    $save['hover1'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['image2']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image2']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image2'))
                {
                    $image_details = $this->upload->data();
                    $save['image2'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['hover2']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['hover2']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('hover2'))
                {
                    $image_details = $this->upload->data();
                    $save['hover2'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['image3']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image3']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image3'))
                {
                    $image_details = $this->upload->data();
                    $save['image3'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['hover3']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['hover3']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('hover3'))
                {
                    $image_details = $this->upload->data();
                    $save['hover3'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['image4']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image4']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image4'))
                {
                    $image_details = $this->upload->data();
                    $save['image4'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['hover4']['name']))
			{
				$config['upload_path'] = './assets/admin/images/products';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['hover4']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('hover4'))
                {
                    $image_details = $this->upload->data();
                    $save['hover4'] = $image_details['file_name'];
                }
			}


			$id = $this->products_model->save_product_model($save);

			if ($id) {
				$this->session->set_flashdata('alert_success','Product Edited successfully!');
				return redirect('products');
			}
		}

		$data['product'] = $this->products_model->get_product_model_id($id);
		$data['collections'] = $this->products_model->get_cat("1");
		$data['accessories'] = $this->products_model->get_cat("2");
    	
        $this->load->view('admin/common/header');
		$this->load->view('admin/products/edit', $data);
		$this->load->view('admin/common/footer');
	}
}

?>