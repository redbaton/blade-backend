<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Popup extends AdminController 
{
	function __construct()
    {
        parent::__construct();
	}

	function index()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save = $this->input->post();
			$save['status'] = $this->input->post('status')=='on' ? 1 : 0;
            
			if(!empty($_FILES['image']['name']))
			{
				$config['upload_path'] = './assets/admin/images/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image'))
                {
                    $image_details = $this->upload->data();
                    $save['img'] = $image_details['file_name'];
                }
			}
            
			$id = $this->popup_model->save_popup_model($save);

			if ($id) {
				$this->session->set_flashdata('alert_success','Data Saved successfully!');
				return redirect('popup');
			}
		}

		$data['details'] = $this->popup_model->get_data();

        $this->load->view('admin/common/header');
		$this->load->view('admin/popup/list', $data);
		$this->load->view('admin/common/footer');
	}
}

?>