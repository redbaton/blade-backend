<?php 

class Socialfeed extends AdminController 
{
	function __construct()
    {
        parent::__construct();
	}

	function index()
	{
		$data['images'] = $this->socialfeed_model->get_images();
    	
        $this->load->view('admin/common/header');
		$this->load->view('admin/socialfeed/edit', $data);
		$this->load->view('admin/common/footer');
	}

    function deletedata(){
        $this->db->where("id", $this->input->post('id'))->delete("socialfeed");
    }

	function upload($id=false) {
		$save = $this->input->post();

		if(!empty($_FILES['image']['name']))
		{
			$config['upload_path'] = './assets/admin/images/social/';
            $config['allowed_types'] = 'jpg|jpeg|png|svg';
            $config['file_name'] = $_FILES['image']['name'];
			$config['overwrite']     = FALSE;

            $this->load->library('upload', $config);
            
            if($this->upload->do_upload('image'))
            {
                $image_details = $this->upload->data();
                $save['image'] = $image_details['file_name'];
            }
			$id = $this->socialfeed_model->save_images_data_model($save);
		}
	}

	function upload_update() {
		$save = $this->input->post();
		$save['id'] = $this->input->post('id');
		

		if(!empty($_FILES['image']['name']))
		{
			$config['upload_path'] = './assets/admin/images/social/';
            $config['allowed_types'] = 'jpg|jpeg|png|svg';
            $config['file_name'] = $_FILES['image']['name'];
			$config['overwrite']     = FALSE;

            $this->load->library('upload', $config);
            
            if($this->upload->do_upload('image'))
            {
                $image_details = $this->upload->data();
                $save['image'] = $image_details['file_name'];
            }
		}

		$id = $this->socialfeed_model->update_image_data_model($save);
	}
}

?>