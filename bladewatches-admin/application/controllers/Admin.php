<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends AdminController {
	
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		if($this->session->userdata("admin_user_id")) {
			return redirect('admin/profile');
		} else {
			$data['meta_title'] = $this->admin_model->details('site_title');
			$data['fav_icon'] = $this->admin_model->details('favicon');
			return $this->load->view('admin/account/login',$data);
		}
	}

	public function login()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			if($admin_info = $this->admin_model->login($email,$password)){
				$this->session->set_userdata("admin_user_id",$admin_info['ID']);
				
				if($this->session->userdata('admin_redirect_link')){
					$redirect_link = $this->session->userdata('admin_redirect_link');
					$this->session->unset_userdata('admin_redirect_link');
					return redirect($redirect_link);
				}

				$this->session->set_userdata('collapse_side_bar','');
				return redirect('admin/profile');
			}else{
				$this->session->set_flashdata("alert_error","Invalid Email or Password!");
				return redirect('admin');
			}
		}
	}

	public function forgot_password()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$email = $this->input->post('email');

			if($admin_info = $this->admin_model->admin_user('uemail', $email))
			{
				$time_now = time();
				$this->db->where('meta_key', 'reset_pw_key') -> update('settings', array('meta_value' => md5($time_now)));
				$this->db->where('meta_key', 'reset_pw_time') -> update('settings', array('meta_value' => $time_now));

				$message = "Hi ".$admin_info['fname'].",</p>
					<p>Please click the following link to reset your password.</p><p>
					<a style='text-decoration:none;color:#fff;background-color:#b25810;padding:10px;border-radius:5px;' href='".site_url('admin/reset-password/'.md5($time_now))."' target='_blank'>Reset Password</a></p>
					<p><br>
					<small style='color:#acacac;'>The above link will be available for only 2 hours.<br>if you didn't requested to reset password, please ignore this mail.</small>
				";
				$email_template = $this->load->view('email/template',array(),true);
				send_email(array(
					'receiver_email' => $admin_info['uemail'],
					'from_name' => 'BladeWatches',
					'from_email' => $this->admin_model->details('contemail'),
					'subject' => 'Password Reset Request',
					'message' => str_replace('[message]', $message, $email_template)
				));

				$this->session->set_flashdata("alert_msg_success","Password reset link sent !");
				return redirect('admin/forgot-password');
			}else{
				$this->session->set_flashdata("alert_error","Incorrect Email id !");
				return redirect('admin/forgot-password');
			}
		}

		$data['meta_title'] = $this->admin_model->details('site_title');
		$data['fav_icon'] = $this->admin_model->details('favicon');
		$this->load->view('admin/account/forgotpassword',$data);
	}

	public function reset_password($reset_pw_key=false)
	{
		$user_info = $this->db->where(array('meta_key'=>'reset_pw_key', 'meta_value'=>$reset_pw_key)) -> get('settings')->row_array();

		if(!empty($user_info['meta_value']))
		{
			$rq_time = $this->db->where('meta_key', 'reset_pw_time') -> get('settings') -> row() -> meta_value;
			$valid = mktime(date('H',$rq_time)+2, date('i',$rq_time), date('s',$rq_time), date('m',$rq_time), date('d',$rq_time), date('Y',$rq_time));

			if(time() <= $valid)
			{
				if($this->input->server('REQUEST_METHOD') === 'POST')
				{
					if(empty($_POST['password']) || empty($_POST['re_password']) ){
						$this->session->set_flashdata("alert_error","Please fill all the fields");			
						return redirect(current_url());
					}

					if($this->input->post('password') != $this->input->post('re_password')){
						$this->session->set_flashdata("alert_match","Passwords don't match");			
						return redirect(current_url());
					}else{
						$this->db->where('role', 'admin') -> update('users', array('upass' => md5($this->input->post('password'))));
						$this->db->where('meta_key', 'reset_pw_key') -> update('settings', array('meta_value' => ''));
						$this->db->where('meta_key', 'reset_pw_time') -> update('settings', array('meta_value' => ''));

						$this->session->set_flashdata("alert_msg_success","Password Changed Successfully!");
						return redirect('admin');
					}
				}

				$data['meta_title'] = $this->admin_model->details('site_title');
				$data['fav_icon'] = $this->admin_model->details('favicon');
				$this->load->view('admin/account/reset_password',$data);
			}else
				echo "Reset link has been expired !";
		}else
			echo "Invalid reset link !";
	}

	public function password()
	{
		$data['user_info'] = $this->admin_model->admin_user('ID', $this->session->userdata("admin_user_id"));

		if($this->input->post('new_password') != $this->input->post('conf_password')){
			$this->session->set_flashdata("alert_error","New password and Confirmation password do not match");			
			return redirect("admin/profile");
		}

		if(md5($this->input->post('old_password')) == $data['user_info']['upass']){
			$this->db->where("ID", $this->session->userdata("admin_user_id")) -> update("users",array("upass"=>md5($this->input->post('new_password'))));

			$this->session->set_flashdata("alert_success","Password Changed Successfully!");			
			return redirect("admin/profile");
		}else{
			$this->session->set_flashdata("alert_error","Incorrect old password");			
			return redirect("admin/profile");
		}
	}

	public function logout()
	{
		$this->session->unset_userdata("admin_user_id");
		return redirect('admin');
	}

	public function profile() {
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save['admin_user_id'] = $this->session->userdata("admin_user_id");

			$save['firstname'] = $this->input->post('firstname');
			$save['lastname'] = $this->input->post('lastname');
			$save['email'] = $this->input->post('email');
			$save['contemail'] = $this->input->post('contemail');
			$save['copyrights'] = $this->input->post('copyrights');
			$save['sitetitle'] = $this->input->post('sitetitle');
			$save['sitelogo'] = $this->admin_model->details('main_logo');
			$save['scrolllogo'] = $this->admin_model->details('scroll_logo');
			$save['footlogo'] = $this->admin_model->details('footer_logo');
			$save['favlogo'] = $this->admin_model->details('favicon');

			if(!empty($_FILES['sitelogo']['name'])){
				$upload_path = './assets/admin/images';

				$config['upload_path'] = $upload_path;
				$config['allowed_types'] = 'jpg|png|jpeg|svg';
				$config['overwrite']     = FALSE;
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('sitelogo')){
					$this->session->set_flashdata("alert_error",$this->upload->display_errors());
				}else{
					$image_info = $this->upload->data();
					$save['sitelogo'] = $image_info['file_name'];
				}
			}

			if(!empty($_FILES['scrolllogo']['name'])){
				$upload_path = './assets/admin/images';

				$config['upload_path'] = $upload_path;
				$config['allowed_types'] = 'jpg|png|jpeg|svg';
				$config['overwrite']     = FALSE;
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('scrolllogo')){
					$this->session->set_flashdata("alert_error",$this->upload->display_errors());
				}else{
					$image_info = $this->upload->data();
					$save['scrolllogo'] = $image_info['file_name'];
				}
			}

			if(!empty($_FILES['footlogo']['name'])){
				$upload_path = './assets/admin/images';

				$config['upload_path'] = $upload_path;
				$config['allowed_types'] = 'jpg|png|jpeg|svg';
				$config['overwrite']     = FALSE;
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('footlogo')){
					$this->session->set_flashdata("alert_error",$this->upload->display_errors());
				}else{
					$image_info = $this->upload->data();
					$save['footlogo'] = $image_info['file_name'];
				}
			}

			if(!empty($_FILES['favlogo']['name'])){
				$upload_path = './assets/admin/images';

				$config['upload_path'] = $upload_path;
				$config['allowed_types'] = 'ico';
				$config['overwrite']     = FALSE;
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('favlogo')){
					$this->session->set_flashdata("alert_error",$this->upload->display_errors());
				}else{
					$image_info = $this->upload->data();
					$save['favlogo'] = $image_info['file_name'];
				}
			}

			if ( $this->admin_model->save_admin_user($save) ) {
			 	$this->session->set_flashdata("alert_success","Changes saved Successfully!");
				return redirect('admin/profile');
			} 
		}

		$data['user_info'] = $this->admin_model->admin_user('ID', $this->session->userdata("admin_user_id"));

		$this->load->view('admin/common/header');
		$this->load->view('admin/account/profile_form',$data);
		$this->load->view('admin/common/footer');
	}

	public function social()
	{
		$save = $this->input->post();

		if ( $this->admin_model->save_social($save) ) {
		 	$this->session->set_flashdata("alert_success","Changes saved Successfully!");
			return redirect('admin/profile');
		}
	}

	public function contactdetail()
	{
		$save = $this->input->post();

		if ( $this->admin_model->save_contactdetail($save) ) {
		 	$this->session->set_flashdata("alert_success","Changes saved Successfully!");
			return redirect('admin/profile');
		}
	}

	public function toggle_sidebar()
	{
		$side_bar = $this->session->userdata('collapse_side_bar');
		if($side_bar)
			$this->session->set_userdata('collapse_side_bar','');
		else
			$this->session->set_userdata('collapse_side_bar','sidebar-xs');
	}
}

?>