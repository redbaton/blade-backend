<?php 

class Categories extends AdminController 
{
	function __construct()
    {
        parent::__construct();
	}

	function index()
	{
		$data['categories'] = $this->categories_model->get_data();

        $this->load->view('admin/common/header');
		$this->load->view('admin/categories/list',$data);
		$this->load->view('admin/common/footer');
	}

	function create()
	{	
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save['name'] = $this->input->post('name');
			$save['slug'] = strtolower(preg_replace('/-+/', '-', preg_replace('/[^\wáéíóú]/', '-', $save['name'])));
			$save['metadesc'] = $this->input->post('metadesc');
			$save['metakeys'] = $this->input->post('metakeys');
			$save['excerpt'] = $this->input->post('excerpt');
			$save['type'] = $this->input->post('type');
			$save['linktofooter'] = $this->input->post('linktofooter');
			$save['status'] = $this->input->post('status')=='on' ? 1 : 0;

			if(!empty($_FILES['image']['name']))
			{
				$config['upload_path'] = './assets/admin/images/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image'))
                {
                    $image_details = $this->upload->data();
                    $save['image'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['banimage']['name']))
			{
				$config['upload_path'] = './assets/admin/images/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['banimage']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('banimage'))
                {
                    $image_details = $this->upload->data();
                    $save['banimage'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['mbanimage']['name']))
			{
				$config['upload_path'] = './assets/admin/images/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['mbanimage']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('mbanimage'))
                {
                    $image_details = $this->upload->data();
                    $save['mbanimage'] = $image_details['file_name'];
                }
			}


			$id = $this->categories_model->save_cat_model($save);

			if ($id) {
				$this->session->set_flashdata('alert_success','Category Added successfully!');
				return redirect('categories');
			}
		}

        $this->load->view('admin/common/header');
		$this->load->view('admin/categories/create');
		$this->load->view('admin/common/footer');
	}
    
    function delete($id){
        $id = $this->categories_model->delete_cat_model($id);
        if($id)
			$this->session->set_flashdata('alert_success','Category Deleted successfully!');
        return redirect('categories');
    }
    
	function edit($id)
	{	
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save['id'] = $this->input->post('id');
			$save['name'] = $this->input->post('name');
			$save['slug'] = strtolower(preg_replace('/-+/', '-', preg_replace('/[^\wáéíóú]/', '-', $save['name'])));
			$save['metadesc'] = $this->input->post('metadesc');
			$save['metakeys'] = $this->input->post('metakeys');
			$save['excerpt'] = $this->input->post('excerpt');
			$save['type'] = $this->input->post('type');
			$save['linktofooter'] = $this->input->post('linktofooter');
			$save['status'] = $this->input->post('status')=='on' ? 1 : 0;

			if(!empty($_FILES['image']['name']))
			{
				$config['upload_path'] = './assets/admin/images/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['image']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image'))
                {
                    $image_details = $this->upload->data();
                    $save['image'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['banimage']['name']))
			{
				$config['upload_path'] = './assets/admin/images/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['banimage']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('banimage'))
                {
                    $image_details = $this->upload->data();
                    $save['banimage'] = $image_details['file_name'];
                }
			}

			if(!empty($_FILES['mbanimage']['name']))
			{
				$config['upload_path'] = './assets/admin/images/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $_FILES['mbanimage']['name'];
				$config['overwrite']     = FALSE;

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('mbanimage'))
                {
                    $image_details = $this->upload->data();
                    $save['mbanimage'] = $image_details['file_name'];
                }
			}

			$id = $this->categories_model->save_cat_model($save);

			if ($id) {
				$this->session->set_flashdata('alert_success','Category Edited successfully!');
				return redirect('categories');
			}
		}

		$data['cat'] = $this->categories_model->get_cat_model_id($id);
    	
        $this->load->view('admin/common/header');
		$this->load->view('admin/categories/edit', $data);
		$this->load->view('admin/common/footer');
	}
}

?>