<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends AdminController 
{
	function __construct()
    {
        parent::__construct();
	}

	function home()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save = $this->input->post();

			if(!empty($_FILES['bannerImg']['name'])){
				$upload_path = './assets/admin/images/pages';

				$config['upload_path'] = $upload_path;
				$config['allowed_types'] = 'jpg|png|jpeg|svg';
				$config['overwrite']     = FALSE;
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('bannerImg')){
					$this->session->set_flashdata("alert_error",$this->upload->display_errors());
				}else{
					$image_info = $this->upload->data();
					$save['bannerImg'] = $image_info['file_name'];
				}
			}

			$data = json_encode($save);

			$tosend = array('name' => 'Home', 'slug' => 'home', 'contents' => $data);

			$id = $this->pages_model->save_page_model($tosend);
            
			$this->session->set_flashdata('alert_success','Contents Updated Successfully!');
			return redirect('pages/home');
		}

		$data['home'] = $this->pages_model->get_page_model('home');

        $this->load->view('admin/common/header');
		$this->load->view('admin/pages/home', $data);
		$this->load->view('admin/common/footer');
	}

	function about()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save = $this->input->post();

			if(!empty($_FILES['abtImg']['name'])){
				$upload_path = './assets/admin/images/pages';

				$config['upload_path'] = $upload_path;
				$config['allowed_types'] = 'jpg|png|jpeg|svg';
				$config['overwrite']     = FALSE;
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('abtImg')){
					$this->session->set_flashdata("alert_error",$this->upload->display_errors());
				}else{
					$image_info = $this->upload->data();
					$save['abtImg'] = $image_info['file_name'];
				}
			}

			$data = json_encode($save);

			$tosend = array('slug' => 'aboutus', 'contents' => $data);

			$id = $this->pages_model->save_page_model($tosend);
            
			$this->session->set_flashdata('alert_success','Contents Updated Successfully!');
			return redirect('pages/about');
		}

		$data['about'] = $this->pages_model->get_page_model('aboutus');

        $this->load->view('admin/common/header');
		$this->load->view('admin/pages/aboutus', $data);
		$this->load->view('admin/common/footer');
	}

	function contact()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save = $this->input->post();

			if(!empty($_FILES['contImg']['name'])){
				$upload_path = './assets/admin/images/pages';

				$config['upload_path'] = $upload_path;
				$config['allowed_types'] = 'jpg|png|jpeg|svg';
				$config['overwrite']     = FALSE;
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('contImg')){
					$this->session->set_flashdata("alert_error",$this->upload->display_errors());
				}else{
					$image_info = $this->upload->data();
					$save['contImg'] = $image_info['file_name'];
				}
			}

			$data = json_encode($save);

			$tosend = array('slug' => 'contactus', 'contents' => $data);

			$id = $this->pages_model->save_page_model($tosend);
            
			$this->session->set_flashdata('alert_success','Contents Updated Successfully!');
			return redirect('pages/contact');
		}

		$data['contact'] = $this->pages_model->get_page_model('contactus');

        $this->load->view('admin/common/header');
		$this->load->view('admin/pages/contactus', $data);
		$this->load->view('admin/common/footer');
	}
}

?>