<?php 

class Stores extends AdminController 
{
	function __construct()
    {
        parent::__construct();
	}

	function index()
	{
		$data['stores'] = $this->stores_model->get_data();

        $this->load->view('admin/common/header');
		$this->load->view('admin/stores/list',$data);
		$this->load->view('admin/common/footer');
	}

	function create()
	{	
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save = $this->input->post();

			if ($this->input->post('newcountry') !== null) {
				$save['country'] = $this->input->post('newcountry');
			}

			unset($save['newcountry']);
            
			$id = $this->stores_model->save_stores_model($save);

			if ($id) {
				$this->session->set_flashdata('alert_success','Store / Service Added successfully!');
				return redirect('stores');
			}
		}

		$data['stores_country'] = $this->stores_model->get_stores_country();

        $this->load->view('admin/common/header');
		$this->load->view('admin/stores/create', $data);
		$this->load->view('admin/common/footer');
	}
    
    function delete($id){
        $id = $this->stores_model->delete_stores_model($id);
        if($id)
				$this->session->set_flashdata('alert_success','Store / Service Deleted successfully!');
        redirect('stores');
    }
    
	function edit($id=false)
	{	
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$save = $this->input->post();

			if ($this->input->post('newcountry')!== null) {
				$save['country'] = $this->input->post('newcountry');
			}

			unset($save['newcountry']);
            
			$id = $this->stores_model->save_stores_model($save);

			if ($id) {
				$this->session->set_flashdata('alert_success','Store / Service Edited successfully!');
				return redirect('stores');
			}
		}

		$data['stores_country'] = $this->stores_model->get_stores_country();
		$data['storeDetail'] = $this->stores_model->get_store_id($id);

        $this->load->view('admin/common/header');
		$this->load->view('admin/stores/edit', $data);
		$this->load->view('admin/common/footer');
	}
}

?>