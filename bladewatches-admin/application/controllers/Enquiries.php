<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiries extends AdminController 
{
	function __construct()
    {
        parent::__construct();
	}

	function offers()
	{
		$data['offers'] = $this->enquiries_model->get_offers();

        $this->load->view('admin/common/header');
		$this->load->view('admin/enquiries/list', $data);
		$this->load->view('admin/common/footer');
	}

	function newsletter()
	{
		$data['newsletter'] = $this->enquiries_model->get_newsletter();

        $this->load->view('admin/common/header');
		$this->load->view('admin/enquiries/list', $data);
		$this->load->view('admin/common/footer');
	}

	function enquiry()
	{
		$data['enquiries'] = $this->enquiries_model->get_enquiries();

        $this->load->view('admin/common/header');
		$this->load->view('admin/enquiries/list', $data);
		$this->load->view('admin/common/footer');
	}

	function delete($table, $id) {
		$this->enquiries_model->delete_entry($table, $id);
		$this->session->set_flashdata('alert_success','Deleted successfully!');
        return redirect(base_url('enquiries/'.$table));
	}

	public function downloadfile($table)  {
        $data['meta_title']  = $table . 'Records Sheet';
        $data['ref_href'] = url_params_to_url();

        $subscribers = $this->db->get($table)->result_array();

        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Webeasystep.com ')
            ->setLastModifiedBy('BladeWatches')
            ->setTitle('BladeWatches Records')
            ->setSubject('Enquiries')
            ->setDescription('All Enquiries');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);

        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
        // auto fit column to content

        foreach(range('A','J') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1",'Id')
            ->setCellValue("B1",'Name')
            ->setCellValue("C1",'Company')
            ->setCellValue("D1",'Email')
            ->setCellValue("E1",'Contact')
            ->setCellValue("F1",'City')
            ->setCellValue("G1",'Country')
            ->setCellValue("H1",'Type')
            ->setCellValue("I1",'Message');

        // Add some data
        $x= 2;
        foreach($subscribers as $sub){
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x",$sub['id'])
            	->setCellValue("B$x", isset($sub['name']) ? $sub['name'] : '')
            	->setCellValue("C$x", isset($sub['company']) ? $sub['company'] : '')
            	->setCellValue("D$x", isset($sub['email']) ? $sub['email'] : '')
            	->setCellValue("E$x", isset($sub['cont']) ? $sub['cont'] : '')
            	->setCellValue("F$x", isset($sub['city']) ? $sub['city'] : '')
            	->setCellValue("G$x", isset($sub['country']) ? $sub['country'] : '')
            	->setCellValue("H$x", isset($sub['type']) ? $sub['type'] : '')
            	->setCellValue("I$x", isset($sub['message']) ? $sub['message'] : '');
            $x++;
        }



        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle($table . ' Enquiries');

        // set right to left direction
        //      $spreadsheet->getActiveSheet()->setRightToLeft(true);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Enquiries_sheet.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

    }
}

?>