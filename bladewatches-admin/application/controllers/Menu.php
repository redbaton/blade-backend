<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends AdminController 
{
	function __construct()
    {
        parent::__construct();
	}

	function index()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->db->where('meta_key', 'menu_1') -> update('settings', array('meta_value' => $this->input->post('menu_1')));
			$this->db->where('meta_key', 'menu_2') -> update('settings', array('meta_value' => $this->input->post('menu_2')));
			$this->db->where('meta_key', 'menu_3') -> update('settings', array('meta_value' => $this->input->post('menu_3')));
            $this->db->where('meta_key', 'menu_4') -> update('settings', array('meta_value' => $this->input->post('menu_4')));
            $this->db->where('meta_key', 'menu_5') -> update('settings', array('meta_value' => $this->input->post('menu_5')));
			$this->db->where('meta_key', 'menu_1_link') -> update('settings', array('meta_value' => $this->input->post('menu_1_link')));
			$this->db->where('meta_key', 'menu_2_link') -> update('settings', array('meta_value' => $this->input->post('menu_2_link')));
			$this->db->where('meta_key', 'menu_3_link') -> update('settings', array('meta_value' => $this->input->post('menu_3_link')));
            $this->db->where('meta_key', 'menu_4_link') -> update('settings', array('meta_value' => $this->input->post('menu_4_link')));
            $this->db->where('meta_key', 'menu_5_link') -> update('settings', array('meta_value' => $this->input->post('menu_5_link')));
            
			$this->session->set_flashdata('alert_success','Menu Updated successfully!');
			return redirect('menu');
		}

        $this->load->view('admin/common/header');
		$this->load->view('admin/menu/list');
		$this->load->view('admin/common/footer');
	}

	function footer()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->db->where('meta_key', 'foot_menu_1') -> update('settings', array('meta_value' => $this->input->post('foot_menu_1')));
			$this->db->where('meta_key', 'foot_menu_2') -> update('settings', array('meta_value' => $this->input->post('foot_menu_2')));
			$this->db->where('meta_key', 'foot_menu_3') -> update('settings', array('meta_value' => $this->input->post('foot_menu_3')));
			$this->db->where('meta_key', 'foot_menu_1_link') -> update('settings', array('meta_value' => $this->input->post('foot_menu_1_link')));
			$this->db->where('meta_key', 'foot_menu_2_link') -> update('settings', array('meta_value' => $this->input->post('foot_menu_2_link')));
			$this->db->where('meta_key', 'foot_menu_3_link') -> update('settings', array('meta_value' => $this->input->post('foot_menu_3_link')));
            
			$this->session->set_flashdata('alert_success','Menu Updated successfully!');
			return redirect('menu');
		}
	}
}

?>