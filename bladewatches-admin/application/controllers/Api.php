<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function common() {
		header('Content-Type: application/json');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Headers: Content-Type, Accept");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
		
		$jsonObj = new \stdClass();

		$jsonObj->headerData = array(
			'logo' => base_url('assets/admin/images/' . $this->admin_model->details('main_logo')),
			'links' => array(
				array('menu' => $this->admin_model->details('menu_1'), 'link' => $this->admin_model->details('menu_1_link')),
				array('menu' => $this->admin_model->details('menu_2'), 'link' => $this->admin_model->details('menu_2_link')),
				array('menu' => $this->admin_model->details('menu_3'), 'link' => $this->admin_model->details('menu_3_link')),
                array('menu' => $this->admin_model->details('menu_4'), 'link' => $this->admin_model->details('menu_4_link')),
                array('menu' => $this->admin_model->details('menu_5'), 'link' => $this->admin_model->details('menu_5_link'))
			)
		);

		$popupDetails = $this->popup_model->get_data();
		$popupDetails['img'] = base_url('assets/admin/images/' . $popupDetails['img']);
		$jsonObj->popupData = $popupDetails;

		$jsonObj->footerData = array(
			'logo' => base_url('assets/admin/images/' . $this->admin_model->details('footer_logo')),
			'links' => array(
				array('menu' => $this->admin_model->details('foot_menu_1'), 'link' => $this->admin_model->details('foot_menu_1_link')),
				array('menu' => $this->admin_model->details('foot_menu_2'), 'link' => $this->admin_model->details('foot_menu_2_link')),
				array('menu' => $this->admin_model->details('foot_menu_3'), 'link' => $this->admin_model->details('foot_menu_3_link'))
			),
			'collection_list_1' => $this->categories_model->get_footer_collist(1),
			'collection_list_2' => $this->categories_model->get_footer_collist(2),
			'contactInfo' => array(
				'addr' => $this->admin_model->details('addr'),
				'tel' => $this->admin_model->details('cont'),
				'fax' => $this->admin_model->details('fax'),
				'email' => $this->admin_model->details('email')
			),
			'sociallinks' => array(
				'fb' => $this->admin_model->details('facebook'),
				'gmail' => $this->admin_model->details('gmail'),
				'twitter' => $this->admin_model->details('twitter'),
				'linkedin' => $this->admin_model->details('linkedin'),
				'youtube' => $this->admin_model->details('youtube'),
				'insta' => $this->admin_model->details('insta'),
				'gplus' => $this->admin_model->details('gplus'),
				'pinterest' => $this->admin_model->details('pinterest')
			),
			'copyright' => $this->admin_model->details('copyrights')
		);

		print_r(json_encode($jsonObj));
	}
	
	public function pages($slug) {
		header('Content-Type: application/json');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Headers: Content-Type, Accept");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

		$jsonObj = new \stdClass();

		if ($slug === 'home') {
			$home = $this->pages_model->get_page_model('home');
			$homeContents = json_decode($home['contents']);
			$homeContents->bannerImg = base_url('assets/admin/images/pages/' . $homeContents->bannerImg);
			$jsonObj->homeData = $homeContents;

			$collArray = $this->products_model->get_cat("1");
			foreach ($collArray as $key => $array) {
				$collArray[$key]['image'] = base_url('assets/admin/images/' . $array['image']);
				$collArray[$key]['banimage'] = base_url('assets/admin/images/' . $array['banimage']);
			}
			$jsonObj->collections = $collArray;

			$accArray = $this->products_model->get_cat("2");
			foreach ($accArray as $key => $array) {
				$accArray[$key]['image'] = base_url('assets/admin/images/' . $array['image']);
				$accArray[$key]['banimage'] = base_url('assets/admin/images/' . $array['banimage']);
			}
			$jsonObj->accessories = $accArray;

			$feedArray = $this->socialfeed_model->get_images();
			foreach ($feedArray as $key => $array) {
				$feedArray[$key]['image'] = base_url('assets/admin/images/social/' . $array['image']);
			}
			$jsonObj->socialFeed = $feedArray;
			
		}

		if ($slug === 'aboutus') {
			$abt = $this->pages_model->get_page_model('aboutus');
			$abtContents = json_decode($abt['contents']);
			$abtContents->abtImg = base_url('assets/admin/images/pages/' . $abtContents->abtImg);
			$jsonObj->abtData = $abtContents;

			$timelineArray = $this->timeline_model->get_data();
			foreach ($timelineArray as $key => $array) {
				if (!empty($array['image'])) {
					$timelineArray[$key]['image'] = base_url('assets/admin/images/timeline/' . $array['image']);
				}
			}
			$timelineArray = array_reverse($timelineArray);
			$jsonObj->timeline = $timelineArray;
			
		}

		if ($slug === 'contactus') {
			$contact = $this->pages_model->get_page_model('contactus');
			$contContents = json_decode($contact['contents']);
			$contContents->contImg = base_url('assets/admin/images/pages/' . $contContents->contImg);
			$jsonObj->contData = $contContents;

			$jsonObj->countries = $this->stores_model->get_stores_country();
			$jsonObj->allStores = $this->stores_model->get_data();
			$jsonObj->sociallinks = array(
				'fb' => $this->admin_model->details('facebook'),
				'gmail' => $this->admin_model->details('gmail'),
				'twitter' => $this->admin_model->details('twitter'),
				'linkedin' => $this->admin_model->details('linkedin'),
				'youtube' => $this->admin_model->details('youtube'),
				'insta' => $this->admin_model->details('insta'),
				'gplus' => $this->admin_model->details('gplus'),
				'pinterest' => $this->admin_model->details('pinterest')
			);
		}

		print_r(json_encode($jsonObj));
	}

	public function storenservice($category, $country) {
		header('Content-Type: application/json');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Headers: Content-Type, Accept");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

		$jsonObj = new \stdClass();

		$jsonObj->allStores = $this->stores_model->get_storendservice($category, $country);

		print_r(json_encode($jsonObj));
	}

	public function collections($category, $product=false) {
		header('Content-Type: application/json');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Headers: Content-Type, Accept");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

		$jsonObj = new \stdClass();

		if ($product === false) {
			$jsonObj->categories = $this->products_model->get_cat("1");

			$categoryData = $this->categories_model->get_cat_by_slug($category);
			$categoryData['image'] = base_url('assets/admin/images/' . $categoryData['image']);
			$categoryData['banimage'] = base_url('assets/admin/images/' . $categoryData['banimage']);
			$categoryData['mbanimage'] = base_url('assets/admin/images/' . $categoryData['mbanimage']);
			$jsonObj->categoryData = $categoryData;

			$products = $this->products_model->get_data();
			$productlist = array();
			foreach ($products as $key => $product) {
				$prodcoll = explode(",", $product['collections']);
				if (in_array($categoryData['id'], $prodcoll)) {
					if ($product['status'] == 1) {
						$productlist[] = $product;
					}
                }
                unset($prodcoll);
			}
			foreach ($productlist as $key => $array) {
				if (!empty($array['image1'])) {
					$productlist[$key]['image1'] = base_url('assets/admin/images/products/' . $array['image1']);
				}
				if (!empty($array['hover1'])) {
					$productlist[$key]['hover1'] = base_url('assets/admin/images/products/' . $array['hover1']);
				}
				if (!empty($array['image2'])) {
					$productlist[$key]['image2'] = base_url('assets/admin/images/products/' . $array['image2']);
				}
				if (!empty($array['hover2'])) {
					$productlist[$key]['hover2'] = base_url('assets/admin/images/products/' . $array['hover2']);
				}
				if (!empty($array['image3'])) {
					$productlist[$key]['image3'] = base_url('assets/admin/images/products/' . $array['image3']);
				}
				if (!empty($array['hover3'])) {
					$productlist[$key]['hover3'] = base_url('assets/admin/images/products/' . $array['hover3']);
				}
				if (!empty($array['image4'])) {
					$productlist[$key]['image4'] = base_url('assets/admin/images/products/' . $array['image4']);
				}
				if (!empty($array['hover4'])) {
					$productlist[$key]['hover4'] = base_url('assets/admin/images/products/' . $array['hover4']);
				}

				unset($productlist[$key]['attributes']);
			}
			$jsonObj->products = $productlist;

		} else {
			$prodData = $this->products_model->get_product_model_id($product);
			if (!empty($prodData['image1'])) {
				$prodData['image1'] = base_url('assets/admin/images/products/' . $prodData['image1']);
			}
			if (!empty($prodData['hover1'])) {
				$prodData['hover1'] = base_url('assets/admin/images/products/' . $prodData['hover1']);
			}
			if (!empty($prodData['image2'])) {
				$prodData['image2'] = base_url('assets/admin/images/products/' . $prodData['image2']);
			}
			if (!empty($prodData['hover2'])) {
				$prodData['hover2'] = base_url('assets/admin/images/products/' . $prodData['hover2']);
			}
			if (!empty($prodData['image3'])) {
				$prodData['image3'] = base_url('assets/admin/images/products/' . $prodData['image3']);
			}
			if (!empty($prodData['hover3'])) {
				$prodData['hover3'] = base_url('assets/admin/images/products/' . $prodData['hover3']);
			}
			if (!empty($prodData['image4'])) {
				$prodData['image4'] = base_url('assets/admin/images/products/' . $prodData['image4']);
			}
			if (!empty($prodData['hover4'])) {
				$prodData['hover4'] = base_url('assets/admin/images/products/' . $prodData['hover4']);
			}
			$attributes = json_decode($prodData['attributes']);
			$prodData['attributes'] = array(
				array('key' => $attributes->key1, 'value' => $attributes->value1),
				array('key' => $attributes->key2, 'value' => $attributes->value2),
				array('key' => $attributes->key3, 'value' => $attributes->value3),
				array('key' => $attributes->key4, 'value' => $attributes->value4),
				array('key' => $attributes->key5, 'value' => $attributes->value5),
				array('key' => $attributes->key6, 'value' => $attributes->value6),
				array('key' => $attributes->key7, 'value' => $attributes->value7),
				array('key' => $attributes->key8, 'value' => $attributes->value8)
			);
			$jsonObj->prodData = $prodData;

			$prodColl = explode(",", $prodData['collections']);
			$otherprods = array();
			foreach ($prodColl as $key => $prod) {
				$products = $this->products_model->get_data();
				foreach ($products as $key => $product) {
					if ($product['id'] != $prodData['id']) {
						$prodcoll = explode(",", $product['collections']);
						if (in_array($prod, $prodcoll)) {
							if ($product['status'] == 1) {
								if (!empty($otherprods)) {
									foreach ($otherprods as $key => $other) {
										if ($other['id'] == $product['id']) {
											break;
										}
										if ($key == sizeof($otherprods) - 1) {
											if (!empty($product['image1'])) {
												$product['image1'] = base_url('assets/admin/images/products/' . $product['image1']);
											}
											if (!empty($product['hover1'])) {
												$product['hover1'] = base_url('assets/admin/images/products/' . $product['hover1']);
											}
											if (!empty($product['image2'])) {
												$product['image2'] = base_url('assets/admin/images/products/' . $product['image2']);
											}
											if (!empty($product['hover2'])) {
												$product['hover2'] = base_url('assets/admin/images/products/' . $product['hover2']);
											}
											if (!empty($product['image3'])) {
												$product['image3'] = base_url('assets/admin/images/products/' . $product['image3']);
											}
											if (!empty($product['hover3'])) {
												$product['hover3'] = base_url('assets/admin/images/products/' . $product['hover3']);
											}
											if (!empty($product['image4'])) {
												$product['image4'] = base_url('assets/admin/images/products/' . $product['image4']);
											}
											if (!empty($product['hover4'])) {
												$product['hover4'] = base_url('assets/admin/images/products/' . $product['hover4']);
											}
											$otherprods[] = $product;
										}
									}
								} else {
									if (!empty($product['image1'])) {
										$product['image1'] = base_url('assets/admin/images/products/' . $product['image1']);
									}
									if (!empty($product['hover1'])) {
										$product['hover1'] = base_url('assets/admin/images/products/' . $product['hover1']);
									}
									if (!empty($product['image2'])) {
										$product['image2'] = base_url('assets/admin/images/products/' . $product['image2']);
									}
									if (!empty($product['hover2'])) {
										$product['hover2'] = base_url('assets/admin/images/products/' . $product['hover2']);
									}
									if (!empty($product['image3'])) {
										$product['image3'] = base_url('assets/admin/images/products/' . $product['image3']);
									}
									if (!empty($product['hover3'])) {
										$product['hover3'] = base_url('assets/admin/images/products/' . $product['hover3']);
									}
									if (!empty($product['image4'])) {
										$product['image4'] = base_url('assets/admin/images/products/' . $product['image4']);
									}
									if (!empty($product['hover4'])) {
										$product['hover4'] = base_url('assets/admin/images/products/' . $product['hover4']);
									}
									$otherprods[] = $product;
								}
							}
		                }
		                unset($prodcoll);
		            }
				}
			}
			$jsonObj->otherProds = $otherprods;
		}

		print_r(json_encode($jsonObj));
	}

	public function accessories($category, $product=false) {
		header('Content-Type: application/json');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Headers: Content-Type, Accept");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

		$jsonObj = new \stdClass();

		if ($product === false) {
			$jsonObj->categories = $this->products_model->get_cat("2");

			$categoryData = $this->categories_model->get_cat_by_slug($category);
			$categoryData['image'] = base_url('assets/admin/images/' . $categoryData['image']);
			$categoryData['banimage'] = base_url('assets/admin/images/' . $categoryData['banimage']);
			$categoryData['mbanimage'] = base_url('assets/admin/images/' . $categoryData['mbanimage']);
			$jsonObj->categoryData = $categoryData;

			$products = $this->products_model->get_data();
			foreach ($products as $key => $product) {
				$prodcoll = explode(",", $product['accessories']);
				if (in_array($categoryData['id'], $prodcoll)) {
					if ($product['status'] == 1) {
						$productlist[] = $product;
					}
                }
                unset($prodcoll);
			}
			foreach ($productlist as $key => $array) {
				if (!empty($array['image1'])) {
					$productlist[$key]['image1'] = base_url('assets/admin/images/products/' . $array['image1']);
				}
				if (!empty($array['hover1'])) {
					$productlist[$key]['hover1'] = base_url('assets/admin/images/products/' . $array['hover1']);
				}
				if (!empty($array['image2'])) {
					$productlist[$key]['image2'] = base_url('assets/admin/images/products/' . $array['image2']);
				}
				if (!empty($array['hover2'])) {
					$productlist[$key]['hover2'] = base_url('assets/admin/images/products/' . $array['hover2']);
				}
				if (!empty($array['image3'])) {
					$productlist[$key]['image3'] = base_url('assets/admin/images/products/' . $array['image3']);
				}
				if (!empty($array['hover3'])) {
					$productlist[$key]['hover3'] = base_url('assets/admin/images/products/' . $array['hover3']);
				}
				if (!empty($array['image4'])) {
					$productlist[$key]['image4'] = base_url('assets/admin/images/products/' . $array['image4']);
				}
				if (!empty($array['hover4'])) {
					$productlist[$key]['hover4'] = base_url('assets/admin/images/products/' . $array['hover4']);
				}

				unset($productlist[$key]['attributes']);
			}
			$jsonObj->products = $productlist;

		} else {
			$prodData = $this->products_model->get_product_model_id($product);
			if (!empty($prodData['image1'])) {
				$prodData['image1'] = base_url('assets/admin/images/products/' . $prodData['image1']);
			}
			if (!empty($prodData['hover1'])) {
				$prodData['hover1'] = base_url('assets/admin/images/products/' . $prodData['hover1']);
			}
			if (!empty($prodData['image2'])) {
				$prodData['image2'] = base_url('assets/admin/images/products/' . $prodData['image2']);
			}
			if (!empty($prodData['hover2'])) {
				$prodData['hover2'] = base_url('assets/admin/images/products/' . $prodData['hover2']);
			}
			if (!empty($prodData['image3'])) {
				$prodData['image3'] = base_url('assets/admin/images/products/' . $prodData['image3']);
			}
			if (!empty($prodData['hover3'])) {
				$prodData['hover3'] = base_url('assets/admin/images/products/' . $prodData['hover3']);
			}
			if (!empty($prodData['image4'])) {
				$prodData['image4'] = base_url('assets/admin/images/products/' . $prodData['image4']);
			}
			if (!empty($prodData['hover4'])) {
				$prodData['hover4'] = base_url('assets/admin/images/products/' . $prodData['hover4']);
			}
			$attributes = json_decode($prodData['attributes']);
			$prodData['attributes'] = array(
				array('key' => $attributes->key1, 'value' => $attributes->value1),
				array('key' => $attributes->key2, 'value' => $attributes->value2),
				array('key' => $attributes->key3, 'value' => $attributes->value3),
				array('key' => $attributes->key4, 'value' => $attributes->value4),
				array('key' => $attributes->key5, 'value' => $attributes->value5),
				array('key' => $attributes->key6, 'value' => $attributes->value6),
				array('key' => $attributes->key7, 'value' => $attributes->value7),
				array('key' => $attributes->key8, 'value' => $attributes->value8)
			);
			$jsonObj->prodData = $prodData;

			$prodColl = explode(",", $prodData['accessories']);
			$otherprods = array();
			foreach ($prodColl as $key => $prod) {
				$products = $this->products_model->get_data();
				foreach ($products as $key => $product) {
					if ($product['id'] != $prodData['id']) {
						$prodcoll = explode(",", $product['accessories']);
						if (in_array($prod, $prodcoll)) {
							if ($product['status'] == 1) {
								if (!empty($otherprods)) {
									foreach ($otherprods as $key => $other) {
										if ($other['id'] == $product['id']) {
											break;
										}
										if ($key == sizeof($otherprods) - 1) {
											if (!empty($product['image1'])) {
												$product['image1'] = base_url('assets/admin/images/products/' . $product['image1']);
											}
											if (!empty($product['hover1'])) {
												$product['hover1'] = base_url('assets/admin/images/products/' . $product['hover1']);
											}
											if (!empty($product['image2'])) {
												$product['image2'] = base_url('assets/admin/images/products/' . $product['image2']);
											}
											if (!empty($product['hover2'])) {
												$product['hover2'] = base_url('assets/admin/images/products/' . $product['hover2']);
											}
											if (!empty($product['image3'])) {
												$product['image3'] = base_url('assets/admin/images/products/' . $product['image3']);
											}
											if (!empty($product['hover3'])) {
												$product['hover3'] = base_url('assets/admin/images/products/' . $product['hover3']);
											}
											if (!empty($product['image4'])) {
												$product['image4'] = base_url('assets/admin/images/products/' . $product['image4']);
											}
											if (!empty($product['hover4'])) {
												$product['hover4'] = base_url('assets/admin/images/products/' . $product['hover4']);
											}
											$otherprods[] = $product;
										}
									}
								} else {
									if (!empty($product['image1'])) {
										$product['image1'] = base_url('assets/admin/images/products/' . $product['image1']);
									}
									if (!empty($product['hover1'])) {
										$product['hover1'] = base_url('assets/admin/images/products/' . $product['hover1']);
									}
									if (!empty($product['image2'])) {
										$product['image2'] = base_url('assets/admin/images/products/' . $product['image2']);
									}
									if (!empty($product['hover2'])) {
										$product['hover2'] = base_url('assets/admin/images/products/' . $product['hover2']);
									}
									if (!empty($product['image3'])) {
										$product['image3'] = base_url('assets/admin/images/products/' . $product['image3']);
									}
									if (!empty($product['hover3'])) {
										$product['hover3'] = base_url('assets/admin/images/products/' . $product['hover3']);
									}
									if (!empty($product['image4'])) {
										$product['image4'] = base_url('assets/admin/images/products/' . $product['image4']);
									}
									if (!empty($product['hover4'])) {
										$product['hover4'] = base_url('assets/admin/images/products/' . $product['hover4']);
									}
									$otherprods[] = $product;
								}
							}
		                }
		                unset($prodcoll);
		            }
				}
			}
			$jsonObj->otherProds = $otherprods;
		}

		print_r(json_encode($jsonObj));
	}

	public function offers() {
		header('Content-Type: application/json');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Headers: Content-Type, Accept");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
		
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$jsonObj = new \stdClass();

			$postdata = file_get_contents("php://input");
			$save = json_decode($postdata);

			$this->db->insert("offers",array("name" => $save->name, "cont" => $save->cont, "email" => $save->email));
			$response =  $this->db->insert_id();

			if ($response) {
				$jsonObj->response = array('msg' => 'Thank You');
				print_r(json_encode($jsonObj));

				// trigger mail to user
				$message = "<h4>Thank You for Subscribing !<h4><h1>Welcome to BLADE</h1><p>Hi ".$save->name.",</p>
				<p>By subscribing to our newsletter, you're now among the first to hear about our latest designs, exclusive deals & competitions. We respect your time and promise to never spam you or share your details with anyone else.</p><h5>STAY CONNECTED</h5><p>Follow us and stay in touch!<br><a href='https://www.facebook.com/BladeWatches/' target='_blank'>Facebook</a><br><a href='https://www.instagram.com/bladewatches/' target='_blank'>Instagram</a><br><a href='https://twitter.com/bladewatches' target='_blank'>Twitter</a></p><p>Until Next Time!</p>";
				$email_template = $this->load->view('email/template',array(),true);
				send_email(array(
					'receiver_email' => $save->email,
					'from_name' => 'Blade Watches',
					'from_email' => 'info@bladewatches.com',
					'subject' => 'BladeWatches Offers',
					'message' => str_replace('[message]', $message, $email_template)
				));

				// trigger mail to admin
				$message = "<p>You have a new subscriber!</p>
					<p>Email: ".$save->email."</p><p>Name: ".$save->name.".</p><p>Phone: ".$save->cont.".</p><p>Sign-Up Time: ".date("Y-m-d h:i").".</p>
				";
				$email_template = $this->load->view('email/template',array(),true);
				send_email(array(
					'receiver_email' => $this->admin_model->details('contemail'),
					'from_name' => $save->name,
					'from_email' => $save->email,
					'subject' => 'New Subscription',
					'message' => str_replace('[message]', $message, $email_template)
				));
			}
		}
	}

	public function newsletter() {
		header('Content-Type: application/json');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Headers: Content-Type, Accept");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$jsonObj = new \stdClass();

			$postdata = file_get_contents("php://input");
			$save = json_decode($postdata);

			$this->db->insert("newsletter",array("email" => $save->email));
			$response =  $this->db->insert_id();

			if ($response) {
				$jsonObj->response = array('msg' => 'Thank You');
				print_r(json_encode($jsonObj));

				// trigger mail to user
				$message = "<h4>Thank You for Subscribing !<h4><h1>Welcome to BLADE</h1><p>Hi There,</p>
				<p>By subscribing to our newsletter, you're now among the first to hear about our latest designs, exclusive deals & competitions. We respect your time and promise to never spam you or share your details with anyone else.</p><h5>STAY CONNECTED</h5><p>Follow us and stay in touch!<br><a href='https://www.facebook.com/BladeWatches/' target='_blank'>Facebook</a><br><a href='https://www.instagram.com/bladewatches/' target='_blank'>Instagram</a><br><a href='https://twitter.com/bladewatches' target='_blank'>Twitter</a></p><p>Until Next Time!</p>";
				$email_template = $this->load->view('email/template',array(),true);
				send_email(array(
					'receiver_email' => $save->email,
					'from_name' => 'Blade Watches',
					'from_email' => 'info@bladewatches.com',
					'subject' => 'Newsletter Subscribed',
					'message' => str_replace('[message]', $message, $email_template)
				));

				// trigger mail to admin
				$message = "<p>You have a new newsletter request!</p>
					<p>Email: ".$save->email."</p><p>Sign-Up Time: ".date("Y-m-d h:i").".</p>
				";
				$email_template = $this->load->view('email/template',array(),true);
				send_email(array(
					'receiver_email' => $this->admin_model->details('contemail'),
					'from_name' => 'Blade Watches',
					'from_email' => $save->email,
					'subject' => 'New Newsletter Request',
					'message' => str_replace('[message]', $message, $email_template)
				));
			}
		}
	}

	public function enquiry() {
		header('Content-Type: application/json');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Headers: Content-Type, Accept");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$jsonObj = new \stdClass();

			$postdata = file_get_contents("php://input");
			$save = json_decode($postdata);

			$this->db->insert("enquiry",array("name" => $save->first . " " . $save->last, "company" => $save->company, "email" => $save->email, "cont" => $save->cont, "city" => $save->city, "country" => $save->country, "type" => $save->type, "message" => $save->message));
			$response =  $this->db->insert_id();

			if ($response) {
				$jsonObj->response = array('msg' => 'Thank You');
				print_r(json_encode($jsonObj));

				// trigger mail to user
				$message = "<h4>Thank You for Subscribing !<h4><h1>Welcome to BLADE</h1><p>Dear ".$save->first.",</p>
				<p>This is an automated email to confirm that we have received your enquiry. Our team will be in touch with you within the next 48hrs.</p><h5>STAY CONNECTED</h5><p>Follow us and stay in touch!<br><a href='https://www.facebook.com/BladeWatches/' target='_blank'>Facebook</a><br><a href='https://www.instagram.com/bladewatches/' target='_blank'>Instagram</a><br><a href='https://twitter.com/bladewatches' target='_blank'>Twitter</a></p><p>Until Next Time!</p>";
				$email_template = $this->load->view('email/template',array(),true);
				send_email(array(
					'receiver_email' => $save->email,
					'from_name' => 'Blade Watches',
					'from_email' => 'info@bladewatches.com',
					'subject' => 'Newsletter Subscribed',
					'message' => str_replace('[message]', $message, $email_template)
				));

				// trigger mail to admin
				$message = "<p>You have a new enquiry!</p>
					<p>Email: ".$save->email."</p><p>Name: ".$save->first." ".$save->last.".</p><p>Phone: ".$save->cont.".</p><p>Sign-Up Time: ".date("Y-m-d h:i").".</p>
				";
				$email_template = $this->load->view('email/template',array(),true);
				send_email(array(
					'receiver_email' => $this->admin_model->details('contemail'),
					'from_name' => $save->first,
					'from_email' => $save->email,
					'subject' => 'New Enquiry',
					'message' => str_replace('[message]', $message, $email_template)
				));
			}
		}
	}

}

?>
