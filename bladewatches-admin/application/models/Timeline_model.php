<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timeline_model extends CI_Model
{
    function get_data(){
        return $this->db->get('timeline')->result_array();
    }

    function save_timeline_data_model($data){
        $this->db->insert("timeline",$data);      
        return $this->db->insert_id();
    }

    function update_timeline_data_model($data){
        $this->db->where("id",$data['id'])->update("timeline",$data);
        return $data['id'];
    }
}

?>