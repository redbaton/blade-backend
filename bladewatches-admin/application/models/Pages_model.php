<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends CI_Model
{
    function get_page_model($slug){
        $this->db->where("slug",$slug);
        return $this->db->get('pages')->row_array();
    }
    
    function save_page_model($data){
		$this->db->where("slug",$data['slug'])->update("pages",$data);
		return $data['id'];
    }
}

?>