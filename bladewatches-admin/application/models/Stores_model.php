<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stores_model extends CI_Model
{
	function get_data()
	{
		return $this->db->get('stores')->result_array();
	}

	function get_storendservice($category, $country)
	{
		if ($category == 'storenservice' && $country == 'All') {
			return $this->db->get('stores')->result_array();
		} else if ($country == 'All') {
			return $this->db->where('category', $category)->get('stores')->result_array();
		} else if ($category == 'storenservice') {
			return $this->db->where('country', $country)->get('stores')->result_array();
		} else {
			return $this->db->where(array('category' => $category, 'country' => $country))->get('stores')->result_array();
		}
	}
    
    function get_stores_country()
	{
		$countries = $this->db->distinct() -> select('country') -> where('country !=', '') -> order_by('country', 'ASC') -> get('stores') -> result_array();
		array_unshift($countries, array("country" => "All"));
		return $countries;
	}
    
    function get_store_id($id){
        $this->db->where("id",$id);
        return $this->db->get('stores')->row_array();
    }
    
    function save_stores_model($data){
        if(!empty($data['id'])){
			$this->db->where("id",$data['id']);
			$this->db->update("stores",$data);
			return $data['id'];
		}else{
			$this->db->insert("stores",$data);		
			return $this->db->insert_id();
		}
    }
    
    function delete_stores_model($id){
		$this->db->where("id",$id);
		return $this->db->delete("stores");
    }
}

?>