<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Socialfeed_model extends CI_Model
{
    function get_images(){
        return $this->db->get('socialfeed')->result_array();
    }

    function save_images_data_model($data){
		$this->db->insert("socialfeed",$data);		
		return $this->db->insert_id();
    }

    function update_image_data_model($data){
		$this->db->where("id",$data['id'])->update("socialfeed",$data);
		return $data['id'];
    }
}

?>