<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model
{
	function get_data()
	{
		return $this->db->get('products')->result_array();
	}

	function get_cat($type)
	{
		return $this->db->where(array('type'=>$type, 'status'=>1))->get('product_categories')->result_array();
	}
    
    function get_product_model_id($id){
        $this->db->where("id",$id);
        return $this->db->get('products')->row_array();
    }
    
    function save_product_model($data){
        if(!empty($data['id'])){
			$this->db->where("id",$data['id']);
			$this->db->update("products",$data);
			return $data['id'];
		}else{
			$this->db->insert("products",$data);		
			return $this->db->insert_id();
		}
    }
    
    function delete_product_model($id){
		$this->db->where("id",$id);
		return $this->db->delete("products");
    }
}

?>