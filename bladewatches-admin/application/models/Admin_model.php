<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	function details($key) {
		return $detail = $this->db->where('meta_key', $key) -> get('settings') -> row() -> meta_value;
	}

	public function login($email,$password)
	{
		$user = $this->db->where(array('ustatus'=>1, 'uemail'=>$email, 'upass'=>md5($password))) -> get('users');
		
		if($user->num_rows())
			return  $user->row_array();
		else		
			return false;
	}

	function admin_user($col, $val)
	{		
		$query = $this->db->where($col, $val) -> get('users');
		return $query->num_rows()?$query->row_array():false;
	}

	function save_admin_user($data)
	{
		if($data['admin_user_id']){
			if ( $this->db->where('ID',$data['admin_user_id']) -> update('users', array('uemail' => $data['email'])) ) {
			 	$this->db->where('meta_key', 'favicon') -> update('settings', array('meta_value' => $data['favlogo']));
			 	$this->db->where('meta_key', 'main_logo') -> update('settings', array('meta_value' => $data['sitelogo']));
			 	$this->db->where('meta_key', 'scroll_logo') -> update('settings', array('meta_value' => $data['scrolllogo']));
			 	$this->db->where('meta_key', 'footer_logo') -> update('settings', array('meta_value' => $data['footlogo']));
			 	$this->db->where('meta_key', 'site_title') -> update('settings', array('meta_value' => $data['sitetitle']));
			 	$this->db->where('meta_key', 'admin_fname') -> update('settings', array('meta_value' => $data['firstname']));
			 	$this->db->where('meta_key', 'admin_lname') -> update('settings', array('meta_value' => $data['lastname']));
			 	$this->db->where('meta_key', 'copyrights') -> update('settings', array('meta_value' => $data['copyrights']));
			 	$this->db->where('meta_key', 'contemail') -> update('settings', array('meta_value' => $data['contemail']));

			 	return $data['admin_user_id'];
			}
		}
	}

	function save_social($data)
	{
	 	$this->db->where('meta_key', 'facebook') -> update('settings', array('meta_value' => $data['facebook']));
	 	$this->db->where('meta_key', 'gmail') -> update('settings', array('meta_value' => $data['gmail']));
	 	$this->db->where('meta_key', 'twitter') -> update('settings', array('meta_value' => $data['twitter']));
	 	$this->db->where('meta_key', 'linkedin') -> update('settings', array('meta_value' => $data['linkedin']));
	 	$this->db->where('meta_key', 'insta') -> update('settings', array('meta_value' => $data['insta']));
	 	$this->db->where('meta_key', 'gplus') -> update('settings', array('meta_value' => $data['gplus']));
	 	$this->db->where('meta_key', 'pinterest') -> update('settings', array('meta_value' => $data['pinterest']));
	 	$this->db->where('meta_key', 'youtube') -> update('settings', array('meta_value' => $data['youtube']));

	 	return true;
	}

	function save_contactdetail($data)
	{
	 	$this->db->where('meta_key', 'addr') -> update('settings', array('meta_value' => $data['addr']));
	 	$this->db->where('meta_key', 'cont') -> update('settings', array('meta_value' => $data['cont']));
	 	$this->db->where('meta_key', 'email') -> update('settings', array('meta_value' => $data['email']));
	 	$this->db->where('meta_key', 'fax') -> update('settings', array('meta_value' => $data['fax']));

	 	return true;
	}
}

?>