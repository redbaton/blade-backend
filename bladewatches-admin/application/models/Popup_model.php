<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Popup_model extends CI_Model
{
	function get_data()
	{
		return $this->db->get('popup')->row_array();
	}
    
    function save_popup_model($data){
		$this->db->where("id",$data['id']) -> update("popup",$data);
		return $data['id'];
    }
}

?>