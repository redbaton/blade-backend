<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiries_model extends CI_Model
{
	function get_offers()
	{
		return $this->db->get('offers')->result_array();
	}

	function get_newsletter()
	{
		return $this->db->get('newsletter')->result_array();
	}

	function get_enquiries() {
		return $this->db->get('enquiry')->result_array();
	}

	function delete_entry($table, $id){
		$this->db->where("id",$id)->delete($table);
    }
}

?>