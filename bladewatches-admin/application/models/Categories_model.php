<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model
{
	function get_data()
	{
		return $this->db->get('product_categories')->result_array();
	}
    
    function get_cat_model_id($id){
        $this->db->where("id",$id);
        return $this->db->get('product_categories')->row_array();
    }

    function get_cat_by_slug($slug){
        $this->db->where("slug",$slug);
        return $this->db->get('product_categories')->row_array();
    }

	function get_footer_collist($id){
        $this->db->where("linktofooter",$id);
        return $this->db->get('product_categories')->result_array();
    }
    
    function save_cat_model($data){
        if(!empty($data['id'])){
			$this->db->where("id",$data['id']);
			$this->db->update("product_categories",$data);
			return $data['id'];
		}else{
			$this->db->insert("product_categories",$data);		
			return $this->db->insert_id();
		}
    }
    
    function delete_cat_model($id){
		$this->db->where("id",$id);
		return $this->db->delete("product_categories");
    }
}

?>