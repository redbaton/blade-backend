webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/aboutus/aboutus.component.html":
/***/ (function(module, exports) {

module.exports = "<section  id=\"aboutus\" >\n\t<div class=\"container-fluid banner\">\n\t\t<div class=\"container \">\n\t\t\t<div class=\"row align-items-top\">\n\t\t\t\t<div class=\"col-12 col-lg-6 order-lg-1 order-2 first\">\n\t\t\t\t\t<p class=\"fade-in-up\">\n\t\t\t\t\t\t{{bannerPara}}\n\t\t\t\t\t</p>\n\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-12 col-lg-6 order-lg-2 order-1 last fade-in-up\">\n\t\t\t\t\t<img src=\"{{abtBannerImg}}\" class=\"img-fluid\" alt=\"\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div class=\"timeline-section\">\n\t\t<div class=\"\">\n\t\t\t<div class=\"container\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-12\">\n\t\t\t\t\t\t<h1 class=\"theam-heading\">TIMELINE</h1>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-12\">\n\t\t\t\t\t\t<div class=\"timeline\">\n\n\t\t\t\t\t\t\t<ul class=\"days\" >\n\t\t\t\t\t\t\t\t<li class=\"day\" *ngFor=\"let timedata of timelineData\">\n\t\t\t\t\t\t\t\t\t<div class=\"events \">\n\t\t\t\t\t\t\t\t\t\t<p>{{timedata?.description}}</p>\n\t\t\t\t\t\t\t\t\t\t<div class=\"day__img\">\n\t\t\t\t\t\t\t\t\t\t\t<img src=\"{{timedata.image}}\" class=\"mt-3\" alt=\"\">\n\t\t\t\t\t\t\t\t\t\t\t<p class=\"caption\">\n\t\t\t\t\t\t\t\t\t\t\t\t{{timedata?.excerpt}}\n\t\t\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t<div class=\"date \">{{timedata?.year}}</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</li>\n\n\t\t\t\t\t\t\t\t<!-- <li class=\"day\">\n\t\t\t\t\t\t\t\t\t<div class=\"events\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"day__img\">\n\t\t\t\t\t\t\t\t\t\t\t<img src=\"http://placehold.it/400x300\" alt=\"\" />\n\t\t\t\t\t\t\t\t\t\t\t<p class=\"caption\">\n\t\t\t\t\t\t\t\t\t\t\t\tLorem ipsum dolor sit amet.\n\t\t\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"date\">2009</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</li> -->\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</section>\n\n"

/***/ }),

/***/ "../../../../../src/app/aboutus/aboutus.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#aboutus .banner {\n  background: #fff;\n  background-position: top;\n  padding-top: 150px;\n  padding-bottom: 100px;\n  font-weight: 300;\n  color: #000; }\n  @media screen and (max-width: 600px) {\n    #aboutus .banner {\n      padding-top: 80px; } }\n  #aboutus .timeline-section {\n  background: #fff;\n  padding-top: 50px;\n  padding-bottom: 50px; }\n  #aboutus .timeline-section .timeline {\n    text-align: center;\n    padding-top: 50px;\n    padding-bottom: 50px; }\n  #aboutus .timeline-section .timeline .year {\n      display: inline-block;\n      font-size: 36px;\n      font-weight: bold;\n      position: relative; }\n  #aboutus .timeline-section .timeline .year span {\n        display: inline-block;\n        padding-bottom: 6px; }\n  #aboutus .timeline-section .timeline .year:after {\n        content: \"\";\n        display: block;\n        width: 80%;\n        margin: auto;\n        height: 1px;\n        background: green; }\n  #aboutus .timeline-section .timeline .year--end:before {\n        content: \"\";\n        display: block;\n        width: 80%;\n        margin: auto;\n        height: 1px;\n        background: green; }\n  #aboutus .timeline-section .timeline .year--end:after {\n        content: none; }\n  #aboutus .timeline-section .timeline .days {\n      list-style-type: none;\n      margin: 0;\n      padding: 0; }\n  #aboutus .timeline-section .timeline .days .day {\n        width: 100%;\n        float: left; }\n  #aboutus .timeline-section .timeline .days .day .events {\n          position: relative;\n          float: left;\n          border-right: 1px solid #aa0000;\n          padding: 30px;\n          text-align: right;\n          width: 50%; }\n  @media screen and (max-width: 600px) {\n            #aboutus .timeline-section .timeline .days .day .events {\n              width: 100%;\n              margin-bottom: 30px; } }\n  #aboutus .timeline-section .timeline .days .day .events:after {\n            content: \"\";\n            width: 40%;\n            display: inline-block;\n            height: 1px;\n            background: #aa0000;\n            position: absolute;\n            right: 0; }\n  #aboutus .timeline-section .timeline .days .day .events .date {\n            position: absolute;\n            top: 50%;\n            left: 100%;\n            -webkit-transform: translateY(-50%);\n                    transform: translateY(-50%);\n            font-weight: 600;\n            padding: 30px;\n            text-align: left;\n            color: #aa0000;\n            font-size: 24px;\n            white-space: nowrap; }\n  @media screen and (max-width: 600px) {\n              #aboutus .timeline-section .timeline .days .day .events .date {\n                top: 0;\n                left: 58%; } }\n  #aboutus .timeline-section .timeline .days .day .events .day__img {\n            margin-right: -30px;\n            position: relative;\n            overflow: hidden; }\n  #aboutus .timeline-section .timeline .days .day .events .day__img img {\n              display: block;\n              float: right; }\n  #aboutus .timeline-section .timeline .days .day .events .day__img .caption {\n              position: absolute;\n              margin: 0;\n              bottom: 0;\n              right: 0;\n              padding: 20px;\n              background: rgba(0, 0, 0, 0.7);\n              color: #fff; }\n  #aboutus .timeline-section .timeline .days .day:nth-child(odd) .events {\n          width: calc(50% + 1px);\n          float: right;\n          border-right: 0;\n          border-left: 1px solid #aa0000;\n          text-align: left; }\n  @media screen and (max-width: 600px) {\n            #aboutus .timeline-section .timeline .days .day:nth-child(odd) .events {\n              width: 100%; } }\n  #aboutus .timeline-section .timeline .days .day:nth-child(odd) .events:after {\n            right: auto;\n            left: 0; }\n  #aboutus .timeline-section .timeline .days .day:nth-child(odd) .events .date {\n            left: auto;\n            right: 100%;\n            text-align: right; }\n  @media screen and (max-width: 600px) {\n              #aboutus .timeline-section .timeline .days .day:nth-child(odd) .events .date {\n                left: 0;\n                top: 0; } }\n  #aboutus .timeline-section .timeline .days .day:nth-child(odd) .events .day__img {\n            margin-right: auto;\n            margin-left: -30px; }\n  #aboutus .timeline-section .timeline .days .day:nth-child(odd) .events .day__img img {\n              float: left; }\n  #aboutus .timeline-section .timeline .days .day:nth-child(odd) .events .day__img .caption {\n              right: auto;\n              left: 0; }\n  #aboutus .theam-heading {\n  color: #000; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/aboutus/aboutus.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutusComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__global_service__ = __webpack_require__("../../../../../src/app/global.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AboutusComponent = /** @class */ (function () {
    function AboutusComponent(http, global, spinnerService) {
        this.http = http;
        this.global = global;
        this.spinnerService = spinnerService;
        this.aboutUrl = global.baseURL + '/pages/aboutus';
    }
    AboutusComponent.prototype.getData = function () {
        return this.http.get(this.aboutUrl);
    };
    AboutusComponent.prototype.getHomeData = function () {
        var _this = this;
        this.getData().subscribe(function (data) {
            _this.aboutData = data;
            _this.bannerPara = _this.aboutData.abtData.aboutPara;
            _this.abtBannerImg = _this.aboutData.abtData.abtImg;
            _this.timelineData = _this.aboutData.timeline;
            _this.spinnerService.hide();
        });
    };
    AboutusComponent.prototype.ngOnInit = function () {
        this.getHomeData();
        $(document).ready(function () {
            fadeInUp();
            remFullpage();
        });
    };
    AboutusComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-aboutus',
            template: __webpack_require__("../../../../../src/app/aboutus/aboutus.component.html"),
            styles: [__webpack_require__("../../../../../src/app/aboutus/aboutus.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__global_service__["a" /* GlobalService */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], AboutusComponent);
    return AboutusComponent;
}());



/***/ }),

/***/ "../../../../../src/app/accessories/accessories-prod/accessories-prod.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid inner-bg smoothstate\" id=\"product-ind\">\n  <div class=\"container h-100\">\n    <div class=\"row h-100 align-items-center\">\n      <div class=\"col-12 product-card\">\n        <div class=\"card fade-in-up\">\n          <div class=\"card-body\">\n            <div class=\"row h-100\">\n\n              <div class=\"col-12 col-md-5 product-slider fade-in-up\">\n                <swiper class=\"swiper-container gallery-top\" #galleryTop [config]=\"galleryTopConfig\">\n                  <div class=\"swiper-wrapper\">\n                    <div *ngIf=\"pImg1!=null\" class=\"swiper-slide\" (mouseenter)=\"imageZoom('myimage1');\" (mouseleave)=\"resetZoom();\">\n                      <img src=\"{{pImg1}}\" class=\"img-fluid \" id=\"myimage1\" alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg2!=null\" class=\"swiper-slide\" (mouseenter)=\"imageZoom('myimage2');\" (mouseleave)=\"resetZoom();\">\n                      <img src=\"{{pImg2}}\" class=\"img-fluid \" id=\"myimage2\" alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg3!=null\" class=\"swiper-slide\" (mouseenter)=\"imageZoom('myimage3');\" (mouseleave)=\"resetZoom();\">\n                      <img src=\"{{pImg3}}\" class=\"img-fluid \" id=\"myimage3\" alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg4!=null\" class=\"swiper-slide\" (mouseenter)=\"imageZoom('myimage4');\" (mouseleave)=\"resetZoom();\">\n                      <img src=\"{{pImg4}}\" class=\"img-fluid \" id=\"myimage4\" alt=\"\">\n                    </div>\n                  </div>\n                </swiper>\n                <swiper class=\"swiper-container gallery-thumbs\" #galleryThumbs [config]=\"galleryThumbsConfig\">\n                  <div class=\"swiper-wrapper\">\n                    <div *ngIf=\"pImg1!=null\" class=\"swiper-slide\">\n                      <img src=\"{{pImg1}}\" class=\"img-fluid\" alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg2!=null\" class=\"swiper-slide\">\n                      <img src=\"{{pImg2}}\" class=\"img-fluid\" alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg3!=null\" class=\"swiper-slide\">\n                      <img src=\"{{pImg3}}\" class=\"img-fluid\" alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg4!=null\" class=\"swiper-slide\">\n                      <img src=\"{{pImg4}}\" class=\"img-fluid\" alt=\"\">\n                    </div>\n                  </div>\n                  <div class=\"button-next\">\n                    <i class=\"ion-ios-arrow-right\"></i>\n                  </div>\n                  <div class=\"button-prev\">\n                    <i class=\"ion-ios-arrow-left\"></i>\n                  </div>\n                </swiper>\n              </div>\n\n              <div class=\"col-12 col-md-7\">\n                <div class=\"product-details active fade-in-up\">\n                  <div class=\"row\">\n                    <div class=\"col-8\">\n                        <h1 class=\"product-title\">{{pName}}</h1>\n                    </div>\n                    <div class=\"col-4 back\">\n                        <a (click)=\"goBack()\" class=\"float-right\">\n                          <i class=\"ion-close\"></i></a>\n                      </div>\n                  </div>\n                  <div class=\"row\">\n                    <div class=\"col-6\" *ngFor=\"let attribute of pAttributes\">\n                      <h3>{{attribute.key}}</h3>\n                      <p>{{attribute.value}}</p>\n                    </div>\n                  </div>\n                  <div class=\"row mt-20\">\n                    <div class=\"col-8\">\n                      <div class=\"sharing-icon\">\n                        <p class=\"d-inline-block\">Share on</p>\n                        <span class=\"d-inline-block\">\n                          <button shareButton=\"facebook\"><i class=\"ion-social-facebook\"></i></button>\n                          <button shareButton=\"google\">\n                              <svg style=\"padding-top:5px;\" xmlns=\"http://www.w3.org/2000/svg\" width=\"25\" height=\"20\" viewBox=\"0 0 35 20\">\n                                <path fill=\"#5F6165\" d=\"M10,8.57142857 L10,12 L15.6714286,12 C15.4428571,13.47 13.9571429,16.3142857 10,16.3142857 C6.58571429,16.3142857 3.8,13.4871429 3.8,10 C3.8,6.51428571 6.58571429,3.68571429 10,3.68571429 C11.9428571,3.68571429 13.2428571,4.51428571 13.9857143,5.22857143 L16.7,2.61428571 C14.9571429,0.985714286 12.7,1.77635684e-15 10,1.77635684e-15 C4.47142857,1.77635684e-15 -8.8817842e-16,4.47142857 -8.8817842e-16,10 C-8.8817842e-16,15.5285714 4.47142857,20 10,20 C15.7714286,20 19.6014286,15.9428571 19.6014286,10.2285714 C19.6014286,9.57142857 19.5285714,9.07142857 19.4428571,8.57142857 L10,8.57142857 Z M10,8.57142857 L34.2857143,11.4285714 L30,11.4285714 L30,15.7142857 L27.1428571,15.7142857 L27.1428571,11.4285714 L22.8571429,11.4285714 L22.8571429,8.57142857 L27.1428571,8.57142857 L27.1428571,4.28571429 L30,4.28571429 L30,8.57142857 L34.2857143,8.57142857 L34.2857143,11.4285714 L10,8.57142857 Z\"/>\n                              </svg>\n                          </button>\n                          <button shareButton=\"twitter\"><i class=\"ion-social-twitter\"></i></button>\n                          <button shareButton=\"whatsapp\"><i class=\"ion-social-whatsapp\"></i></button>\n                          <button shareButton=\"pinterest\"><i class=\"ion-social-pinterest\"></i></button>\n                        </span>\n                      </div>\n                    </div>\n                    \n                  </div>\n                </div>\n\n\n                <div id=\"resultZoomDiv\" class=\"zoomDiv\">\n\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"fade-in-up\">\n          <div aria-label=\"breadcrumb\">\n            <ol class=\"breadcrumb\">\n              <li class=\"breadcrumb-item\">\n                <a [routerLink]=\"['/collections', urlParams.accessoriesName]\">Collections</a>\n              </li>\n              <li class=\"breadcrumb-item\">\n                <a [routerLink]=\"['/collections', urlParams.accessoriesName]\">{{urlParams.accessoriesName}}</a>\n              </li>\n              <li class=\"breadcrumb-item active\" aria-current=\"page\">{{pName}}</li>\n            </ol>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"container-fluid\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-12 collection rViewSlider\">\n        <h2>Related VIEWED</h2>\n        <swiper class=\"swiper-container\" [config]=\"relatedView\">\n          <div class=\"swiper-wrapper\">\n            <div class=\"swiper-slide\" *ngFor=\"let prods of otherProd\">\n              <a (click)=\"viewProd(prods.id)\">\n                <div class=\"card fade-in\">\n                  <div class=\"card-body\">\n                    <img src=\"{{prods.image1}}\" class=\"img-fluid\" alt=\"\">\n                    <div class=\"collection-name\">\n                      <h3>{{prods.name}}</h3>\n                      <p>{{prods.excerpt}}</p>\n                    </div>\n                  </div>\n                  <a class=\"btn btn-primary btn-block\">VIEW MORE</a>\n                </div>\n              </a>\n            </div>\n          </div>\n\n        </swiper>\n        <div class=\"rbutton-next\">\n          <i class=\"ion-ios-arrow-right\"></i>\n        </div>\n        <div class=\"rbutton-prev\">\n          <i class=\"ion-ios-arrow-left\"></i>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/accessories/accessories-prod/accessories-prod.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#product-ind {\n  height: 100vh; }\n  #product-ind.inner-bg {\n    background: #000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABkEAAAQuCAYAAABrr5qaAAAACXBIWXMAAAsSAAALEgHS3X78AAAgAElEQVR4nOzcuwkCQRRAUUeM7MryzO3O3NwKxsxU0JXVu+cUMDyYDywXdsw55w4AAAAAAKDltF97AgAAAAAAgG8QQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIOlzO5/vaQ8DTGMsut+hqwCYs/A4BAMDXzLn2BPA3Xt6WjdynEf3mfXv3NrLvW3a93Y5j98EZAQAAAAAA+FEnv8MCAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAA4NG+HZwAAAIwEMP9h65LCMKRTND/0SQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEg6235vAAAAAAAAeM4TBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEANIrjVUAAAP+SURBVAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACApAvUih4ePerC8AAAAABJRU5ErkJggg==) no-repeat top; }\n  #product-ind .product-card .card {\n    border-radius: 0px;\n    margin: 0px 50px;\n    border: none; }\n  #product-ind .product-card .card .card-body {\n      min-height: 400px;\n      padding: 30px 40px;\n      background: #F8FAFB; }\n  #product-ind .product-card .card .card-body .product-slider {\n        background-color: #fff;\n        border: 1px solid #efefef; }\n  #product-ind .product-card .card .card-body .zoom {\n        font-size: 12px; }\n  #product-ind .product-card .card .card-body .product-details {\n        padding: 0px 0px 0px 40px;\n        display: none; }\n  #product-ind .product-card .card .card-body .product-details.active {\n          display: block; }\n  #product-ind .product-card .card .card-body .product-details .product-title {\n          font-size: 22px;\n          margin-bottom: 30px;\n          font-family: 'us';\n          letter-spacing: 1px; }\n  #product-ind .product-card .card .card-body .product-details h3 {\n          font-size: 18px;\n          font-weight: 500;\n          margin: 0px;\n          font-family: 'us', sans-serif;\n          letter-spacing: 1px; }\n  #product-ind .product-card .card .card-body .product-details p {\n          font-weight: 300; }\n  #product-ind .product-card .card .card-body .product-details .sharing-icon span {\n          margin-left: 5px; }\n  #product-ind .product-card .card .card-body .product-details .sharing-icon span a {\n            color: #333;\n            font-size: 20px; }\n  #product-ind .product-card .card .card-body .product-details .sharing-icon button {\n          border: none;\n          background: none;\n          cursor: pointer;\n          color: #5f6165;\n          font-size: 1.25rem; }\n  #product-ind .product-card .card .card-body .product-details .sharing-icon button:hover, #product-ind .product-card .card .card-body .product-details .sharing-icon button:focus {\n            outline: 0;\n            -webkit-box-shadow: none;\n                    box-shadow: none; }\n  #product-ind .product-card .card .card-body .product-details .back a {\n          color: #333;\n          cursor: pointer; }\n  #product-ind .product-card .card .card-body .product-details .back a .ion-ios-arrow-thin-left {\n            font-size: 36px;\n            vertical-align: middle;\n            margin-right: 5px;\n            -webkit-transition: all ease 0.3s;\n            transition: all ease 0.3s; }\n  #product-ind .product-card .card .card-body .product-details .back a:hover .ion-ios-arrow-thin-left {\n            margin-right: 10px;\n            -webkit-transition: all ease 0.3s;\n            transition: all ease 0.3s; }\n  #product-ind .product-card .card .card-body .zoomDiv {\n        min-height: 100%;\n        min-width: 100%;\n        background-repeat: no-repeat;\n        display: none; }\n  #product-ind .product-card .card .card-body .zoomDiv.active {\n          display: block; }\n  @media only screen and (max-width: 990px) {\n      #product-ind .product-card .card {\n        margin: 0px 0px; } }\n  @media only screen and (max-width: 990px) {\n    #product-ind .product-card {\n      margin-top: 30px;\n      margin-bottom: 30px; } }\n  #product-ind .swiper-container {\n    width: 100%;\n    margin-left: auto;\n    margin-right: auto; }\n  #product-ind .swiper-slide {\n    background-size: cover;\n    background-position: center; }\n  #product-ind .gallery-top {\n    height: auto;\n    width: 100%;\n    text-align: center; }\n  #product-ind .gallery-top img {\n      margin-top: 30px;\n      margin-bottom: 30px; }\n  #product-ind .gallery-thumbs {\n    height: 20%;\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box;\n    padding: 10px 0; }\n  #product-ind .gallery-thumbs .swiper-slide {\n    width: 20%;\n    opacity: 0.4; }\n  #product-ind .gallery-thumbs .swiper-slide img {\n      width: 85%;\n      padding: 5px;\n      border: 1px solid #efefef; }\n  #product-ind .gallery-thumbs .swiper-slide-active {\n    opacity: 1; }\n  #product-ind .button-next,\n  #product-ind .button-prev {\n    position: absolute;\n    top: 25%;\n    font-size: 30px;\n    color: #ccc;\n    z-index: 9999; }\n  #product-ind .button-next {\n    right: 0; }\n  #product-ind .button-prev {\n    left: 0; }\n  #product-ind .breadcrumb {\n    padding: 5px 0px;\n    background: transparent;\n    margin: 0px 50px; }\n  #product-ind .breadcrumb a {\n      color: #fff; }\n  @media only screen and (max-width: 990px) {\n    #product-ind {\n      height: auto; }\n      #product-ind .breadcrumb {\n        margin: 0px; } }\n  .rViewSlider {\n  margin: 50px 0px; }\n  .rViewSlider .rbutton-next,\n  .rViewSlider .rbutton-prev {\n    position: absolute;\n    top: 50%;\n    z-index: 999;\n    font-size: 40px;\n    outline: none;\n    -webkit-box-shadow: none;\n            box-shadow: none;\n    cursor: pointer; }\n  .rViewSlider .rbutton-next {\n    right: 0;\n    right: -15px; }\n  .rViewSlider .rbutton-prev {\n    left: 0;\n    left: -15px; }\n  .rViewSlider h2 {\n    font-size: 36px;\n    font-family: 'us';\n    text-align: center; }\n  @media only screen and (max-width: 768px) {\n  #product-ind {\n    padding: 30px 0px; }\n    #product-ind .product-card .card {\n      margin: 0px; } }\n  @media only screen and (max-width: 500px) {\n  #product-ind {\n    height: auto; }\n    #product-ind .product-card .card .card-body {\n      padding: 0px 15px; }\n      #product-ind .product-card .card .card-body .product-details {\n        padding: 15px 0px; }\n        #product-ind .product-card .card .card-body .product-details h3 {\n          font-size: 15px; }\n        #product-ind .product-card .card .card-body .product-details p {\n          font-size: 14px; }\n    #product-ind .breadcrumb {\n      margin: 0px;\n      font-size: 12px; }\n      #product-ind .breadcrumb a {\n        font-size: 12px; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/accessories/accessories-prod/accessories-prod.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccessoriesProdComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global_service__ = __webpack_require__("../../../../../src/app/global.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__ = __webpack_require__("../../../../angular2-useful-swiper/lib/swiper.module.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AccessoriesProdComponent = /** @class */ (function () {
    function AccessoriesProdComponent(_location, route, router, http, global, spinnerService, ngZone) {
        var _this = this;
        this._location = _location;
        this.route = route;
        this.router = router;
        this.http = http;
        this.global = global;
        this.spinnerService = spinnerService;
        this.ngZone = ngZone;
        this.galleryTopConfig = {
            spaceBetween: 30,
            slidesPerView: 1
        };
        this.galleryThumbsConfig = {
            nextButton: '.button-next',
            prevButton: '.button-prev',
            spaceBetween: 10,
            centeredSlides: true,
            slidesPerView: 'auto',
            touchRatio: 0.2,
            slideToClickedSlide: true
        };
        this.relatedView = {
            nextButton: '.rbutton-next',
            prevButton: '.rbutton-prev',
            spaceBetween: 30,
            slidesPerView: 4
        };
        this.prod = this.route.params.subscribe(function (params) { return _this.urlParams = params; });
        this.prodUrl = this.global.baseURL + '/accessories' + '/' + this.urlParams.accessoriesName + '/' + this.urlParams.accessoriesId;
    }
    AccessoriesProdComponent.prototype.getData = function () {
        return this.http.get(this.prodUrl);
    };
    AccessoriesProdComponent.prototype.getProdData = function () {
        var _this = this;
        this.getData().subscribe(function (data) {
            _this.productDetail = data;
            _this.pName = _this.productDetail.prodData.name;
            _this.pAttributes = _this.productDetail.prodData.attributes;
            _this.pImg1 = _this.productDetail.prodData.image1;
            _this.pImg2 = _this.productDetail.prodData.image2;
            _this.pImg3 = _this.productDetail.prodData.image3;
            _this.pImg4 = _this.productDetail.prodData.image4;
            _this.pImgH1 = _this.productDetail.prodData.hover1;
            _this.pImgH2 = _this.productDetail.prodData.hover2;
            _this.pImgH3 = _this.productDetail.prodData.hover3;
            _this.pImgH4 = _this.productDetail.prodData.hover4;
            _this.otherProd = _this.productDetail.otherProds;
            console.log(_this.otherProd);
            _this.spinnerService.hide();
        });
    };
    AccessoriesProdComponent.prototype.goBack = function () {
        this._location.back();
    };
    AccessoriesProdComponent.prototype.viewProd = function (prodId) {
        window.location.reload();
        this.router.navigate(['/accessories', this.urlParams.accessoriesName, prodId], { relativeTo: this.route });
    };
    AccessoriesProdComponent.prototype.imageZoom = function (imgID) {
        if (!($('#resultZoomDiv').hasClass('active'))) {
            $('#resultZoomDiv').addClass('active');
            $('.product-details').removeClass('active');
        }
        var img, lens, result, cx, cy;
        img = document.getElementById(imgID);
        result = document.getElementById('resultZoomDiv');
        /*create lens:*/
        lens = document.createElement("DIV");
        lens.setAttribute("class", "img-zoom-lens");
        /*insert lens:*/
        if ($(img.parentElement).find('.img-zoom-lens').length == 0)
            img.parentElement.insertBefore(lens, img);
        /*calculate the ratio between result DIV and lens:*/
        cx = result.offsetWidth / lens.offsetWidth;
        cy = result.offsetHeight / lens.offsetHeight;
        /*set background properties for the result DIV:*/
        result.style.backgroundImage = "url('" + img.src + "')";
        result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
        /*execute a function when someone moves the cursor over the image, or the lens:*/
        lens.addEventListener("mousemove", moveLens);
        img.addEventListener("mousemove", moveLens);
        /*and also for touch screens:*/
        lens.addEventListener("touchmove", moveLens);
        img.addEventListener("touchmove", moveLens);
        function moveLens(e) {
            var pos, x, y;
            /*prevent any other actions that may occur when moving over the image:*/
            e.preventDefault();
            /*get the cursor's x and y positions:*/
            pos = getCursorPos(e);
            /*calculate the position of the lens:*/
            // console.log(pos);
            // console.log(lens.offsetHeight);
            // console.log(lens.offsetWidth);
            x = pos.x - (lens.offsetWidth / 2);
            y = pos.y - (lens.offsetHeight / 2);
            /*prevent the lens from being positioned outside the image:*/
            if (x > img.width - lens.offsetWidth) {
                x = img.width - lens.offsetWidth;
            }
            if (x < 0) {
                x = 0;
            }
            if (y > img.height - lens.offsetHeight) {
                y = img.height - lens.offsetHeight;
            }
            if (y < 0) {
                y = 0;
            }
            /*set the position of the lens:*/
            lens.style.left = x + "px";
            lens.style.top = y + "px";
            /*display what the lens "sees":*/
            result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
        }
        function getCursorPos(e) {
            var a, x = 0, y = 0;
            e = e || window.event;
            /*get the x and y positions of the image:*/
            a = img.getBoundingClientRect();
            /*calculate the cursor's x and y coordinates, relative to the image:*/
            x = e.pageX - a.left;
            y = e.pageY - a.top;
            /*consider any page scrolling:*/
            x = x - window.pageXOffset;
            y = y - window.pageYOffset;
            return {
                x: x,
                y: y
            };
        }
    };
    AccessoriesProdComponent.prototype.resetZoom = function () {
        $('#resultZoomDiv').removeClass('active');
        $('.product-details').addClass('active');
        console.log('hide zoom');
    };
    AccessoriesProdComponent.prototype.ngOnInit = function () {
        this.getProdData();
        $(document).ready(function () {
            fadeInUp();
        });
    };
    AccessoriesProdComponent.prototype.ngAfterViewInit = function () {
        this.galleryTop.swiper.params.control = this.galleryThumbs.swiper;
        this.galleryThumbs.swiper.params.control = this.galleryTop.swiper;
        // if(this.elevatezoomBig){
        //   $(this.elevatezoomBig.nativeElement).elevateZoom({
        //       borderSize: 1,
        //       lensFadeIn: 200,
        //       cursor: 'crosshair',
        //       zoomWindowFadeIn: 200,
        //       loadingIcon: true,
        //       zoomWindowOffety: -50,
        //       zoomWindowOffetx: 50,
        //       zoomWindowHeight: 530,
        //       responsive: true
        //   });
        // }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('galleryTop'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__["SwiperComponent"])
    ], AccessoriesProdComponent.prototype, "galleryTop", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('galleryThumbs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__["SwiperComponent"])
    ], AccessoriesProdComponent.prototype, "galleryThumbs", void 0);
    AccessoriesProdComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-accessories-prod',
            template: __webpack_require__("../../../../../src/app/accessories/accessories-prod/accessories-prod.component.html"),
            styles: [__webpack_require__("../../../../../src/app/accessories/accessories-prod/accessories-prod.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common__["Location"], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_5__global_service__["a" /* GlobalService */], __WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner__["Ng4LoadingSpinnerService"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
    ], AccessoriesProdComponent);
    return AccessoriesProdComponent;
}());



/***/ }),

/***/ "../../../../../src/app/accessories/accessories.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid inner-bg \" id=\"collections\">\n    <div class=\"row collection-navigation\" id=\"collectionnavigation\">\n      <div class=\"col-12\">\n        <app-decollection-nav id=\"innerNav\"></app-decollection-nav>\n      </div>\n    </div>\n    <div class=\"row collection-list justify-content-center\" data-spy=\"scroll\" data-target=\"#collectionnavigation\" data-offset=\"100\">\n        <div class=\"col-12 banner-bg d-none d-lg-block\" [ngStyle]=\"Desbgstyle()\"></div>\n        <div class=\"col-12 banner-bg d-block d-lg-none d-xl-none\" [ngStyle]=\"DesbgstyleMobile()\"></div>\n      <div class=\"col-12\">\n        <div class=\"container\">\n          <div class=\"row collection\">\n            <div class=\"col-md-3 col-sm-6\" *ngFor=\"let product of viewcollection.productList\">\n              <a [routerLink]=\"['/accessories', viewcollection.cName, product.id]\"><div class=\"card fade-in\">\n                <div class=\"card-body\">\n                  <img src=\"{{product.image1}}\" class=\"img-fluid\" alt=\"\">\n                  <div class=\"collection-name\">\n                    <h3>{{product.name}}</h3>\n                    <p>{{product.excerpt}}</p>\n                  </div>\n                </div>\n                <button class=\"btn btn-primary btn-block\">VIEW MORE</button>\n              </div></a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n  "

/***/ }),

/***/ "../../../../../src/app/accessories/accessories.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#collections .theam-heading {\n  color: #000; }\n\n#collections .banner-bg {\n  -webkit-animation: background-anim 0.8s linear;\n          animation: background-anim 0.8s linear;\n  background-size: 100% !important; }\n\n#collections .collection {\n  padding-top: 50px;\n  padding-bottom: 50px; }\n\n.sticky {\n  position: fixed;\n  top: 55px;\n  z-index: 9999;\n  left: 0;\n  right: 0;\n  background: #fff; }\n\n@media only screen and (max-width: 480px) and (min-width: 320px) {\n  .sticky {\n    top: 50px; } }\n\n@-webkit-keyframes background-anim {\n  0% {\n    background-size: 150%;\n    opacity: 0.3; }\n  100% {\n    background-size: 100%;\n    opacity: 1; } }\n\n@keyframes background-anim {\n  0% {\n    background-size: 150%;\n    opacity: 0.3; }\n  100% {\n    background-size: 100%;\n    opacity: 1; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/accessories/accessories.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccessoriesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__collections_view_collection_service__ = __webpack_require__("../../../../../src/app/collections/view-collection.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AccessoriesComponent = /** @class */ (function () {
    function AccessoriesComponent(route, router, spinnerService, viewcollection) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.viewcollection = viewcollection;
        this.sub = this.route.params.subscribe(function (params) { return _this.accessName = params.accessoriesName; });
    }
    AccessoriesComponent.prototype.loadAccess = function () {
        this.viewcollection.getAccessData('accessories', this.accessName);
    };
    AccessoriesComponent.prototype.Desbgstyle = function () {
        var DesBg = {
            'background-image': 'url(' + this.viewcollection.cBanner + ')',
            'height': '288px',
            'max-height': '288px',
            'background-position': 'center'
        };
        return DesBg;
    };
    AccessoriesComponent.prototype.DesbgstyleMobile = function () {
        var DesBg = {
            'background-image': 'url(' + this.viewcollection.cMobBanner + ')',
            'height': '288px',
            'max-height': '288px',
            'background-position': 'center'
        };
        return DesBg;
    };
    AccessoriesComponent.prototype.ngOnInit = function () {
        this.loadAccess();
        remFullpage();
        $(document).ready(function () {
            fadeIn();
            window.onscroll = function () { scrollFunction(); };
            function scrollFunction() {
                if (window.pageYOffset >= 20) {
                    $("#innerNav").addClass("sticky");
                }
                else {
                    $("#innerNav").removeClass("sticky");
                }
            }
            // fadeInUp();
            // $.fn.fullpage.destroy('all');
        });
    };
    AccessoriesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-accessories',
            template: __webpack_require__("../../../../../src/app/accessories/accessories.component.html"),
            styles: [__webpack_require__("../../../../../src/app/accessories/accessories.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__["Ng4LoadingSpinnerService"], __WEBPACK_IMPORTED_MODULE_3__collections_view_collection_service__["a" /* ViewCollectionService */]])
    ], AccessoriesComponent);
    return AccessoriesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/accessories/view-accessories.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewAccessoriesService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ViewAccessoriesService = /** @class */ (function () {
    function ViewAccessoriesService() {
    }
    ViewAccessoriesService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ViewAccessoriesService);
    return ViewAccessoriesService;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__seo_service__ = __webpack_require__("../../../../../src/app/seo.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = /** @class */ (function () {
    function AppComponent(ng4LoadingSpinnerService, router, seoService, ngZone) {
        this.ng4LoadingSpinnerService = ng4LoadingSpinnerService;
        this.router = router;
        this.seoService = seoService;
        this.ngZone = ngZone;
        this.title = 'app';
        this.loadTemplate = "<img src=\"http://pa1.narvii.com/5722/2c617cd9674417d272084884b61e4bb7dd5f0b15_hq.gif\" />";
        seoService.addSeoData();
        //  router.events.subscribe((event: RouterEvent) => {
        //   this.navigationInterceptor(event)
        // })
    }
    // Shows and hides the loading spinner during RouterEvent changes
    AppComponent.prototype.navigationInterceptor = function (event) {
        if (event instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["e" /* NavigationStart */]) {
            this.ng4LoadingSpinnerService.show();
        }
        if (event instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* NavigationEnd */]) {
            this.ng4LoadingSpinnerService.hide();
        }
        // // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* NavigationCancel */]) {
            this.ng4LoadingSpinnerService.hide();
        }
        if (event instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* NavigationError */]) {
            this.ng4LoadingSpinnerService.hide();
        }
    };
    // this.ng4LoadingSpinnerService.show();
    // this.ng4LoadingSpinnerService.hide();
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.subscribe(function (evt) {
            _this.navigationInterceptor(evt);
            window.scrollTo(0, 0);
            if (!(evt instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* NavigationEnd */])) {
                return;
            }
        });
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: "\n\n    <app-header></app-header>\n    <ng4-loading-spinner [threshold]=\"2000\" [template]=\"loadTemplate\" [loadingText]=\"'Please wait...'\" [zIndex]=\"9999999\"> </ng4-loading-spinner>\n    <router-outlet >    </router-outlet>\n    <app-footer></app-footer>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ng4_loading_spinner__["Ng4LoadingSpinnerService"], __WEBPACK_IMPORTED_MODULE_2__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_3__seo_service__["a" /* SeoService */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_fullpage__ = __webpack_require__("../../../../ngx-fullpage/ngx-fullpage.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_fullpage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngx_fullpage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__ = __webpack_require__("../../../../angular2-useful-swiper/lib/swiper.module.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__collections_view_collection_service__ = __webpack_require__("../../../../../src/app/collections/view-collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__seo_service__ = __webpack_require__("../../../../../src/app/seo.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__header_footer_service__ = __webpack_require__("../../../../../src/app/header-footer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ngx_share_core__ = __webpack_require__("../../../../@ngx-share/core/esm5/ngx-share-core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__header_header_component__ = __webpack_require__("../../../../../src/app/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__home_home_component__ = __webpack_require__("../../../../../src/app/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__footer_footer_component__ = __webpack_require__("../../../../../src/app/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__contactus_contactus_component__ = __webpack_require__("../../../../../src/app/contactus/contactus.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__collections_collections_component__ = __webpack_require__("../../../../../src/app/collections/collections.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__collections_decollection_nav_decollection_nav_component__ = __webpack_require__("../../../../../src/app/collections/decollection-nav/decollection-nav.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__collections_product_product_component__ = __webpack_require__("../../../../../src/app/collections/product/product.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__aboutus_aboutus_component__ = __webpack_require__("../../../../../src/app/aboutus/aboutus.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__page_not_found_page_not_found_component__ = __webpack_require__("../../../../../src/app/page-not-found/page-not-found.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__global_service__ = __webpack_require__("../../../../../src/app/global.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__accessories_accessories_component__ = __webpack_require__("../../../../../src/app/accessories/accessories.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__accessories_accessories_prod_accessories_prod_component__ = __webpack_require__("../../../../../src/app/accessories/accessories-prod/accessories-prod.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__accessories_view_accessories_service__ = __webpack_require__("../../../../../src/app/accessories/view-accessories.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























var routes = [
    { 'path': '', 'redirectTo': '/home', 'pathMatch': 'full' },
    { 'path': 'home',
        'component': __WEBPACK_IMPORTED_MODULE_14__home_home_component__["a" /* HomeComponent */],
        'data': {
            title: "Home",
            metatags: {
                description: "Page Description or some content here",
                keywords: "home"
            }
        }
    },
    {
        'path': 'contactus',
        'component': __WEBPACK_IMPORTED_MODULE_16__contactus_contactus_component__["a" /* ContactusComponent */],
        'data': {
            title: "Contact Us",
            metatags: {
                description: "Page Description or some content here",
                keywords: "Contact Us"
            }
        }
    },
    {
        'path': 'collections/:collectionName',
        'component': __WEBPACK_IMPORTED_MODULE_17__collections_collections_component__["a" /* CollectionsComponent */],
        'data': {
            title: "Collection",
            metatags: {
                description: "Page Description or some content here",
                keywords: "Collection"
            }
        },
        pathMatch: 'full'
    },
    {
        'path': 'collections/:collectionName/:productId',
        'component': __WEBPACK_IMPORTED_MODULE_19__collections_product_product_component__["a" /* ProductComponent */],
        'data': {
            title: "Collection",
            metatags: {
                description: "Page Description or some content here",
                keywords: "Collection"
            }
        },
        pathMatch: 'full'
    },
    {
        'path': 'accessories/:accessoriesName',
        'component': __WEBPACK_IMPORTED_MODULE_23__accessories_accessories_component__["a" /* AccessoriesComponent */],
        'data': {
            title: "Accessories",
            metatags: {
                description: "Page Description or some content here",
                keywords: "Accessories"
            }
        },
        pathMatch: 'full'
    },
    {
        'path': 'accessories/:accessoriesName/:accessoriesId',
        'component': __WEBPACK_IMPORTED_MODULE_24__accessories_accessories_prod_accessories_prod_component__["a" /* AccessoriesProdComponent */],
        'data': {
            title: "accessories",
            metatags: {
                description: "Page Description or some content here",
                keywords: "accessories"
            }
        },
        pathMatch: 'full'
    },
    {
        'path': 'aboutus',
        'component': __WEBPACK_IMPORTED_MODULE_20__aboutus_aboutus_component__["a" /* AboutusComponent */],
        'data': {
            title: "About Us",
            metatags: {
                description: "Page Description or some content here",
                keywords: "About Us"
            }
        }
    },
    { 'path': '**',
        'component': __WEBPACK_IMPORTED_MODULE_21__page_not_found_page_not_found_component__["a" /* PageNotFoundComponent */]
    }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_13__header_header_component__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_14__home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_15__footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_16__contactus_contactus_component__["a" /* ContactusComponent */],
                __WEBPACK_IMPORTED_MODULE_17__collections_collections_component__["a" /* CollectionsComponent */],
                __WEBPACK_IMPORTED_MODULE_18__collections_decollection_nav_decollection_nav_component__["a" /* DecollectionNavComponent */],
                __WEBPACK_IMPORTED_MODULE_19__collections_product_product_component__["a" /* ProductComponent */],
                __WEBPACK_IMPORTED_MODULE_20__aboutus_aboutus_component__["a" /* AboutusComponent */],
                __WEBPACK_IMPORTED_MODULE_21__page_not_found_page_not_found_component__["a" /* PageNotFoundComponent */],
                __WEBPACK_IMPORTED_MODULE_23__accessories_accessories_component__["a" /* AccessoriesComponent */],
                __WEBPACK_IMPORTED_MODULE_24__accessories_accessories_prod_accessories_prod_component__["a" /* AccessoriesProdComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */].withServerTransition({ appId: 'ang4-seo-pre' }),
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */].forRoot(routes),
                __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_3_ngx_fullpage__["MnFullpageModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner__["Ng4LoadingSpinnerModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__["SwiperModule"],
                __WEBPACK_IMPORTED_MODULE_7__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_12__ngx_share_core__["b" /* ShareModule */].forRoot()
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_9__collections_view_collection_service__["a" /* ViewCollectionService */], __WEBPACK_IMPORTED_MODULE_10__seo_service__["a" /* SeoService */], __WEBPACK_IMPORTED_MODULE_11__header_footer_service__["a" /* HeaderFooterService */], __WEBPACK_IMPORTED_MODULE_22__global_service__["a" /* GlobalService */], __WEBPACK_IMPORTED_MODULE_25__accessories_view_accessories_service__["a" /* ViewAccessoriesService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/collections/collections.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid inner-bg \" id=\"collections\">\n  <div class=\"row collection-navigation\" id=\"collectionnavigation\">\n    <div class=\"col-12\">\n      <app-decollection-nav id=\"innerNav\"></app-decollection-nav>\n    </div>\n  </div>\n  <div class=\"row collection-list justify-content-center\" data-spy=\"scroll\" data-target=\"#collectionnavigation\" data-offset=\"100\">\n    <div class=\"col-12 banner-bg d-none d-lg-block\" [ngStyle]=\"Desbgstyle()\"></div>\n    <div class=\"col-12 banner-bg d-block d-lg-none d-xl-none\" [ngStyle]=\"DesbgstyleMobile()\"></div>\n    <div class=\"col-12\">\n      <div class=\"container\">\n        <div class=\"row collection\">\n          <div class=\"col-md-3 col-sm-6\" *ngFor=\"let product of viewcollection.productList\">\n            <a [routerLink]=\"['/collections', viewcollection.cName, product.id]\"><div class=\"card fade-in\">\n              <div class=\"card-body\">\n                <img src=\"{{product.image1}}\" class=\"img-fluid\" alt=\"\">\n                <div class=\"collection-name\">\n                  <h3>{{product.name}}</h3>\n                  <p>{{product.excerpt}}</p>\n                </div>\n              </div>\n              <button class=\"btn btn-primary btn-block\">VIEW MORE</button>\n            </div></a>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n"

/***/ }),

/***/ "../../../../../src/app/collections/collections.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#collections .theam-heading {\n  color: #000; }\n\n#collections .banner-bg {\n  -webkit-animation: background-anim 0.8s linear;\n          animation: background-anim 0.8s linear;\n  background-size: 100% !important; }\n\n#collections .collection {\n  padding-top: 50px;\n  padding-bottom: 50px; }\n\n.sticky {\n  position: fixed;\n  top: 55px;\n  z-index: 9999;\n  left: 0;\n  right: 0;\n  background: #fff; }\n\n@media only screen and (max-width: 480px) and (min-width: 320px) {\n  .sticky {\n    top: 50px; } }\n\n@-webkit-keyframes background-anim {\n  0% {\n    background-size: 150%;\n    opacity: 0.3; }\n  100% {\n    background-size: 100%;\n    opacity: 1; } }\n\n@keyframes background-anim {\n  0% {\n    background-size: 150%;\n    opacity: 0.3; }\n  100% {\n    background-size: 100%;\n    opacity: 1; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/collections/collections.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollectionsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__collections_view_collection_service__ = __webpack_require__("../../../../../src/app/collections/view-collection.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CollectionsComponent = /** @class */ (function () {
    function CollectionsComponent(route, router, spinnerService, viewcollection) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.viewcollection = viewcollection;
        this.sub = this.route.params.subscribe(function (params) { return _this.collectionName = params.collectionName; });
    }
    CollectionsComponent.prototype.dataload = function () {
        this.viewcollection.viewCollection(this.collectionName);
    };
    CollectionsComponent.prototype.ngOnInit = function () {
        this.dataload();
        remFullpage();
        $(document).ready(function () {
            fadeIn();
            window.onscroll = function () { scrollFunction(); };
            function scrollFunction() {
                if (window.pageYOffset >= 20) {
                    $("#innerNav").addClass("sticky");
                }
                else {
                    $("#innerNav").removeClass("sticky");
                }
            }
            fadeInUp();
            // $.fn.fullpage.destroy('all');
        });
    };
    CollectionsComponent.prototype.Desbgstyle = function () {
        var DesBg = {
            'background-image': 'url(' + this.viewcollection.cBanner + ')',
            'height': '288px',
            'max-height': '288px',
            'background-position': 'center',
            'background-repeat': 'no-repeat'
        };
        return DesBg;
    };
    CollectionsComponent.prototype.DesbgstyleMobile = function () {
        var DesBg = {
            'background-image': 'url(' + this.viewcollection.cMobBanner + ')',
            'height': '288px',
            'max-height': '288px',
            'background-position': 'center',
            'background-repeat': 'no-repeat'
        };
        return DesBg;
    };
    CollectionsComponent.prototype.viewProduct = function (productId) {
        this.id = productId;
        this.router.navigate(['/collections', this.collectionName, this.id]);
    };
    CollectionsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-collections',
            template: __webpack_require__("../../../../../src/app/collections/collections.component.html"),
            styles: [__webpack_require__("../../../../../src/app/collections/collections.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__["Ng4LoadingSpinnerService"], __WEBPACK_IMPORTED_MODULE_3__collections_view_collection_service__["a" /* ViewCollectionService */]])
    ], CollectionsComponent);
    return CollectionsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/collections/decollection-nav/decollection-nav.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"\">\n\t<ul class=\"nav\">\n\t  <li class=\"nav-item\"  *ngFor=\"let categorie of viewcollection.categories\">\n\t    <a *ngIf=\"categorie.type == 2\" class=\"nav-link\" data-toggle=\"tab\" routerLink=\"/accessories/{{categorie.slug}}\" routerLinkActive=\"active\" (click)=\"changeData(categorie.type == '2' ? 'accessories' : 'collections', categorie.slug)\">{{categorie.name}}</a>\n      <a *ngIf=\"categorie.type != 2\" class=\"nav-link\" data-toggle=\"tab\" routerLink=\"/collections/{{categorie.slug}}\" routerLinkActive=\"active\" (click)=\"changeData(categorie.type == '2' ? 'accessories' : 'collections', categorie.slug)\">{{categorie.name}}</a>\n\t  </li>\n\t</ul>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/collections/decollection-nav/decollection-nav.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/collections/decollection-nav/decollection-nav.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DecollectionNavComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__collections_view_collection_service__ = __webpack_require__("../../../../../src/app/collections/view-collection.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DecollectionNavComponent = /** @class */ (function () {
    function DecollectionNavComponent(viewcollection, router, activeRoute) {
        var _this = this;
        this.viewcollection = viewcollection;
        this.router = router;
        this.activeRoute = activeRoute;
        this.activeParam = this.activeRoute.params.subscribe(function (params) { return _this.activeItem = params; });
    }
    DecollectionNavComponent.prototype.changeData = function (aName, cName) {
        this.selectedItem = cName;
        this.router.navigate(['/' + aName, cName]);
        this.viewcollection.getAccessData(aName, cName);
        this.activeSlug = cName;
    };
    DecollectionNavComponent.prototype.ngOnInit = function () {
        this.selectedItem = this.activeItem.accessoriesName || this.activeItem.collectionName;
        $(document).ready(function () {
        });
    };
    DecollectionNavComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-decollection-nav',
            template: __webpack_require__("../../../../../src/app/collections/decollection-nav/decollection-nav.component.html"),
            styles: [__webpack_require__("../../../../../src/app/collections/decollection-nav/decollection-nav.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__collections_view_collection_service__["a" /* ViewCollectionService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]])
    ], DecollectionNavComponent);
    return DecollectionNavComponent;
}());



/***/ }),

/***/ "../../../../../src/app/collections/product/product.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid inner-bg smoothstate\" id=\"product-ind\">\n  <div class=\"container h-100\">\n    <div class=\"row h-100 align-items-center\">\n      <div class=\"col-12 product-card\">\n        <div class=\"card fade-in-up\">\n          <div class=\"card-body\">\n            <div class=\"row h-100\">\n              <div class=\"col-12 col-md-5 product-slider fade-in-up\">\n                <swiper class=\"swiper-container gallery-top\" #galleryTop [config]=\"galleryTopConfig\">\n                  <div class=\"swiper-wrapper\">\n                    <div *ngIf=\"pImg1!=null\" class=\"swiper-slide\" (mouseenter)=\"imageZoom('myimage');\" (mouseleave)=\"resetZoom();\">\n                      <img src=\"{{pImg1}}\" id=\"myimage\" class=\"img-fluid\" alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg2!=null\" class=\"swiper-slide\" (mouseenter)=\"imageZoom('myimage2');\" (mouseleave)=\"resetZoom();\">\n                      <img src=\"{{pImg2}}\" id=\"myimage2\" class=\"img-fluid \" #elevatezoomBig [attr.data-zoom-image]=\"pImgH2\"\n                        alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg3!=null\" class=\"swiper-slide\" (mouseenter)=\"imageZoom('myimage3');\" (mouseleave)=\"resetZoom();\">\n                      <img src=\"{{pImg3}}\" id=\"myimage3\" class=\"img-fluid \" #elevatezoomBig [attr.data-zoom-image]=\"pImgH3\"\n                        alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg4!=null\" class=\"swiper-slide\" (mouseenter)=\"imageZoom('myimage4');\" (mouseleave)=\"resetZoom();\">\n                      <img src=\"{{pImg4}}\" id=\"myimage4\" class=\"img-fluid \" #elevatezoomBig [attr.data-zoom-image]=\"pImgH4\"\n                        alt=\"\">\n                    </div>\n                  </div>\n                </swiper>\n                <swiper class=\"swiper-container gallery-thumbs\" #galleryThumbs [config]=\"galleryThumbsConfig\">\n                  <div class=\"swiper-wrapper\">\n                    <div *ngIf=\"pImg1!=null\" class=\"swiper-slide\">\n                      <img src=\"{{pImg1}}\" class=\"img-fluid\" alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg2!=null\" class=\"swiper-slide\">\n                      <img src=\"{{pImg2}}\" class=\"img-fluid\" alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg3!=null\" class=\"swiper-slide\">\n                      <img src=\"{{pImg3}}\" class=\"img-fluid\" alt=\"\">\n                    </div>\n                    <div *ngIf=\"pImg4!=null\" class=\"swiper-slide\">\n                      <img src=\"{{pImg4}}\" class=\"img-fluid\" alt=\"\">\n                    </div>\n                  </div>\n                  <div class=\"button-next\">\n                    <i class=\"ion-ios-arrow-right\"></i>\n                  </div>\n                  <div class=\"button-prev\">\n                    <i class=\"ion-ios-arrow-left\"></i>\n                  </div>\n                </swiper>\n              </div>\n              <div class=\"col-12 col-md-7\">\n                <div class=\"product-details active fade-in-up\">\n                  <div class=\"row\">\n                    <div class=\"col-8\">\n                      <h1 class=\"product-title\">{{pName}}</h1>\n                    </div>\n                    <div class=\"col-4 back\">\n                      <a (click)=\"goBack()\" class=\"float-right\">\n                        <i class=\"ion-close\"></i></a>\n                    </div>\n                  </div>\n                  <div class=\"row\">\n                    <div class=\"col-6\" *ngFor=\"let attribute of pAttributes\">\n                      <h3>{{attribute.key}}</h3>\n                      <p>{{attribute.value}}</p>\n                    </div>\n                  </div>\n                  <div class=\"row mt-20\">\n                    <div class=\"col-8\">\n                      <div class=\"sharing-icon\">\n                        <p class=\"d-inline-block\">Share on</p>\n                        <span class=\"d-inline-block\">\n                          <button shareButton=\"facebook\"><i class=\"ion-social-facebook\"></i></button>\n                          <button shareButton=\"google\">\n                            <svg style=\"padding-top:5px;\" xmlns=\"http://www.w3.org/2000/svg\" width=\"25\" height=\"20\"\n                              viewBox=\"0 0 35 20\">\n                              <path fill=\"#5F6165\" d=\"M10,8.57142857 L10,12 L15.6714286,12 C15.4428571,13.47 13.9571429,16.3142857 10,16.3142857 C6.58571429,16.3142857 3.8,13.4871429 3.8,10 C3.8,6.51428571 6.58571429,3.68571429 10,3.68571429 C11.9428571,3.68571429 13.2428571,4.51428571 13.9857143,5.22857143 L16.7,2.61428571 C14.9571429,0.985714286 12.7,1.77635684e-15 10,1.77635684e-15 C4.47142857,1.77635684e-15 -8.8817842e-16,4.47142857 -8.8817842e-16,10 C-8.8817842e-16,15.5285714 4.47142857,20 10,20 C15.7714286,20 19.6014286,15.9428571 19.6014286,10.2285714 C19.6014286,9.57142857 19.5285714,9.07142857 19.4428571,8.57142857 L10,8.57142857 Z M10,8.57142857 L34.2857143,11.4285714 L30,11.4285714 L30,15.7142857 L27.1428571,15.7142857 L27.1428571,11.4285714 L22.8571429,11.4285714 L22.8571429,8.57142857 L27.1428571,8.57142857 L27.1428571,4.28571429 L30,4.28571429 L30,8.57142857 L34.2857143,8.57142857 L34.2857143,11.4285714 L10,8.57142857 Z\" />\n                            </svg>\n                          </button>\n                          <button shareButton=\"twitter\"><i class=\"ion-social-twitter\"></i></button>\n                          <button shareButton=\"whatsapp\"><i class=\"ion-social-whatsapp\"></i></button>\n                          <button shareButton=\"pinterest\"><i class=\"ion-social-pinterest\"></i></button>\n                        </span>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n\n                <div id=\"resultZoomDiv\" class=\"zoomDiv\">\n\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"fade-in-up\">\n          <div aria-label=\"breadcrumb\">\n            <ol class=\"breadcrumb\">\n              <li class=\"breadcrumb-item\">\n                <a [routerLink]=\"['/collections', urlParams.collectionName]\">Collections</a>\n              </li>\n              <li class=\"breadcrumb-item\">\n                <a [routerLink]=\"['/collections', urlParams.collectionName]\">{{urlParams.collectionName}}</a>\n              </li>\n              <li class=\"breadcrumb-item active\" aria-current=\"page\">{{pName}}</li>\n            </ol>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"container-fluid\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-12 collection rViewSlider\">\n        <h2>Related VIEWED</h2>\n        <swiper class=\"swiper-container\" [config]=\"relatedView\">\n          <div class=\"swiper-wrapper\">\n            <div class=\"swiper-slide\" *ngFor=\"let prods of otherProd\">\n              <a (click)=\"viewProd(prods.id)\">\n                <div class=\"card fade-in\">\n                  <div class=\"card-body\">\n                    <img src=\"{{prods.image1}}\" class=\"img-fluid\" alt=\"\">\n                    <div class=\"collection-name\">\n                      <h3>{{prods.name}}</h3>\n                      <p>{{prods.excerpt}}</p>\n                    </div>\n                  </div>\n                  <a class=\"btn btn-primary btn-block\">VIEW MORE</a>\n                </div>\n              </a>\n            </div>\n          </div>\n        </swiper>\n        <div class=\"rbutton-next\">\n          <i class=\"ion-ios-arrow-right\"></i>\n        </div>\n        <div class=\"rbutton-prev\">\n          <i class=\"ion-ios-arrow-left\"></i>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/collections/product/product.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#product-ind {\n  height: 100vh;\n  /* Portrait and Landscape */ }\n  #product-ind.inner-bg {\n    background: #000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABkEAAAQuCAYAAABrr5qaAAAACXBIWXMAAAsSAAALEgHS3X78AAAgAElEQVR4nOzcuwkCQRRAUUeM7MryzO3O3NwKxsxU0JXVu+cUMDyYDywXdsw55w4AAAAAAKDltF97AgAAAAAAgG8QQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIOlzO5/vaQ8DTGMsut+hqwCYs/A4BAMDXzLn2BPA3Xt6WjdynEf3mfXv3NrLvW3a93Y5j98EZAQAAAAAA+FEnv8MCAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAA4NG+HZwAAAIwEMP9h65LCMKRTND/0SQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEg6235vAAAAAAAAeM4TBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEANIrjVUAAAP+SURBVAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACApAvUih4ePerC8AAAAABJRU5ErkJggg==) no-repeat top; }\n  #product-ind .product-card .card {\n    border-radius: 0px;\n    margin: 0px 50px;\n    border: none; }\n  #product-ind .product-card .card .card-body {\n      min-height: 400px;\n      padding: 30px 40px;\n      background: #F8FAFB; }\n  #product-ind .product-card .card .card-body .product-slider {\n        background-color: #fff;\n        border: 1px solid #efefef; }\n  #product-ind .product-card .card .card-body .zoom {\n        font-size: 12px; }\n  #product-ind .product-card .card .card-body .product-details {\n        padding: 0px 0px 0px 40px;\n        display: none; }\n  #product-ind .product-card .card .card-body .product-details.active {\n          display: block; }\n  #product-ind .product-card .card .card-body .product-details .product-title {\n          font-size: 22px;\n          margin-bottom: 30px;\n          font-family: 'us';\n          letter-spacing: 1px; }\n  #product-ind .product-card .card .card-body .product-details h3 {\n          font-size: 18px;\n          font-weight: 500;\n          margin: 0px;\n          font-family: 'us', sans-serif;\n          letter-spacing: 1px; }\n  #product-ind .product-card .card .card-body .product-details p {\n          font-weight: 300; }\n  #product-ind .product-card .card .card-body .product-details .sharing-icon span {\n          margin-left: 5px; }\n  #product-ind .product-card .card .card-body .product-details .sharing-icon span a {\n            color: #333;\n            font-size: 20px; }\n  #product-ind .product-card .card .card-body .product-details .sharing-icon button {\n          border: none;\n          background: none;\n          cursor: pointer;\n          color: #5f6165;\n          font-size: 1.25rem; }\n  #product-ind .product-card .card .card-body .product-details .sharing-icon button:hover, #product-ind .product-card .card .card-body .product-details .sharing-icon button:focus {\n            outline: 0;\n            -webkit-box-shadow: none;\n                    box-shadow: none; }\n  #product-ind .product-card .card .card-body .product-details .back a {\n          color: #333;\n          cursor: pointer; }\n  #product-ind .product-card .card .card-body .product-details .back a .ion-ios-arrow-thin-left {\n            font-size: 36px;\n            vertical-align: middle;\n            margin-right: 5px;\n            -webkit-transition: all ease 0.3s;\n            transition: all ease 0.3s; }\n  #product-ind .product-card .card .card-body .product-details .back a:hover .ion-ios-arrow-thin-left {\n            margin-right: 10px;\n            -webkit-transition: all ease 0.3s;\n            transition: all ease 0.3s; }\n  #product-ind .product-card .card .card-body .zoomDiv {\n        min-height: 100%;\n        min-width: 100%;\n        background-repeat: no-repeat;\n        display: none; }\n  #product-ind .product-card .card .card-body .zoomDiv.active {\n          display: block; }\n  @media only screen and (max-width: 990px) {\n      #product-ind .product-card .card {\n        margin: 0px 0px; } }\n  @media only screen and (max-width: 990px) {\n    #product-ind .product-card {\n      margin-top: 30px;\n      margin-bottom: 30px; } }\n  #product-ind .swiper-container {\n    width: 100%;\n    margin-left: auto;\n    margin-right: auto; }\n  #product-ind .swiper-slide {\n    background-size: cover;\n    background-position: center; }\n  #product-ind .gallery-top {\n    height: auto;\n    width: 100%;\n    text-align: center; }\n  #product-ind .gallery-top img {\n      margin-top: 30px;\n      margin-bottom: 30px;\n      max-width: 368px;\n      max-height: 368px; }\n  @media only screen and (max-width: 1366px) {\n        #product-ind .gallery-top img {\n          max-width: 300px;\n          max-height: 300px; } }\n  #product-ind .gallery-thumbs {\n    height: 20%;\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box;\n    padding: 10px 0; }\n  #product-ind .gallery-thumbs .swiper-slide {\n    width: 20%;\n    opacity: 0.4; }\n  #product-ind .gallery-thumbs .swiper-slide img {\n      width: 85%;\n      padding: 5px;\n      border: 1px solid #efefef; }\n  #product-ind .gallery-thumbs .swiper-slide-active {\n    opacity: 1; }\n  #product-ind .button-next,\n  #product-ind .button-prev {\n    position: absolute;\n    top: 25%;\n    font-size: 30px;\n    color: #ccc;\n    z-index: 9999; }\n  #product-ind .button-next {\n    right: 0; }\n  #product-ind .button-prev {\n    left: 0; }\n  #product-ind .breadcrumb {\n    padding: 5px 0px;\n    background: transparent;\n    margin: 0px 50px; }\n  #product-ind .breadcrumb li a {\n      font-size: 15px;\n      color: #ffffff;\n      text-transform: capitalize; }\n  @media only screen and (max-width: 990px) {\n    #product-ind {\n      height: auto; }\n      #product-ind .breadcrumb li a {\n        color: #ffffff; } }\n  @media only screen and (max-width: 1024px) {\n    #product-ind .breadcrumb li a {\n      color: #ffffff; } }\n  @media only screen and (max-width: 480px) and (min-width: 320px) {\n    #product-ind .breadcrumb li a {\n      color: #ffffff; } }\n  .rViewSlider {\n  margin: 50px 0px; }\n  .rViewSlider .rbutton-next,\n  .rViewSlider .rbutton-prev {\n    position: absolute;\n    top: 50%;\n    z-index: 999;\n    font-size: 40px;\n    outline: none;\n    -webkit-box-shadow: none;\n            box-shadow: none;\n    cursor: pointer; }\n  .rViewSlider .rbutton-next {\n    right: 0;\n    right: -15px; }\n  .rViewSlider .rbutton-prev {\n    left: 0;\n    left: -15px; }\n  .rViewSlider h2 {\n    font-size: 36px;\n    font-family: 'us';\n    text-align: center; }\n  @media only screen and (max-width: 768px) {\n  #product-ind {\n    padding: 30px 0px; }\n    #product-ind .product-card .card {\n      margin: 0px; } }\n  @media only screen and (max-width: 500px) {\n  #product-ind {\n    height: auto; }\n    #product-ind .product-card .card .card-body {\n      padding: 0px 15px; }\n      #product-ind .product-card .card .card-body .product-details {\n        padding: 15px 0px; }\n        #product-ind .product-card .card .card-body .product-details h3 {\n          font-size: 15px; }\n        #product-ind .product-card .card .card-body .product-details p {\n          font-size: 14px; }\n    #product-ind ol.breadcrumb {\n      margin: 0px;\n      font-size: 12px; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/collections/product/product.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global_service__ = __webpack_require__("../../../../../src/app/global.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__ = __webpack_require__("../../../../angular2-useful-swiper/lib/swiper.module.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_share_core__ = __webpack_require__("../../../../@ngx-share/core/esm5/ngx-share-core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ProductComponent = /** @class */ (function () {
    function ProductComponent(share, _location, route, router, http, global, spinnerService, ngZone) {
        var _this = this;
        this.share = share;
        this._location = _location;
        this.route = route;
        this.router = router;
        this.http = http;
        this.global = global;
        this.spinnerService = spinnerService;
        this.ngZone = ngZone;
        this.galleryTopConfig = {
            spaceBetween: 30,
            slidesPerView: 1
        };
        this.galleryThumbsConfig = {
            nextButton: '.button-next',
            prevButton: '.button-prev',
            spaceBetween: 10,
            centeredSlides: true,
            slidesPerView: 'auto',
            touchRatio: 0.2,
            slideToClickedSlide: true
        };
        this.relatedView = {
            nextButton: '.rbutton-next',
            prevButton: '.rbutton-prev',
            spaceBetween: 30,
            slidesPerView: 4,
            breakpoints: {
                480: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                960: {
                    slidesPerView: 3,
                    spaceBetween: 10
                }
            }
        };
        this.prod = this.route.params.subscribe(function (params) { return _this.urlParams = params; });
        this.prodUrl = this.global.baseURL + '/collections' + '/' + this.urlParams.collectionName + '/' + this.urlParams.productId;
        console.log(this.urlParams.collectionName);
        console.log(this.urlParams.productId);
    }
    ProductComponent.prototype.getData = function () {
        return this.http.get(this.prodUrl);
    };
    ProductComponent.prototype.getProdData = function () {
        var _this = this;
        this.getData().subscribe(function (data) {
            console.log(data);
            _this.productDetail = data;
            _this.pName = _this.productDetail.prodData.name;
            _this.pAttributes = _this.productDetail.prodData.attributes;
            _this.pImg1 = _this.productDetail.prodData.image1;
            _this.pImg2 = _this.productDetail.prodData.image2;
            _this.pImg3 = _this.productDetail.prodData.image3;
            _this.pImg4 = _this.productDetail.prodData.image4;
            _this.pImgH1 = _this.productDetail.prodData.hover1;
            _this.pImgH2 = _this.productDetail.prodData.hover2;
            _this.pImgH3 = _this.productDetail.prodData.hover3;
            _this.pImgH4 = _this.productDetail.prodData.hover4;
            _this.otherProd = _this.productDetail.otherProds;
            _this.spinnerService.hide();
            console.log(_this.pImg3);
        });
    };
    ProductComponent.prototype.goBack = function () {
        this._location.back();
    };
    ProductComponent.prototype.viewProd = function (prodId) {
        window.location.reload();
        this.router.navigate(['/collections', this.urlParams.collectionName, prodId], {
            relativeTo: this.route
        });
    };
    ProductComponent.prototype.imageZoom = function (imgID) {
        if (!($('#resultZoomDiv').hasClass('active'))) {
            $('#resultZoomDiv').addClass('active');
            $('.product-details').removeClass('active');
        }
        var img, lens, result, cx, cy;
        img = document.getElementById(imgID);
        result = document.getElementById('resultZoomDiv');
        /*create lens:*/
        lens = document.createElement("DIV");
        lens.setAttribute("class", "img-zoom-lens");
        /*insert lens:*/
        if ($(img.parentElement).find('.img-zoom-lens').length == 0)
            img.parentElement.insertBefore(lens, img);
        /*calculate the ratio between result DIV and lens:*/
        cx = result.offsetWidth / lens.offsetWidth;
        cy = result.offsetHeight / lens.offsetHeight;
        /*set background properties for the result DIV:*/
        result.style.backgroundImage = "url('" + img.src + "')";
        result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
        /*execute a function when someone moves the cursor over the image, or the lens:*/
        lens.addEventListener("mousemove", moveLens);
        img.addEventListener("mousemove", moveLens);
        /*and also for touch screens:*/
        lens.addEventListener("touchmove", moveLens);
        img.addEventListener("touchmove", moveLens);
        function moveLens(e) {
            var pos, x, y;
            /*prevent any other actions that may occur when moving over the image:*/
            e.preventDefault();
            /*get the cursor's x and y positions:*/
            pos = getCursorPos(e);
            /*calculate the position of the lens:*/
            // console.log(pos);
            // console.log(lens.offsetHeight);
            // console.log(lens.offsetWidth);
            x = pos.x - (lens.offsetWidth / 2);
            y = pos.y - (lens.offsetHeight / 2);
            /*prevent the lens from being positioned outside the image:*/
            if (x > img.width - lens.offsetWidth) {
                x = img.width - lens.offsetWidth;
            }
            if (x < 0) {
                x = 0;
            }
            if (y > img.height - lens.offsetHeight) {
                y = img.height - lens.offsetHeight;
            }
            if (y < 0) {
                y = 0;
            }
            /*set the position of the lens:*/
            lens.style.left = x + "px";
            lens.style.top = y + "px";
            /*display what the lens "sees":*/
            result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
        }
        function getCursorPos(e) {
            var a, x = 0, y = 0;
            e = e || window.event;
            /*get the x and y positions of the image:*/
            a = img.getBoundingClientRect();
            /*calculate the cursor's x and y coordinates, relative to the image:*/
            x = e.pageX - a.left;
            y = e.pageY - a.top;
            /*consider any page scrolling:*/
            x = x - window.pageXOffset;
            y = y - window.pageYOffset;
            return {
                x: x,
                y: y
            };
        }
    };
    ProductComponent.prototype.resetZoom = function () {
        $('#resultZoomDiv').removeClass('active');
        $('.product-details').addClass('active');
        console.log('hide zoom');
    };
    ProductComponent.prototype.ngOnInit = function () {
        this.getProdData();
        $(document).ready(function () {
            // pSlider();
            fadeInUp();
            // rViewSlider();
        });
    };
    ProductComponent.prototype.ngAfterViewInit = function () {
        this.galleryTop.swiper.params.control = this.galleryThumbs.swiper;
        this.galleryThumbs.swiper.params.control = this.galleryTop.swiper;
        // if(this.elevatezoomBig){
        //   $(this.elevatezoomBig.nativeElement).elevateZoom({
        //       borderSize: 1,
        //       lensFadeIn: 200,
        //       cursor: 'crosshair',
        //       zoomWindowFadeIn: 200,
        //       loadingIcon: true,
        //       zoomWindowOffety: -50,
        //       zoomWindowOffetx: 50,
        //       zoomWindowHeight: 530,
        //       responsive: true
        //   });
        // }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('galleryTop'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__["SwiperComponent"])
    ], ProductComponent.prototype, "galleryTop", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('galleryThumbs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_angular2_useful_swiper__["SwiperComponent"])
    ], ProductComponent.prototype, "galleryThumbs", void 0);
    ProductComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-product',
            template: __webpack_require__("../../../../../src/app/collections/product/product.component.html"),
            styles: [__webpack_require__("../../../../../src/app/collections/product/product.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__ngx_share_core__["a" /* ShareButtons */], __WEBPACK_IMPORTED_MODULE_2__angular_common__["Location"], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_5__global_service__["a" /* GlobalService */], __WEBPACK_IMPORTED_MODULE_4_ng4_loading_spinner__["Ng4LoadingSpinnerService"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
    ], ProductComponent);
    return ProductComponent;
}());



/***/ }),

/***/ "../../../../../src/app/collections/view-collection.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewCollectionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__global_service__ = __webpack_require__("../../../../../src/app/global.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ViewCollectionService = /** @class */ (function () {
    function ViewCollectionService(route, router, http, global, spinnerService) {
        this.route = route;
        this.router = router;
        this.http = http;
        this.global = global;
        this.spinnerService = spinnerService;
        this.collectionUrl = global.baseURL + '/collections/';
        this.accessUrl = global.baseURL;
    }
    ViewCollectionService.prototype.getData = function (collectionName) {
        return this.http.get(this.collectionUrl + '/' + collectionName);
    };
    ViewCollectionService.prototype.getHomeData = function (col) {
        var _this = this;
        this.getData(col).subscribe(function (data) {
            _this.collection = data;
            _this.cBanner = _this.collection.categoryData.banimage;
            _this.cMobBanner = _this.collection.categoryData.mbanimage;
            _this.cName = _this.collection.categoryData.slug;
            _this.productList = _this.collection.products;
            _this.categories = _this.collection.categories;
            _this.spinnerService.hide();
        });
    };
    ViewCollectionService.prototype.viewCollection = function (collectionName) {
        this.name = collectionName;
        this.getHomeData(this.name);
        this.router.navigate(['/collections', this.name]);
    };
    ViewCollectionService.prototype.getAccess = function (cattName, accessName) {
        return this.http.get(this.accessUrl + '/' + cattName + '/' + accessName);
    };
    ViewCollectionService.prototype.getAccessData = function (catName, aes) {
        var _this = this;
        this.getAccess(catName, aes).subscribe(function (data) {
            _this.collection = data;
            _this.cBanner = _this.collection.categoryData.banimage;
            _this.cMobBanner = _this.collection.categoryData.mbanimage;
            _this.cName = _this.collection.categoryData.slug;
            _this.productList = _this.collection.products;
            _this.categories = _this.collection.categories;
            _this.spinnerService.hide();
            // console.log(this);
        });
    };
    ViewCollectionService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4__global_service__["a" /* GlobalService */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], ViewCollectionService);
    return ViewCollectionService;
}());



/***/ }),

/***/ "../../../../../src/app/contactus/contactus.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid\" id=\"contactus\">\n  <div class=\"container banner\">\n    <div class=\"row align-items-center\">\n      <div class=\"col-12 col-lg-6 col-md-8 fade-in-up\">\n        <p>{{contactPara}}</p>\n        <div>\n          <h1 class=\"heading\">STORES & SERVICES</h1>\n          <form class=\"form-inline\">\n            <div class=\"btn-group\">\n              <!-- <button class=\"btn dropdown-toggle\" data-toggle=\"dropdown\">Store & Service <span class=\"caret\"></span></button> -->\n              <label for=\"storelist\" class=\"selecticon\">\n                <select id=\"storelist\" class=\"form-control\" name=\"cat\" [(ngModel)]=\"selectCat\">\n                  <option class=\"dropdown-item\" value=\"storenservice\" selected>Store & Service</option>\n                  <option class=\"dropdown-item\">store</option>\n                  <option class=\"dropdown-item\">service</option>\n                </select>\n              </label>\n              \n            </div>\n            <div class=\"btn-group\">\n              <!-- <button class=\"btn dropdown-toggle\" data-toggle=\"dropdown\">Country <span class=\"caret\"></span></button> -->\n              <label for=\"countryName\" class=\"selecticon\">\n                <select id=\"countryName\" class=\"form-control\" name=\"ctr\" [(ngModel)]=\"selectedCountry\">\n                  <option class=\"dropdown-item\" *ngFor=\"let countryName of countries\" [ngValue]=\"countryName.country\">{{countryName.country}}</option>\n                </select>\n              </label>\n            </div>\n            <div class=\"\">\n              <button class=\"btn theme-btn-red\" (click)=\"filterData()\">search</button>\n            </div>\n          </form>\n        </div>\n      </div>\n      <div class=\"col-12 col-lg-6 col-md-4 fade-in-up\">\n        <img src=\"assets/images/contact-banner-img.png\" class=\"img-fluid mt-3\" alt=\"\">\n      </div>\n      <div class=\"col-12 index-1\">\n        <div class=\"row\">\n          <div class=\"col-12 col-lg-3 col-md-4 address fade-in-up\" *ngFor=\"let store of stores\">\n            <h4>{{store.name}}</h4>\n            <div class=\"address-details\">\n              <p *ngIf=\"store.addr!=''\">{{store.addr}}</p>\n              <p *ngIf=\"store.tel!=''\">Tel : {{store.tel}}</p>\n              <p *ngIf=\"store.fax!=''\">Fax : {{store.fax}}</p>\n              <p *ngIf=\"store.email!=''\">Email : {{store.email}}</p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"row contact-form\" id=\"contactForm\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-12 col-lg-4 col-md-6 fade-in-up\">\n          <h4>WE’D LOVE TO HEAR</h4>\n          <h4>FROM YOU.</h4>\n          <!-- <p>Just ask us, we are always happy to answer.</p> -->\n          <div class=\"\">\n            <div class=\"social-icon mb-5\">\n              <div class=\"d-inline-block\"><a href=\"{{global.fbLink}}\"><img src=\"assets/images/fb.svg\" alt=\"\"></a></div>\n              <div class=\"d-inline-block\"><a href=\"{{global.twLink}}\"><img src=\"assets/images/twitter.svg\" alt=\"\"></a></div>\n              <div class=\"d-inline-block\"><a href=\"{{global.ytLink}}\"><img src=\"assets/images/youtube.svg\" alt=\"\"></a></div>\n              <div class=\"d-inline-block\"><a href=\"{{global.inLink}}\"><img src=\"assets/images/insta.svg\" alt=\"\"></a></div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-12 col-lg-8 col-md-6 fade-in-up\">\n          <form class=\"\" #contactus=\"ngForm\" (ngSubmit)=\"onSubmit(contactus)\">\n            <div class=\"row\">\n              <div class=\"col-12 col-md-6\">\n                <span class=\"error\" *ngIf=\"firstName.touched && !firstName.valid \">Please enter the Valid Name</span>\n                <input type=\"text\" tabindex=\"1\" autofocus=true name=\"firstName\" class=\"form-control\" placeholder=\"First Name\" ngModel #firstName=\"ngModel\" required minlength=\"2\" />\n              </div>\n              <div class=\"col-12 col-md-6\">\n                <input type=\"text\" tabindex=\"2\" name=\"lastName\" class=\"form-control\" placeholder=\"Last Name\" ngModel #lastName=\"ngModel\" required/>\n              </div>\n              <div class=\"col-12 col-md-6\">\n                <input type=\"text\" tabindex=\"3\" name=\"company\" class=\"form-control\" placeholder=\"Company\" ngModel #company=\"ngModel\" required/>\n              </div>\n              <div class=\"col-12 col-md-6\">\n                <span class=\"error\" *ngIf=\"email.touched && !email.valid \">Please enter the Valid Email</span>\n                <input type=\"email\" tabindex=\"4\" name=\"email\" class=\"form-control\" placeholder=\"Email\" ngModel #email=\"ngModel\" required pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$\" />\n              </div>\n              <div class=\"col-12 col-md-6\">\n                <input type=\"text\" tabindex=\"5\" name=\"city\" class=\"form-control\" placeholder=\"City\" ngModel #city=\"ngModel\" required/>\n              </div>\n              <div class=\"col-12 col-md-6\">\n                <span class=\"error\" *ngIf=\"number.touched && !number.valid \">Please enter Mobile Number</span>\n                <input type=\"tel\" tabindex=\"6\" name=\"number\" class=\"form-control\" placeholder=\"Mobile No.\" ngModel #number=\"ngModel\" required/>\n              </div>\n              <div class=\"col-12 col-md-6\">\n                <input type=\"text\" tabindex=\"7\" name=\"country\" class=\"form-control\" placeholder=\"Country\" ngModel #country=\"ngModel\" required/>\n              </div>\n              <div class=\"col-12 col-md-6\">\n                <span class=\"error\" *ngIf=\"enquiries.touched && !enquiries.valid \">Please select</span>\n                <label for=\"selectable\" class=\"selecticon\">\n                  <select id=\"selectable\" tabindex=\"8\" class=\"form-control\" ngModel #enquiries=\"ngModel\" name=\"enquiries\" required>\n                    <option value=\"\">Enquiries</option>\n                    <option value=\"Watch Repair\">Watch Repair</option>\n                    <option value=\"General Feedback\">General Feedback</option>\n                    <option value=\"Trade Enquiries\">Trade Enquiries</option>\n                    <option value=\"Corporate Orders\">Corporate Orders</option>\n                    <option value=\"Warranty\">Warranty</option>\n                    <option value=\"Careers\">Careers</option>\n                    <option value=\"Others\">Others</option>\n                  </select>\n                </label>\n              </div>\n              <div class=\"col-12\">\n                <textarea class=\"form-control\" tabindex=\"9\" name=\"message\" rows=\"3\" placeholder=\"Message\" ngModel #message=\"ngModel\"></textarea>\n                {{msg}}\n              </div>\n              <div class=\"col-12\">\n                <input type=\"submit\" name=\"\" value=\"Submit\" class=\"btn theme-btn-red float-right\">\n              </div>\n            </div>\n          </form>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "../../../../../src/app/contactus/contactus.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#contactus {\n  background-position: top; }\n  #contactus .banner {\n    padding-top: 150px;\n    padding-bottom: 80px; }\n  #contactus .banner p {\n      color: #000;\n      font-weight: 700;\n      line-height: 1.8;\n      letter-spacing: 0px; }\n  #contactus .banner .form-inline .btn-group {\n      width: 190px;\n      padding: 5px; }\n  #contactus .banner .form-inline .btn-group label {\n        width: 100%; }\n  #contactus .banner .form-inline .btn-group label #storelist {\n          text-transform: capitalize; }\n  #contactus .banner .form-inline .btn-group label .form-control {\n          width: 100%; }\n  #contactus .banner .form-inline .theme-btn-red {\n      padding: 6px 30px; }\n  #contactus .banner .form-inline .selecticon {\n      position: relative;\n      display: block;\n      overflow: hidden;\n      cursor: pointer; }\n  #contactus .banner .form-inline .selecticon:after {\n        content: ' ';\n        position: absolute;\n        right: 7px;\n        top: 7px;\n        width: 15px;\n        height: 60%;\n        display: block;\n        background: transparent url(\"data:image/svg+xml,%3C%3Fxml version%3D%221.0%22 encoding%3D%22UTF-8%22%3F%3E%3Csvg width%3D%2217px%22 height%3D%2210px%22 viewBox%3D%220 0 17 10%22 version%3D%221.1%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22 xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E    %3C!-- Generator%3A Sketch 51.3 (57544) - http%3A%2F%2Fwww.bohemiancoding.com%2Fsketch --%3E    %3Ctitle%3EPath 3%3C%2Ftitle%3E    %3Cdesc%3ECreated with Sketch.%3C%2Fdesc%3E    %3Cdefs%3E%3C%2Fdefs%3E    %3Cg id%3D%22Page-1%22 stroke%3D%22none%22 stroke-width%3D%221%22 fill%3D%22none%22 fill-rule%3D%22evenodd%22 stroke-linecap%3D%22round%22%3E        %3Cpolyline id%3D%22Path-3%22 stroke%3D%22%236B747D%22 stroke-width%3D%222%22 fill-rule%3D%22nonzero%22 points%3D%221 1.09027351 8.156151 8.24642451 15.312302 1.09027351%22%3E%3C%2Fpolyline%3E    %3C%2Fg%3E%3C%2Fsvg%3E\") no-repeat center center;\n        pointer-events: none;\n        opacity: 0.3; }\n  #contactus .index-1 {\n    z-index: -1; }\n  #contactus .dropdown-toggle {\n    min-width: 160px;\n    text-align: left;\n    border-radius: 0px;\n    background: transparent;\n    border: 1px solid #000;\n    color: #000;\n    margin-right: 10px; }\n  #contactus .dropdown-menu {\n    border-radius: 0px;\n    background: #fff; }\n  #contactus .dropdown-item.active, #contactus .dropdown-item:active {\n    background-color: #ccc; }\n  #contactus .dropdown-toggle::after {\n    position: absolute;\n    right: 10px;\n    top: 50%; }\n  #contactus .heading {\n    color: #000;\n    font-size: 28px;\n    font-family: 'us';\n    margin-top: 40px;\n    margin-bottom: 20px; }\n  #contactus .address {\n    margin-top: 50px; }\n  #contactus .address p {\n      margin-bottom: 0px;\n      font-size: 14px;\n      line-height: 1.5;\n      font-weight: 700; }\n  #contactus .address h4 {\n      color: #000;\n      font-size: 20px;\n      font-family: 'us'; }\n  #contactus .address .address-details {\n      opacity: 0.7; }\n  #contactus .selecticon {\n    position: relative;\n    display: block;\n    overflow: hidden;\n    cursor: pointer; }\n  #contactus .selecticon:after {\n      content: ' ';\n      position: absolute;\n      right: 0;\n      top: 0;\n      width: 15px;\n      height: 60%;\n      display: block;\n      background: transparent url(\"data:image/svg+xml,%3C%3Fxml version%3D%221.0%22 encoding%3D%22UTF-8%22%3F%3E%3Csvg width%3D%2217px%22 height%3D%2210px%22 viewBox%3D%220 0 17 10%22 version%3D%221.1%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22 xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E    %3C!-- Generator%3A Sketch 51.3 (57544) - http%3A%2F%2Fwww.bohemiancoding.com%2Fsketch --%3E    %3Ctitle%3EPath 3%3C%2Ftitle%3E    %3Cdesc%3ECreated with Sketch.%3C%2Fdesc%3E    %3Cdefs%3E%3C%2Fdefs%3E    %3Cg id%3D%22Page-1%22 stroke%3D%22none%22 stroke-width%3D%221%22 fill%3D%22none%22 fill-rule%3D%22evenodd%22 stroke-linecap%3D%22round%22%3E        %3Cpolyline id%3D%22Path-3%22 stroke%3D%22%236B747D%22 stroke-width%3D%222%22 fill-rule%3D%22nonzero%22 points%3D%221 1.09027351 8.156151 8.24642451 15.312302 1.09027351%22%3E%3C%2Fpolyline%3E    %3C%2Fg%3E%3C%2Fsvg%3E\") no-repeat center center;\n      pointer-events: none;\n      opacity: 0.3; }\n  #contactus select {\n    color: #6b747d !important; }\n  #contactus .contact-form {\n    background: #fff;\n    padding-top: 100px;\n    padding-bottom: 100px; }\n  #contactus .contact-form .form-control {\n      margin-bottom: 30px;\n      border: 0px;\n      border-bottom: 1px solid #ccc;\n      border-radius: 0px;\n      outline: none;\n      -webkit-box-shadow: none;\n              box-shadow: none; }\n  #contactus .contact-form .social-icon {\n      margin-top: 30px; }\n  #contactus .contact-form .social-icon img {\n        width: 30px;\n        height: 30px;\n        margin-right: 10px; }\n  #contactus .contact-form .error {\n      color: red;\n      font-size: 12px;\n      position: absolute;\n      right: 18px; }\n  @media only screen and (max-width: 480px) and (min-width: 320px) {\n    #contactus .form-inline .btn-group {\n      width: 100% !important; }\n    #contactus .form-inline button {\n      text-align: center; } }\n  @media only screen and (max-width: 768px) {\n    #contactus .banner {\n      padding-top: 80px; } }\n  *::-webkit-input-placeholder {\n  color: #5f6165; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/contactus/contactus.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactusComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__global_service__ = __webpack_require__("../../../../../src/app/global.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContactusComponent = /** @class */ (function () {
    function ContactusComponent(http, global) {
        this.http = http;
        this.global = global;
        this.selectCat = 'storenservice';
        this.contactUrl = global.baseURL + "/pages/contactus";
        this.submitContact = global.baseURL + "/enquiry";
        this.base = global.baseURL + '/storenservice';
    }
    ContactusComponent.prototype.getData = function () {
        return this.http.get(this.contactUrl);
    };
    ContactusComponent.prototype.getContactData = function () {
        var _this = this;
        this.getData().subscribe(function (data) {
            _this.contactData = data;
            _this.contactPara = _this.contactData.contData.contPara;
            _this.contactImg = _this.contactData.contData.contImg;
            _this.countries = _this.contactData.countries;
            _this.selectedCountry = _this.countries[0].country;
            _this.stores = _this.contactData.allStores;
        });
    };
    ContactusComponent.prototype.onSubmit = function (subcribe) {
        this.fName = subcribe.value.firstName;
        this.lName = subcribe.value.lastName;
        this.company = subcribe.value.company;
        this.email = subcribe.value.email;
        this.city = subcribe.value.city;
        this.country = subcribe.value.country;
        this.number = subcribe.value.number;
        this.message = subcribe.value.message;
        this.enquirie = subcribe.value.enquiries;
        this.http.post(this.submitContact, {
            first: this.fName,
            last: this.lName,
            company: this.company,
            email: this.email,
            city: this.city,
            country: this.country,
            cont: this.number,
            type: this.enquirie,
            message: this.message
        }).subscribe(function (data) {
            // this.msg = data.response.msg
            $('#thankyouModal').modal('show');
        });
    };
    ContactusComponent.prototype.ngOnInit = function () {
        remFullpage();
        this.getContactData();
        this.contactForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            name: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
                firstName: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["e" /* Validators */].required),
                lastName: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["e" /* Validators */].required),
            })
        });
        $(document).ready(function () {
            fadeInUp();
            $(".dropdown-menu li").click(function () {
                var selText = $(this).text();
                $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
            });
            $.fn.fullpage.destroy('all');
        });
    };
    ContactusComponent.prototype.getfilterData = function () {
        return this.http.get(this.base + '/' + this.selectCat + '/' + this.selectedCountry);
    };
    ContactusComponent.prototype.filterData = function () {
        var _this = this;
        this.getfilterData().subscribe(function (data) {
            _this.filterStoreData = data;
            _this.stores = _this.filterStoreData.allStores;
        });
    };
    ContactusComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-contactus',
            template: __webpack_require__("../../../../../src/app/contactus/contactus.component.html"),
            styles: [__webpack_require__("../../../../../src/app/contactus/contactus.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__global_service__["a" /* GlobalService */]])
    ], ContactusComponent);
    return ContactusComponent;
}());



/***/ }),

/***/ "../../../../../src/app/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer class=\"fp-noscroll container-fluid\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-lg-3 col-md-6 col-sm-6 col-12\">\n        <a class=\"footer-logo\" href=\"#\"><img src=\"../assets/images/Blade-Logo.svg\" alt=\"\"></a>\n        <div class=\"mt-40\">\n          <ul>\n            <li *ngFor=\"let footerLink of footerLinks\"><a href=\"{{footerLink.link}}\">{{footerLink.menu}}</a></li>\n          </ul>\n        </div>\n      </div>\n      <div class=\"col-lg-3 col-md-6 col-sm-6 col-12 plr0\">\n        <div class=\"row footer-heading\">\n          <div class=\"col-12 plr0\">\n            <h5>COLLECTION\n              <hr />\n            </h5>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col\">\n            <ul>\n              <li *ngFor=\"let collectionOne of collectionListOne\">\n                <a (click)=\"routeCollection(collectionOne.slug)\">{{collectionOne.name}}</a>\n              </li>\n            </ul>\n          </div>\n          <div class=\"col\">\n            <ul>\n              <li *ngFor=\"let collectionTwo of collectionListTwo\">\n                <a (click)=\"routeCollection(collectionTwo.slug)\">{{collectionTwo.name}}</a>\n              </li>\n            </ul>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-lg-3 col-md-6 col-sm-6 col-12 plr0\">\n        <div class=\"row footer-heading ml-15\">\n          <div class=\"col-12 plr0\">\n            <h5>CONTACT US\n              <hr />\n            </h5>\n          </div>\n        </div>\n        <div class=\"row ml-15\">\n          <div class=\"col\">\n            <p *ngIf=\"address!=''\" class=\"addr\">{{address}}</p>\n            <p *ngIf=\"call!=''\"><a href=\"tel:{{call}}\">Tel: {{call}}</a></p>\n            <p *ngIf=\"fax!=''\">Fax: {{fax}}</p>\n            <P *ngIf=\"mail!=''\"><a href=\"mailto:{{mail}}\">{{mail}}</a></P>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-lg-3 col-md-6 col-sm-6 col-12 plr0\">\n        <div class=\"row footer-heading\">\n          <div class=\"col-12 plr0\">\n            <h5>STAY CONNECTED\n              <hr />\n            </h5>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col\">\n            <p>Join our newsletter to stay on top of current information and events.</p>\n            <form #subcribe=\"ngForm\" (ngSubmit)=\"onSubmit(subcribe)\" class=\"form-inline mt-20\">\n              <div class=\"input-group mb-2 mr-sm-2\">\n                <input type=\"email\" class=\"form-control\" name=\"subcribeEmail\" placeholder=\"Email*\" ngModel\n                #subcribeEmail=\"ngModel\" required>\n                <div class=\"input-group-prepend\">\n                  <div class=\"\"><input type=\"submit\" value=\"SUBSCRIBE\" class=\"btn\" size=\"65\"></div>\n                </div>\n              </div>\n              <p class=\"m-0\">{{msg}}</p>\n            </form>\n            <div class=\"social-icon\">\n              <a target=\"_blank\" href=\"{{global.fbLink}}\" class=\"inline-block\">\n                <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 6.07 13\">\n                  <defs>\n                    <style>\n                      .cls-1 {\n                        fill: #dcb305;\n                      }\n                    </style>\n                  </defs>\n                  <title>Facebook</title>\n                  <g id=\"Layer_1\" data-name=\"Layer 1\">\n                    <g id=\"Layer_1-1\" data-name=\"Layer 1\">\n                      <path class=\"cls-1\" d=\"M1.31,2.52V4.31H0V6.5H1.31V13H4V6.5h1.8s.17-1,.25-2.2H4V2.8a.63.63,0,0,1,.58-.52H6.07V0h-2C1.24,0,1.31,2.19,1.31,2.52Z\"></path>\n                    </g>\n                  </g>\n                </svg>\n              </a>\n              <a target=\"_blank\" href=\"{{global.twLink}}\" class=\"inline-block\">\n                <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 15 12\">\n                  <defs>\n                    <style>\n                      .cls-1 {\n                        fill: #dcb305;\n                      }\n                    </style>\n                  </defs>\n                  <title>twitter</title>\n                  <g id=\"Layer_2\" data-name=\"Layer 2\">\n                    <g id=\"Layer_2-1\" data-name=\"Layer 1\">\n                      <path class=\"cls-1\" d=\"M4.72,12a8.62,8.62,0,0,0,8.75-8.62c0-.13,0-.26,0-.39A6.38,6.38,0,0,0,15,1.42a6.36,6.36,0,0,1-1.77.48A3.07,3.07,0,0,0,14.58.22,6.13,6.13,0,0,1,12.63,1,3.12,3.12,0,0,0,8.28.82a3,3,0,0,0-.89,2.9A8.81,8.81,0,0,1,1,.55,3,3,0,0,0,2,4.6,3.25,3.25,0,0,1,.6,4.22v0a3,3,0,0,0,2.47,3,3.11,3.11,0,0,1-1.39,0,3.07,3.07,0,0,0,2.87,2.1A6.25,6.25,0,0,1,0,10.64,8.85,8.85,0,0,0,4.72,12\"></path>\n                    </g>\n                  </g>\n                </svg></a>\n                <a target=\"_blank\" href=\"{{global.ytLink}}\" class=\"inline-block\">\n                  <svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n                  x=\"0px\" y=\"0px\" viewBox=\"0 0 25 15\" style=\"enable-background:new 0 0 25 15;\" xml:space=\"preserve\">\n                  <style type=\"text/css\">\n                    .st0 {\n                      fill: #FFFFFF;\n                    }\n                  </style>\n                  <g>\n                    <path class=\"st0\" d=\"M22.9,2.4c-0.3-1.1-1.2-2-2.3-2.1C17.9,0,15.2,0,12.5,0C9.8,0,7.1,0,4.4,0.3c-1.1,0.1-2.1,1-2.3,2.1\n                    C1.7,4,1.7,5.8,1.7,7.5s0,3.5,0.4,5.1c0.3,1.1,1.2,2,2.3,2.1C7.1,15,9.8,15,12.5,15c2.7,0,5.4,0,8.1-0.3c1.1-0.1,2.1-1,2.3-2.1\n                    c0.4-1.6,0.4-3.4,0.4-5.1C23.3,5.8,23.3,4,22.9,2.4z M9.7,10.8c0-2.4,0-4.7,0-7.1c2.3,1.2,4.5,2.4,6.8,3.6\n                    C14.3,8.5,12,9.7,9.7,10.8z\" />\n                  </g>\n                </svg>\n              </a>\n              <a target=\"_blank\" href=\"{{global.inLink}}\" class=\"inline-block\">\n                <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_4-1\"\n                x=\"0px\" y=\"0px\" width=\"512px\" height=\"512px\" viewBox=\"0 0 169.063 169.063\" style=\"enable-background:new 0 0 169.063 169.063;\"\n                xml:space=\"preserve\" class=\"\">\n                <g>\n                  <g>\n                    <path d=\"M122.406,0H46.654C20.929,0,0,20.93,0,46.655v75.752c0,25.726,20.929,46.655,46.654,46.655h75.752   c25.727,0,46.656-20.93,46.656-46.655V46.655C169.063,20.93,148.133,0,122.406,0z M154.063,122.407   c0,17.455-14.201,31.655-31.656,31.655H46.654C29.2,154.063,15,139.862,15,122.407V46.655C15,29.201,29.2,15,46.654,15h75.752   c17.455,0,31.656,14.201,31.656,31.655V122.407z\"\n                    data-original=\"#000000\" class=\"\" fill=\"#dcb305\"></path>\n                    <path d=\"M84.531,40.97c-24.021,0-43.563,19.542-43.563,43.563c0,24.02,19.542,43.561,43.563,43.561s43.563-19.541,43.563-43.561   C128.094,60.512,108.552,40.97,84.531,40.97z M84.531,113.093c-15.749,0-28.563-12.812-28.563-28.561   c0-15.75,12.813-28.563,28.563-28.563s28.563,12.813,28.563,28.563C113.094,100.281,100.28,113.093,84.531,113.093z\"\n                    data-original=\"#000000\" class=\"\" fill=\"#dcb305\"></path>\n                    <path d=\"M129.921,28.251c-2.89,0-5.729,1.17-7.77,3.22c-2.051,2.04-3.23,4.88-3.23,7.78c0,2.891,1.18,5.73,3.23,7.78   c2.04,2.04,4.88,3.22,7.77,3.22c2.9,0,5.73-1.18,7.78-3.22c2.05-2.05,3.22-4.89,3.22-7.78c0-2.9-1.17-5.74-3.22-7.78   C135.661,29.421,132.821,28.251,129.921,28.251z\"\n                    data-original=\"#000000\" class=\"\" fill=\"#dcb305\"></path>\n                  </g>\n                </g>\n              </svg>\n            </a>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n</footer>\n<div class=\"copyright\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-6 col-sm-8 col-12\">\n        <p>Terms Of Use & Privacy Policy Al Wafaa | Copyright © 2013 Blade Watches</p>\n      </div>\n      <div class=\"col-md-6 col-sm-4 col-12\">\n        <p class=\"float-right\">Powered By <a href=\"http://www.redbaton.in/\" target=\"_blank\">Red Baton</a></p>\n      </div>\n    </div>\n  </div>\n</div>\n<!-- Modal -->\n<div class=\"modal fade bd-example-modal-lg custom-modal\" id=\"leadModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\naria-hidden=\"true\">\n<div class=\"modal-dialog modal-lg modal-dialog-centered\" role=\"document\">\n  <div class=\"modal-content\">\n    <div class=\"\">\n      <button type=\"button\" class=\"close close-btn\" data-dismiss=\"modal\" aria-label=\"Close\">\n        <span aria-hidden=\"true\"><i class=\"ion-ios-close-empty\"></i></span>\n      </button>\n    </div>\n    <div class=\"modal-body custom-body\">\n      <div class=\"container-fluid\">\n        <div class=\"row align-items-center \">\n          <div class=\"col-12 col-md-6 col-lg-6 p-4\">\n            <h2>{{popupHead}}</h2>\n            <p>{{popupDes}}</p>\n            <form #popup=\"ngForm\" (ngSubmit)=\"popupSubmit(popup)\">\n              <div class=\"row modal-form \">\n                <div class=\"col-12\">\n                  <input class=\"form-control\" name=\"name\" tabindex=\"1\" placeholder=\"Name\" type=\"text\" required ngModel #name=\"ngModel\" (keyup.Tab)=\"keytab($event)\" minlength=\"3\">\n                  <input class=\"form-control\" name=\"email\" tabindex=\"2\" placeholder=\"Email\" type=\"email\" required ngModel #email=\"ngModel\" (keyup.Tab)=\"keytab($event)\">\n\n                  <input class=\"form-control\" name=\"phone\" tabindex=\"3\" placeholder=\"Mobile No.\" type=\"tel\" ngModel #phone=\"ngModel\" (keyup.Tab)=\"keytab($event)\" minlength=\"10\" maxlength=\"10\">\n                  <p class=\"text-danger m-0\">{{errMsg}}</p>\n                  <input class=\"btn theme-btn-red mb-4\" tabindex=\"4\" name=\"\" type=\"submit\" value=\"Submit\">\n                </div>\n              </div>\n            </form>\n          </div>\n          <div class=\"col-12 col-md-6 col-lg-6 p-0\" [ngStyle]=\"bgstyle()\">\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n</div>\n<!-- thankyou modal -->\n<div class=\"modal fade custom-modal-2\" id=\"thankyouModal\" role=\"dialog\" aria-labelledby=\"\">\n  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"exampleModalCenterTitle\">Blade Watches</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <p>Thank you for subscribing!</p>\n      </div>\n    </div>\n  </div>\n</div>\n<!-- thankyou modal end-->\n"

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.scss":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("../../../../css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "footer {\n  background: #000 url(" + escape(__webpack_require__("../../../../../src/assets/images/footer-background.png")) + ") center no-repeat;\n  background-size: cover;\n  padding: 30px 15px;\n  font-size: 14px;\n  color: #fff;\n  font-family: 'gotham', sans-serif; }\n  footer .social-icon {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n  footer .social-icon a svg {\n      width: 25px;\n      height: 15px; }\n  footer .social-icon a svg .cls-1 {\n        fill: #ffffff; }\n  footer .social-icon a #Layer_4-1 g path {\n      fill: #ffffff; }\n  footer .social-icon a i {\n      font-size: 18px;\n      vertical-align: sub; }\n  footer .footer-logo img {\n    width: 65%; }\n  footer .col {\n    padding: 0px; }\n  footer .footer-heading hr {\n    border-top: 2px solid white;\n    width: 40px;\n    margin: 12px 0;\n    margin-right: auto; }\n  footer .footer-heading h5 {\n    margin: 0px;\n    color: #fff;\n    font-size: 16px;\n    font-family: 'us';\n    letter-spacing: 1px; }\n  footer ul {\n    list-style-type: none;\n    padding: 0px;\n    margin: 0px; }\n  footer ul li {\n      margin-bottom: 5px; }\n  footer ul li:last-child {\n        margin-bottom: 0px; }\n  footer a {\n    color: #fff;\n    text-decoration: none;\n    font-size: 14px;\n    font-weight: 700;\n    cursor: pointer; }\n  footer p {\n    margin-bottom: 15px;\n    width: 85%; }\n  footer .form-inline .input-group {\n    width: 90%; }\n  footer .form-control {\n    background: transparent;\n    color: #fff;\n    border-color: #fff;\n    border-radius: 0px;\n    font-size: 14px; }\n  footer .form-control:focus {\n      outline: none;\n      -webkit-box-shadow: none;\n              box-shadow: none; }\n  footer .form-control::-webkit-input-placeholder {\n      color: #fff; }\n  footer .form-control:-ms-input-placeholder {\n      color: #fff; }\n  footer .form-control::-ms-input-placeholder {\n      color: #fff; }\n  footer .form-control::placeholder {\n      color: #fff; }\n  footer .btn {\n    border-radius: 0px;\n    font-size: 16px;\n    font-family: 'us'; }\n  @media only screen and (max-width: 990px) {\n    footer .footer-logo img {\n      width: 30%; }\n    footer .mt-40 {\n      margin-top: 20px; }\n    footer .col-12 {\n      margin-bottom: 20px; }\n    footer .col {\n      padding: 0px 15px; }\n    footer .ml-15 {\n      margin-left: -15px;\n      margin-right: -15px; } }\n  footer .social-icon a {\n    color: #fff;\n    margin-right: 15px; }\n  @media only screen and (max-width: 600px) {\n    footer p,\n    footer a {\n      font-size: 10px; } }\n  @media only screen and (max-width: 480px) and (min-width: 320px) {\n  .plr0 {\n    padding-left: 15px !important;\n    padding-right: 15px !important; }\n  footer a,\n  footer p {\n    font-size: 15px; } }\n  @media only screen and (max-width: 768px) {\n  .plr0 {\n    padding-left: 15px !important;\n    padding-right: 15px !important; } }\n  @media only screen and (min-width: 1025px) and (max-width: 1366px) {\n  footer form .input-group-prepend .btn {\n    background-color: #fff;\n    color: #000; }\n  footer .ml-15 p.addr {\n    margin-right: 35px; } }\n  .plr0 {\n  padding-left: 0px;\n  padding-right: 0px; }\n  .copyright {\n  background: #000;\n  padding: 10px; }\n  .copyright p {\n    font-size: 12px;\n    color: #fff;\n    margin: 0;\n    line-height: 2; }\n  .copyright a {\n    color: #fff; }\n  /* modal css */\n  .custom-modal {\n  z-index: 999999 !important; }\n  .custom-modal .close-btn {\n    position: absolute;\n    right: 0;\n    margin: 15px 10px;\n    z-index: 99999;\n    color: #ffffff;\n    opacity: 1;\n    background: rgba(0, 0, 0, 0.5);\n    border-radius: 50%;\n    width: 30px;\n    height: 30px;\n    line-height: 0; }\n  .custom-modal .modal-content {\n    background-color: #fff;\n    height: auto;\n    background-size: contain;\n    border: none; }\n  .custom-modal .custom-body {\n    padding: 0px; }\n  .custom-modal .custom-body h2 {\n      color: #000;\n      font-size: 2rem;\n      padding: 25px 0px;\n      text-transform: uppercase;\n      letter-spacing: 1px;\n      font-family: 'us';\n      font-weight: 700; }\n  .custom-modal .custom-body p {\n      color: #5f6165;\n      font-family: 'gotham'; }\n  .custom-modal .custom-body .modal-form .form-control {\n      margin-bottom: 30px;\n      border: 0px;\n      background-color: transparent;\n      border-bottom: 1px solid #ccc;\n      border-radius: 0px;\n      outline: none;\n      -webkit-box-shadow: none;\n              box-shadow: none;\n      font-size: 14px; }\n  *::-webkit-input-placeholder {\n  color: #5f6165; }\n  /******  thankyou modal ******/\n  .custom-modal-2 .modal-content {\n  border-radius: 5px; }\n  .custom-modal-2 .modal-content .modal-header {\n    background-color: #AA0202;\n    color: #ffffff; }\n  .custom-modal-2 .modal-content .modal-header .close {\n      color: #ffffff; }\n  .custom-modal-2 .modal-content .modal-body p {\n    padding: 2rem;\n    text-align: center;\n    font-size: 20px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__collections_view_collection_service__ = __webpack_require__("../../../../../src/app/collections/view-collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__header_footer_service__ = __webpack_require__("../../../../../src/app/header-footer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__global_service__ = __webpack_require__("../../../../../src/app/global.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FooterComponent = /** @class */ (function () {
    function FooterComponent(viewcollection, headerfooter, http, global) {
        this.viewcollection = viewcollection;
        this.headerfooter = headerfooter;
        this.http = http;
        this.global = global;
        this.sendEmail = global.baseURL + '/newsletter';
        this.sendPopup = global.baseURL + '/offers';
    }
    FooterComponent.prototype.routeCollection = function (name) {
        this.cName = name;
        this.viewcollection.viewCollection(this.cName);
    };
    FooterComponent.prototype.bgstyle = function () {
        var bg = {
            'background-image': 'url(' + this.popupImg + ')',
            'background-size': 'cover',
            'min-height': '500px',
            'background-position': 'center'
        };
        return bg;
    };
    FooterComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    FooterComponent.prototype.validatePhone = function (phone) {
        var re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        return re.test(String(phone));
    };
    FooterComponent.prototype.keytab = function (event) {
        var element = event.srcElement.nextElementSibling; // get the sibling element
        if (element == null)
            return;
        else
            element.focus(); // focus if not null
    };
    FooterComponent.prototype.onSubmit = function (subcribe) {
        var _this = this;
        // let headers = new HttpHeaders({
        //   'Content-Type': 'application/json'
        //   });
        this.email = subcribe.value.subcribeEmail;
        if (this.validateEmail(this.email)) {
            // code...
            this.http.post(this.sendEmail, {
                email: this.email
            }).subscribe(function (data) {
                // this.msg = data.response.msg;
                // window.alert("Thank you for subscribing!");
                $('#thankyouModal').modal('show');
                _this.msg = "";
            }, function (error) {
                console.log(error);
            });
        }
        else {
            this.msg = "Invalid Email Address";
        }
    };
    FooterComponent.prototype.popupSubmit = function (popup) {
        if (this.validateEmail(popup.value.email) && this.validatePhone(popup.value.phone)) {
            this.popupEmail = popup.value.email;
            this.popupName = popup.value.name;
            this.popupPhone = popup.value.phone;
            this.http.post(this.sendPopup, {
                name: this.popupName,
                cont: this.popupPhone,
                email: this.popupEmail
            }).subscribe(function (data) {
                // this.msg = data;
                $('#leadModal').modal('hide');
                $('#thankyouModal').modal('show');
            }, function (error) {
                console.log(error);
            });
        }
        else {
            if (!(this.validateEmail(popup.value.email)))
                this.errMsg = 'Invalid Email Address';
            else
                this.errMsg = 'Invalid Contact Number';
        }
    };
    FooterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.headerfooter.getCommanData().subscribe(function (data) {
            _this.footerLinks = _this.headerfooter.coData.footerData.links;
            _this.collectionListOne = _this.headerfooter.coData.footerData.collection_list_1;
            _this.collectionListTwo = _this.headerfooter.coData.footerData.collection_list_2;
            _this.address = _this.headerfooter.coData.footerData.contactInfo.addr;
            _this.call = _this.headerfooter.coData.footerData.contactInfo.tel;
            _this.fax = _this.headerfooter.coData.footerData.contactInfo.fax;
            _this.mail = _this.headerfooter.coData.footerData.contactInfo.email;
            _this.global.fbLink = _this.headerfooter.coData.footerData.sociallinks.fb;
            _this.global.twLink = _this.headerfooter.coData.footerData.sociallinks.twitter;
            _this.global.ytLink = _this.headerfooter.coData.footerData.sociallinks.youtube;
            _this.global.inLink = _this.headerfooter.coData.footerData.sociallinks.insta;
            _this.popupHead = _this.headerfooter.coData.popupData.heading;
            _this.popupDes = _this.headerfooter.coData.popupData.subhead;
            _this.popupImg = _this.headerfooter.coData.popupData.img;
        });
        $(document).ready(function () {
            var key = 'popup';
            console.log(key);
            $('#leadModal').on('hide.bs.modal', function (e) {
                sessionStorage.setItem(key, 'Open');
            });
            var val = sessionStorage.getItem(key);
            if (val) {
                $('#leadModal').modal('hide');
                console.log(val);
            }
            else {
                if (screen.width > 992) {
                    setTimeout(function () {
                        console.log(screen.width);
                        $('#leadModal').modal('show');
                    }, 5000);
                }
            }
        });
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/footer/footer.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__collections_view_collection_service__["a" /* ViewCollectionService */], __WEBPACK_IMPORTED_MODULE_2__header_footer_service__["a" /* HeaderFooterService */], __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__global_service__["a" /* GlobalService */]])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/global.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GlobalService = /** @class */ (function () {
    function GlobalService() {
        this.baseURL = "https://www.bladewatches.com/bladewatches-admin/api";
    }
    GlobalService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], GlobalService);
    return GlobalService;
}());



/***/ }),

/***/ "../../../../../src/app/header-footer.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderFooterService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__global_service__ = __webpack_require__("../../../../../src/app/global.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HeaderFooterService = /** @class */ (function () {
    function HeaderFooterService(http, global) {
        this.http = http;
        this.global = global;
        this.headerUrl = global.baseURL + '/common';
    }
    HeaderFooterService.prototype.getCommanData = function () {
        return this.http.get(this.headerUrl);
    };
    HeaderFooterService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__global_service__["a" /* GlobalService */]])
    ], HeaderFooterService);
    return HeaderFooterService;
}());



/***/ }),

/***/ "../../../../../src/app/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<header class=\"container-fluid\" [ngClass]=\"{'home-header': (route.url === '/home')}\">\n\t<div class=\"container\">\n\t\t<div class=\"row\">\n\t\t<div class=\"col-12\">\n\t\t\t<nav class=\"navbar navbar-expand-lg navbar-light\">\n\t\t\t\t<a class=\"navbar-brand\" [routerLink]=\"['home']\"><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 163.8 40.16\"><defs><style>.cls-1{fill:#0b0b0b;}</style></defs><title>black-logo</title><g id=\"Layer_2\" data-name=\"Layer 2\"><g id=\"Layer_1-2\" data-name=\"Layer 1\"><rect class=\"cls-1\" x=\"72.85\" y=\"26.62\" width=\"10.86\" height=\"11.26\" rx=\"1.23\" ry=\"1.23\"/><path class=\"cls-1\" d=\"M19.51,13.46c0,2.28-3.29,2.8-5.25,2.8A14.94,14.94,0,0,1,11.1,16c-1.39-.28-1.52-.66-1.52-2.09V8.72c0-.11.08-.15.26-.15.65,0,1.08,0,1.87,0a11,11,0,0,1,3.6.52C18.6,10.27,19.51,12.11,19.51,13.46ZM4.37,10.84a27.23,27.23,0,0,1-.21,4.66c-.17.63-.39,1.11-1.25,1.21-.4,0-.91.07-1.57.07s-.69.08-.69.19.3.21.87.21c1.73,0,4.46-.07,5.46-.07,1.3,0,5.24.13,6.29.13,8,0,11.58-2.42,11.58-4.76,0-1.83-2.21-3.14-4.78-4a1.22,1.22,0,0,1-.28-2.17,3,3,0,0,0,1.58-2.41C21.37,2.77,20,.64,12.14.64,10.66.64,9,.71,6.85.71,6,.71,3.25.64,1,.64.3.64,0,.69,0,.85s.26.2.74.2a16,16,0,0,1,1.69.06c1.43.14,1.78.51,1.86,1.24S4.37,3.66,4.37,7ZM9.58,2c0-.24.08-.33.39-.37a12.12,12.12,0,0,1,1.43-.05c3.72,0,5.42,1.93,5.42,3.4a2.48,2.48,0,0,1-1.77,2.31,7.84,7.84,0,0,1-3.13.42H11A1.46,1.46,0,0,1,9.58,6.2Z\"/><path class=\"cls-1\" d=\"M51.94,17.24c-6.25,0-11.15-.13-14.66-.13-1,0-3.74.07-5.47.07-.56,0-.87-.07-.87-.21s.17-.19.69-.19,1.17,0,1.56-.07c.87-.1,1.09-.58,1.27-1.21a27.23,27.23,0,0,0,.21-4.66V7c0-3.33,0-3.94-.08-4.64s-.44-1.1-1.87-1.24a11.4,11.4,0,0,0-1.43-.06c-.47,0-.74-.05-.74-.2s.31-.21,1-.21c2,0,4.76.07,5.93.07s4.22-.07,5.9-.07c.61,0,.91,0,.91.21s-.25.2-.78.2-1.17,0-1.69.06c-1.17.11-1.51.48-1.6,1.24A41.78,41.78,0,0,0,40.09,7v3.85c0,2.79,0,4.31.91,4.7.74.33,1.86.48,5.24.48,1.7,0,5.29,0,5.32,0s.3.12,1.48-.79c.46-.37,2.64-2.52,2.64-2.52s.39.06.2.58a42.13,42.13,0,0,1-2.42,3.47,1.25,1.25,0,0,1-1,.49Z\"/><path class=\"cls-1\" d=\"M81.68,11.3a.49.49,0,0,1,.43.16l3.47,4.63c.22.26-.08.47-.38.51s-.62.07-.62.2.53.15,1.31.17c3.45.05,6.63.05,8.19.05s2-.05,2-.22-.22-.19-.57-.19c-.52,0-1.17,0-1.82-.07-.91-.12-2.17-.42-3.82-2.39C87.1,10.82,80.16,1.9,79.12.7,78.69.2,78.43,0,78,0s-.69.23-1.22.88L65.94,14.6a4.44,4.44,0,0,1-3.68,2c-.35,0-1,0-1.35,0s-.52.06-.52.19.26.22.82.22c2.26,0,4.64-.07,5.12-.07,1.34,0,3.21.07,4.6.07.47,0,.74-.05.74-.22s-.14-.19-.66-.19h-.65c-1.33,0-1.73-.28-1.73-.67a2.75,2.75,0,0,1,.65-1.34l2.35-3.12A.37.37,0,0,1,72,11.3Zm-8.8-1.09c-.17,0-.22-.06-.17-.15l4-5.38a.32.32,0,0,1,.27-.2c.12,0,.17.11.21.2l3.9,5.4c.05.07,0,.13-.22.13Z\"/><path class=\"cls-1\" d=\"M155.44,17c2.22,0,1.6,0,4.34,0h.57a1.21,1.21,0,0,0,1-.49,41,41,0,0,0,2.41-3.46c.2-.51-.2-.58-.2-.58s-2.17,2.16-2.65,2.53c-1.17.9-1.49.81-1.49.81s-.52,0-2.22,0c-.66,0-1.25,0-1.76,0h-.58c-1.77,0-2.39,0-2.9,0-4.42,0-5.11-.33-5.2-1.84,0-.63,0-2.65,0-3.39V8.94c0-.13,0-.22.26-.22l11,.07c.58,0,1.38-1.21.82-1.21l-11.75,0c-.26,0-.31-.09-.31-.23V1.86c0-.16.09-.22.31-.22.86,0,5.38,0,6.12,0l.46,0c.52,0,1.09,0,1.78,0,1.68,0,1.13,0,1.17,0s.33-.09,1.5.82c.46.36,2.64,2.51,2.64,2.51s.4-.06.2-.57A41.36,41.36,0,0,0,158.5,1a1.25,1.25,0,0,0-1-.49H157c-2.75,0-1.09,0-3.31.06H144.1c-1.21,0-3.94-.06-6.25-.06-.64,0-.95,0-.95.2s.27.2.74.2a16.22,16.22,0,0,1,1.7.06c1.42.15,1.77.51,1.85,1.25a41.52,41.52,0,0,1,.09,4.63v3.85a27.17,27.17,0,0,1-.21,4.66c-.17.63-.39,1.11-1.26,1.21-.4,0-.92.07-1.56.07s-.69.08-.69.19.29.22.86.22c1.74,0,4.47-.07,5.46-.07,1.94,0,5.93,0,11.09.05Z\"/><path class=\"cls-1\" d=\"M67.19,29.16a1.13,1.13,0,0,1-.75.25c-2.38,0-13.16,0-23.67.18C31,29.82,27,33.74,26.91,33.79l1.47.08a1.25,1.25,0,0,0,.83-.26c1-.75,4.32-2.71,13.58-3,11.48-.37,24.21,0,24.33,0h.13l2.82-2.44-.42-.87S67.93,28.59,67.19,29.16Z\"/><path class=\"cls-1\" d=\"M40.58,27.6H68.05a1.22,1.22,0,0,1,1.22,1.22v6.86a1.23,1.23,0,0,1-1.22,1.23H46.42a1.26,1.26,0,0,1-.57-.14c-1.23-.67-6.55-3.12-18.79-4.14a1.22,1.22,0,0,1-.33-2.37A46.13,46.13,0,0,1,40.58,27.6Zm30.51-1.26H39.31a43.21,43.21,0,0,0-20.87,6.37l-.8.54,1,0C38.82,34,45,38.07,45.1,38.11l.08.06h25a1.23,1.23,0,0,0,1.23-1.23V26.34Z\"/><path class=\"cls-1\" d=\"M72.59,24.9V39.53H71.41V24.9Zm.31-.64H72a1.23,1.23,0,0,0-1.23,1.23V38.93A1.23,1.23,0,0,0,72,40.16h0a1.23,1.23,0,0,0,1.22-1.23V24.26Z\"/><path class=\"cls-1\" d=\"M85.18,24.9V39.53H84V24.9Zm.32-.64h-.91a1.23,1.23,0,0,0-1.23,1.23V38.93a1.23,1.23,0,0,0,1.23,1.23h0a1.23,1.23,0,0,0,1.22-1.23V24.26Z\"/><path class=\"cls-1\" d=\"M104.77,10.69a25.82,25.82,0,0,1-.22,4.65c-.17.64-.39,1.12-1.25,1.21a15.27,15.27,0,0,1-1.57.08c-.52,0-.69.07-.69.19s.31.21.87.21c1.73,0,4.51.09,5.52.09,1.37,0,5.2.14,9.18.1a25.12,25.12,0,0,0,10.67-2.08l.12-.05c2.42-1.26,5.23-3.82,5.23-6.54s-2-4.43-4.2-5.56c-4.72-2.5-8.74-2.5-14.16-2.5H101.35c-.66,0-1,0-1,.2s.26.2.74.2,1.34,0,1.69.06c1.43.15,1.78.51,1.87,1.25s.08,1.31.08,4.64Zm5.42-4.2c0-1.73,0-3.64,0-4.38,0-.24.12-.35.51-.44a22.34,22.34,0,0,1,2.82-.15c3.39,0,5.93-.16,9.3,1.78,1.8,1,3.95,3.1,3.95,5.88a6.25,6.25,0,0,1-3.47,5.74c-1.51.83-2.47,1.13-6.92,1.28-3.42.11-5.12-.46-5.68-.88-.35-.24-.43-1.11-.48-1.66s-.08-2.1-.08-4.42Z\"/><path class=\"cls-1\" d=\"M89.76,29.16a1.15,1.15,0,0,0,.75.25c2.39,0,13.16,0,23.67.18,11.79.23,15.8,4.15,15.87,4.2l-1.48.08a1.24,1.24,0,0,1-.82-.26c-1-.75-4.32-2.71-13.58-3-11.49-.37-24.21,0-24.34,0h-.12l-2.82-2.44.42-.87S89,28.59,89.76,29.16Z\"/><path class=\"cls-1\" d=\"M129.9,32.63c-12.24,1-17.57,3.47-18.79,4.14a1.3,1.3,0,0,1-.58.14H88.91a1.23,1.23,0,0,1-1.23-1.23V28.82a1.23,1.23,0,0,1,1.23-1.22h27.47a46,46,0,0,1,13.84,2.66A1.22,1.22,0,0,1,129.9,32.63ZM85.55,26.34v10.6a1.23,1.23,0,0,0,1.23,1.23h25l.09-.06c.06,0,6.28-4.07,26.49-4.83l1,0-.8-.54a43.26,43.26,0,0,0-20.87-6.37H85.55Z\"/></g></g></svg></a>\n\t\t\t\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarText\" aria-controls=\"navbarText\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n\t\t\t\t\t<span class=\"navbar-toggler-icon\"></span>\n\t\t\t\t</button>\n\t\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarText\" >\n\t\t\t\t\t<ul class=\"navbar-nav ml-auto\">\n\t\t\t\t\t\t<li class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{ exact: true }\" *ngFor=\"let nav of headerData\">\n\t\t\t\t\t\t\t<a class=\"nav-link\"  [routerLink]=\"[nav.link]\">{{nav.menu}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</nav>\n\t\t</div>\n\t</div>\n\t</div>\n</header>"

/***/ }),

/***/ "../../../../../src/app/header/header.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "header {\n  position: fixed;\n  left: 0;\n  right: 0;\n  z-index: 9999; }\n  header .navbar {\n    padding: 30px 0px;\n    -webkit-transition: all ease 0.3s;\n    transition: all ease 0.3s; }\n  header .navbar .navbar-brand svg {\n      width: 40%;\n      margin-top: 10px;\n      -webkit-transition: all ease 0.3s;\n      transition: all ease 0.3s; }\n  header .navbar .navbar-brand svg .cls-1 {\n        fill: #0b0b0b; }\n  @media only screen and (max-width: 990px) {\n        header .navbar .navbar-brand svg {\n          width: 33%; } }\n  @media only screen and (max-width: 600px) {\n        header .navbar .navbar-brand svg {\n          width: 50%; } }\n  header .navbar .nav-item {\n      min-width: auto;\n      text-align: center; }\n  header .navbar .nav-item .nav-link {\n        color: #000;\n        font-family: 'us', sans-serif;\n        font-weight: 100;\n        font-size: 18px;\n        letter-spacing: 2px;\n        width: -webkit-fit-content;\n        width: -moz-fit-content;\n        width: fit-content;\n        margin: auto;\n        padding: 0px 20px;\n        cursor: pointer; }\n  header .navbar .nav-item .nav-link:after {\n          content: \"\";\n          display: block;\n          height: 2px;\n          bottom: -1px;\n          width: 0px;\n          background-color: #000;\n          -webkit-transition: all .3s ease;\n          transition: all .3s ease; }\n  header .navbar .nav-item .nav-link:hover:after {\n          width: 100%; }\n  header .navbar .nav-item.active .nav-link:after {\n        width: 100%; }\n  header .navbar .nav-item:last-child .nav-link {\n      padding-right: 0px; }\n  @media only screen and (max-width: 990px) {\n    header {\n      margin-bottom: 0px;\n      background: #b01212; }\n      header .navbar {\n        padding: 15px 0px;\n        margin: 0 -15px; }\n        header .navbar .navbar-brand {\n          width: 50%; }\n        header .navbar .nav-item {\n          margin-bottom: 10px; }\n          header .navbar .nav-item:first-child {\n            margin-top: 15px; }\n          header .navbar .nav-item .nav-link {\n            color: #fff; }\n            header .navbar .nav-item .nav-link:after {\n              background-color: #fff; }\n        header .navbar .navbar-toggler {\n          border: none;\n          padding: 0px; }\n          header .navbar .navbar-toggler:focus {\n            outline: none; }\n          header .navbar .navbar-toggler .navbar-toggler-icon {\n            background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(255,255,255,1)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E\"); }\n      header .banner-img {\n        display: none; }\n      header .cls-1 {\n        fill: #fff; } }\n  @media only screen and (max-width: 600px) {\n    header .navbar {\n      padding: 8px 0px; } }\n  @media only screen and (max-width: 900px) {\n  .home-header .cls-1 {\n    fill: #fff !important; }\n  .home-header .nav-item:last-child .nav-link {\n    padding-right: 20px !important; } }\n  .scroll-header {\n  background-color: #AA0202;\n  -webkit-transition: all ease 0.3s;\n  transition: all ease 0.3s;\n  z-index: 99999; }\n  .scroll-header .navbar {\n    -webkit-transition: all ease 0.3s;\n    transition: all ease 0.3s;\n    padding: 5px 0px; }\n  .scroll-header .navbar .nav-item .nav-link {\n      color: #fff; }\n  .scroll-header .navbar .nav-item .nav-link:after {\n        background-color: #fff; }\n  .scroll-header .navbar .navbar-brand svg {\n      -webkit-transition: all ease 0.3s;\n      transition: all ease 0.3s;\n      width: 35%; }\n  .scroll-header .navbar .navbar-brand svg .cls-1 {\n        fill: #fff; }\n  @media only screen and (max-width: 768px) {\n        .scroll-header .navbar .navbar-brand svg {\n          width: 38%; } }\n  @media only screen and (max-width: 768px) {\n        .scroll-header .navbar .navbar-brand svg {\n          width: 55%; } }\n  .scroll-header .cls-1 {\n    fill: #fff; }\n  /* Portrait and Landscape */\n  @media only screen and (max-width: 480px) and (min-width: 320px) {\n  .nav-item:last-child .nav-link {\n    padding-right: 20px !important; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__collections_view_collection_service__ = __webpack_require__("../../../../../src/app/collections/view-collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__header_footer_service__ = __webpack_require__("../../../../../src/app/header-footer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(viewcollection, headerfooter, route) {
        this.viewcollection = viewcollection;
        this.headerfooter = headerfooter;
        this.route = route;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.headerfooter.getCommanData().subscribe(function (data) {
            _this.headerfooter.coData = data;
            _this.headerData = _this.headerfooter.coData.headerData.links;
        });
        $(document).ready(function () {
            $('.navbar-collapse').collapse('hide');
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();
                $('.navbar-collapse').collapse('hide');
                if (scroll >= 10) {
                    $("header").addClass("scroll-header");
                }
                else {
                    $("header").removeClass("scroll-header");
                }
            });
        });
    };
    HeaderComponent.prototype.routeCollection = function (name) {
        this.cName = name;
        this.viewcollection.viewCollection(this.cName);
    };
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-header',
            template: __webpack_require__("../../../../../src/app/header/header.component.html"),
            styles: [__webpack_require__("../../../../../src/app/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__collections_view_collection_service__["a" /* ViewCollectionService */], __WEBPACK_IMPORTED_MODULE_2__header_footer_service__["a" /* HeaderFooterService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["f" /* Router */]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"home\">\n  <div id=\"fullpage\">\n    <section class=\"banner section active\">\n      <div class=\"container\">\n        <div class=\"row align-items-center \">\n          <div class=\"col-lg-7 col-md-8 fade-in-one content\">\n            <h1>{{bannerHeadOne}}\n              <br />\n              <span>{{bannerHeadTwo}}</span>\n            </h1>\n            <p>{{bannerPara}}</p>\n          </div>\n          <div class=\"banner-img\">\n            <img src=\"{{bannerImg}}\" class=\"img-fluid\" alt=\"\">\n          </div>\n        </div>\n      </div>\n    </section>\n    <section class=\"section\">\n      <div class=\"container\">\n        <div class=\"row align-items-center\">\n          <div class=\"col-12 col-lg-8 col-md-10\">\n            <h2 class=\"theam-heading\">{{collectionsHead}}</h2>\n            <p class=\"theam-heading-content\">{{collectionsPara}}</p>\n          </div>\n          <div class=\"col-12\">\n            <swiper [config]=\"config\" class=\"collection\">\n              <div class=\"swiper-wrapper\">\n                <div class=\"swiper-slide card \" *ngFor=\"let collection of homeCollections\" [routerLink]=\"['/collections', collection.slug]\">\n                  <div class=\"card-body fade-in-two\">\n                    <img src=\"{{collection.image}}\" class=\"img-fluid\" width=\"100%\" height=\"100px\" alt=\"\">\n                    <div class=\"collection-name\">\n                      <h3>{{collection.name}}</h3>\n                      <p>{{collection.excerpt}}</p>\n                    </div>\n                  </div>\n                  <!-- (click)=\"routeCollection(collection.slug)\" -->\n                  <a class=\"btn btn-primary btn-block\">VIEW COLLECTION</a>\n                </div>\n              </div>\n              <!-- Add Arrows -->\n              <div class=\"swiper-button-next\">\n                <i class=\"ion-ios-arrow-thin-right\"></i>\n              </div>\n              <div class=\"swiper-button-prev\">\n                <i class=\"ion-ios-arrow-thin-left\"></i>\n              </div>\n            </swiper>\n          </div>\n        </div>\n      </div>\n    </section>\n    <section class=\"section\">\n      <div class=\"container\">\n        <div class=\"row align-items-center\">\n          <div class=\"col-12 col-lg-8 col-md-10\">\n            <h2 class=\"theam-heading\">{{accessoriesHead}}</h2>\n            <p class=\"theam-heading-content\">{{accessoriesPara}}</p>\n          </div>\n          <div class=\"col-12\">\n            <swiper [config]=\"config\" class=\"collection\">\n              <div class=\"swiper-wrapper\">\n                <div class=\"swiper-slide card \" *ngFor=\"let accessories of homeAccessories\" [routerLink]=\"['/accessories', accessories.slug]\">\n                  <div class=\"card-body\">\n                    <img src=\"{{accessories.image}}\" class=\"img-fluid\" alt=\"\">\n                    <div class=\"collection-name\">\n                      <h3>{{accessories.name}}</h3>\n                      <p>{{accessories.excerpt}}</p>\n                    </div>\n                  </div>\n                  <a class=\"btn btn-primary btn-block\">VIEW ACCESSORIES</a>\n                </div>\n              </div>\n              <!-- Add Arrows -->\n              <div class=\"swiper-button-next\">\n                <i class=\"ion-ios-arrow-thin-right\"></i>\n              </div>\n              <div class=\"swiper-button-prev\">\n                <i class=\"ion-ios-arrow-thin-left\"></i>\n              </div>\n            </swiper>\n          </div>\n        </div>\n      </div>\n    </section>\n    <section class=\"section\">\n\n      <div class=\"container\">\n        <div class=\"row align-items-center\">\n          <div class=\"col-12 col-lg-8 col-md-10\">\n            <h2 class=\"theam-heading\">{{socialHead}}</h2>\n            <p class=\"theam-heading-content\">{{socialPara}}</p>\n          </div>\n          <div class=\"col-12\">\n            <div class=\"social-img\">\n              <swiper [config]=\"config\" class=\"collection\">\n                <div class=\"swiper-wrapper\">\n                  <div class=\"swiper-slide fade-in-two\" *ngFor=\"let social of homeSocial; let i = index\">\n                    <div (click)=\"selectedItem = social\">\n                      <a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\".bd-example-modal-lg\">\n                        <img src=\"{{social.image}}\" class=\"img-fluid\" alt=\"\">\n                        <div class=\"social-img-content\">\n                          <div class=\"content\">\n                            <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\"\n                              id=\"Layer_4-1\" x=\"0px\" y=\"0px\" width=\"512px\" height=\"512px\" viewBox=\"0 0 169.063 169.063\"\n                              style=\"enable-background:new 0 0 169.063 169.063;\" xml:space=\"preserve\" class=\"\">\n                              <g>\n                                <g>\n                                  <path d=\"M122.406,0H46.654C20.929,0,0,20.93,0,46.655v75.752c0,25.726,20.929,46.655,46.654,46.655h75.752   c25.727,0,46.656-20.93,46.656-46.655V46.655C169.063,20.93,148.133,0,122.406,0z M154.063,122.407   c0,17.455-14.201,31.655-31.656,31.655H46.654C29.2,154.063,15,139.862,15,122.407V46.655C15,29.201,29.2,15,46.654,15h75.752   c17.455,0,31.656,14.201,31.656,31.655V122.407z\"\n                                    data-original=\"#000000\" class=\"\" fill=\"#dcb305\"></path>\n                                  <path d=\"M84.531,40.97c-24.021,0-43.563,19.542-43.563,43.563c0,24.02,19.542,43.561,43.563,43.561s43.563-19.541,43.563-43.561   C128.094,60.512,108.552,40.97,84.531,40.97z M84.531,113.093c-15.749,0-28.563-12.812-28.563-28.561   c0-15.75,12.813-28.563,28.563-28.563s28.563,12.813,28.563,28.563C113.094,100.281,100.28,113.093,84.531,113.093z\"\n                                    data-original=\"#000000\" class=\"\" fill=\"#dcb305\"></path>\n                                  <path d=\"M129.921,28.251c-2.89,0-5.729,1.17-7.77,3.22c-2.051,2.04-3.23,4.88-3.23,7.78c0,2.891,1.18,5.73,3.23,7.78   c2.04,2.04,4.88,3.22,7.77,3.22c2.9,0,5.73-1.18,7.78-3.22c2.05-2.05,3.22-4.89,3.22-7.78c0-2.9-1.17-5.74-3.22-7.78   C135.661,29.421,132.821,28.251,129.921,28.251z\"\n                                    data-original=\"#000000\" class=\"\" fill=\"#dcb305\"></path>\n                                </g>\n                              </g>\n                            </svg>\n                            <p>View More</p>\n                          </div>\n                        </div>\n                      </a>\n                    </div>\n                  </div>\n                </div>\n                <!-- Add Arrows -->\n                <div class=\"swiper-button-next\">\n                  <i class=\"ion-ios-arrow-thin-right\"></i>\n                </div>\n                <div class=\"swiper-button-prev\">\n                  <i class=\"ion-ios-arrow-thin-left\"></i>\n                </div>\n              </swiper>\n            </div>\n            <!-- Modal -->\n            <div *ngFor=\"let social of homeSocial\">\n              <div class=\"modal fade bd-example-modal-lg custom-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n                aria-hidden=\"true\">\n                <div class=\"modal-dialog modal-md modal-dialog-centered \" role=\"document\">\n                  <div class=\"modal-content \">\n                    <button type=\"button\" class=\"close close-btn\" data-dismiss=\"modal\" aria-label=\"Close\">\n                      <span aria-hidden=\"true\">\n                        <i class=\"ion-ios-close-empty\"></i>\n                      </span>\n                    </button>\n                    <div class=\"modal-body p-0 custom-body\">\n                      <div class=\"text-center\">\n                        <img src=\"{{selectedItem?.image}}\" class=\"img-fluid mb-2\" alt=\"\">\n                        <div class=\"social-icon\">\n                          <p>{{selectedItem?.img_heading}}</p>\n                          <a *ngIf=\"selectedItem?.fb_link!=''\" href=\"{{selectedItem?.fb_link}}\" class=\"inline-block mr-1\">Follow Us\n                            <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 6.07 13\">\n                              <defs>\n                                <style>\n                                  .cls-1 {\n                                    fill: #dcb305;\n                                  }\n\n                                </style>\n                              </defs>\n                              <title>Facebook</title>\n                              <g id=\"Layer_1\" data-name=\"Layer 1\">\n                                <g id=\"Layer_1-1\" data-name=\"Layer 1\">\n                                  <path class=\"cls-1\" d=\"M1.31,2.52V4.31H0V6.5H1.31V13H4V6.5h1.8s.17-1,.25-2.2H4V2.8a.63.63,0,0,1,.58-.52H6.07V0h-2C1.24,0,1.31,2.19,1.31,2.52Z\"></path>\n                                </g>\n                              </g>\n                            </svg>\n                          </a>\n                          <a *ngIf=\"selectedItem?.twitter_link!=''\" href=\"{{selectedItem?.twitter_link}}\" class=\"inline-block mr-1\">Follow Us\n                            <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 15 12\">\n                              <defs>\n                                <style>\n                                  .cls-1 {\n                                    fill: #dcb305;\n                                  }\n\n                                </style>\n                              </defs>\n                              <title>twitter</title>\n                              <g id=\"Layer_2\" data-name=\"Layer 2\">\n                                <g id=\"Layer_2-1\" data-name=\"Layer 1\">\n                                  <path class=\"cls-1\" d=\"M4.72,12a8.62,8.62,0,0,0,8.75-8.62c0-.13,0-.26,0-.39A6.38,6.38,0,0,0,15,1.42a6.36,6.36,0,0,1-1.77.48A3.07,3.07,0,0,0,14.58.22,6.13,6.13,0,0,1,12.63,1,3.12,3.12,0,0,0,8.28.82a3,3,0,0,0-.89,2.9A8.81,8.81,0,0,1,1,.55,3,3,0,0,0,2,4.6,3.25,3.25,0,0,1,.6,4.22v0a3,3,0,0,0,2.47,3,3.11,3.11,0,0,1-1.39,0,3.07,3.07,0,0,0,2.87,2.1A6.25,6.25,0,0,1,0,10.64,8.85,8.85,0,0,0,4.72,12\"></path>\n                                </g>\n                              </g>\n                            </svg>\n                          </a>\n                          <a *ngIf=\"selectedItem?.ytube_link!=''\" href=\"{{selectedItem?.ytube_link}}\" class=\"inline-block mr-1\">Follow Us\n                            <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 13.6 13\">\n                              <defs>\n                                <style>\n                                  .cls-1 {\n                                    fill: #dcb305;\n                                  }\n\n                                </style>\n                              </defs>\n                              <title>Linkedin</title>\n                              <g id=\"Layer_3\" data-name=\"Layer 3\">\n                                <g id=\"Layer_3-1\" data-name=\"Layer 1\">\n                                  <path class=\"cls-1\" d=\"M4.7,13H7.62V8.1a2.19,2.19,0,0,1,.09-.71,1.6,1.6,0,0,1,1.5-1.07c1,0,1.47.81,1.47,2V13H13.6V8C13.6,5.28,12.16,4,10.24,4A2.89,2.89,0,0,0,7.6,5.5h0V4.23H4.7c0,.82,0,8.77,0,8.77ZM1.63,3a1.52,1.52,0,1,0,0-3A1.52,1.52,0,0,0,0,1.52,1.5,1.5,0,0,0,1.61,3ZM3.09,13V4.23H.17V13Z\"></path>\n                                </g>\n                              </g>\n                            </svg>\n                          </a>\n                          <a *ngIf=\"selectedItem?.insta_link!=''\" href=\"{{selectedItem?.insta_link}}\" class=\"inline-block mr-1\">Follow Us\n                            <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\"\n                              id=\"Layer_4-1\" x=\"0px\" y=\"0px\" width=\"512px\" height=\"512px\" viewBox=\"0 0 169.063 169.063\"\n                              style=\"enable-background:new 0 0 169.063 169.063;\" xml:space=\"preserve\" class=\"\">\n                              <g>\n                                <g>\n                                  <path d=\"M122.406,0H46.654C20.929,0,0,20.93,0,46.655v75.752c0,25.726,20.929,46.655,46.654,46.655h75.752   c25.727,0,46.656-20.93,46.656-46.655V46.655C169.063,20.93,148.133,0,122.406,0z M154.063,122.407   c0,17.455-14.201,31.655-31.656,31.655H46.654C29.2,154.063,15,139.862,15,122.407V46.655C15,29.201,29.2,15,46.654,15h75.752   c17.455,0,31.656,14.201,31.656,31.655V122.407z\"\n                                    data-original=\"#000000\" class=\"\" fill=\"#dcb305\"></path>\n                                  <path d=\"M84.531,40.97c-24.021,0-43.563,19.542-43.563,43.563c0,24.02,19.542,43.561,43.563,43.561s43.563-19.541,43.563-43.561   C128.094,60.512,108.552,40.97,84.531,40.97z M84.531,113.093c-15.749,0-28.563-12.812-28.563-28.561   c0-15.75,12.813-28.563,28.563-28.563s28.563,12.813,28.563,28.563C113.094,100.281,100.28,113.093,84.531,113.093z\"\n                                    data-original=\"#000000\" class=\"\" fill=\"#dcb305\"></path>\n                                  <path d=\"M129.921,28.251c-2.89,0-5.729,1.17-7.77,3.22c-2.051,2.04-3.23,4.88-3.23,7.78c0,2.891,1.18,5.73,3.23,7.78   c2.04,2.04,4.88,3.22,7.77,3.22c2.9,0,5.73-1.18,7.78-3.22c2.05-2.05,3.22-4.89,3.22-7.78c0-2.9-1.17-5.74-3.22-7.78   C135.661,29.421,132.821,28.251,129.921,28.251z\"\n                                    data-original=\"#000000\" class=\"\" fill=\"#dcb305\"></path>\n                                </g>\n                              </g>\n                            </svg>\n                          </a>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </section>\n    <section class=\"section  fp-auto-height home-footer\">\n      <app-footer></app-footer>\n    </section>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/home/home.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".home {\n  background: #000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABkEAAAQuCAYAAABrr5qaAAAACXBIWXMAAAsSAAALEgHS3X78AAAgAElEQVR4nOzcuwkCQRRAUUeM7MryzO3O3NwKxsxU0JXVu+cUMDyYDywXdsw55w4AAAAAAKDltF97AgAAAAAAgG8QQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIOlzO5/vaQ8DTGMsut+hqwCYs/A4BAMDXzLn2BPA3Xt6WjdynEf3mfXv3NrLvW3a93Y5j98EZAQAAAAAA+FEnv8MCAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAAIEkEAQAAAAAAkkQQAAAAAAAgSQQBAAAAAACSRBAAAAAAACBJBAEAAAAAAJJEEAAAAAAA4NG+HZwAAAIwEMP9h65LCMKRTND/0SQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEg6235vAAAAAAAAeM4TBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEANIrjVUAAAP+SURBVAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACAJBEEAAAAAABIEkEAAAAAAIAkEQQAAAAAAEgSQQAAAAAAgCQRBAAAAAAASBJBAAAAAACApAvUih4ePerC8AAAAABJRU5ErkJggg==) no-repeat fixed top;\n  min-height: 100vh;\n  background-size: cover; }\n  .home .banner {\n    font-family: 'us', sans-serif;\n    color: #000; }\n  .home .banner .content {\n      margin-bottom: 150px; }\n  .home .banner h1 {\n      font-size: 95px;\n      margin: 0px;\n      line-height: 0.9;\n      text-transform: uppercase; }\n  .home .banner h1 span {\n        font-size: 124px; }\n  .home .banner p {\n      font-family: 'gotham', sans-serif;\n      font-size: 16px;\n      font-weight: 700;\n      padding-top: 20px; }\n  .home .banner .banner-img {\n      width: 40%;\n      position: absolute;\n      right: 0;\n      top: 20%; }\n  .home .collection {\n    height: 450px; }\n  .home .collection .swiper-wrapper .card {\n      max-height: 350px;\n      cursor: pointer; }\n  .home .swiper-button-next,\n  .home .swiper-button-prev {\n    position: absolute;\n    top: 88%;\n    width: 30px;\n    height: 30px;\n    text-align: center;\n    background-size: 10px;\n    margin-top: 5px;\n    padding: 0px;\n    border: 1px solid transparent;\n    line-height: 2;\n    background: #000;\n    outline: none;\n    background-image: none;\n    -webkit-transition: all ease-in-out 0.3s;\n    transition: all ease-in-out 0.3s; }\n  @media screen and (min-width: 1280px) {\n      .home .swiper-button-next,\n      .home .swiper-button-prev {\n        top: 85%; } }\n  .home .swiper-button-next i,\n    .home .swiper-button-prev i {\n      font-size: 30px;\n      color: #fff;\n      line-height: 1; }\n  .home .swiper-button-next i::before,\n      .home .swiper-button-prev i::before {\n        position: absolute;\n        top: 0;\n        bottom: 0;\n        left: 0;\n        right: 0; }\n  .home .swiper-button-next {\n    right: 15px; }\n  .home .swiper-button-next:hover .icon {\n      color: #333; }\n  .home .swiper-button-prev {\n    left: 92%; }\n  .home .swiper-button-prev:hover .icon {\n      color: #333; }\n  @media only screen and (max-width: 990px) {\n    .home .banner .content {\n      margin-bottom: 0px; }\n    .home .banner .banner-img {\n      left: 0;\n      width: 33%;\n      position: relative;\n      margin: auto; }\n    .home .banner h1 {\n      font-size: 42px; }\n      .home .banner h1 span {\n        font-size: 60px; }\n    .home .banner p {\n      font-size: 14px; }\n    .home .swiper-button-prev {\n      left: 85%; }\n    .home .swiper-button-next {\n      right: 5%; }\n    .home .home-footer {\n      display: none; } }\n  @media only screen and (max-width: 768px) {\n    .home .banner .banner-img {\n      width: 65%; }\n    .home .banner h1 {\n      margin-top: 50px; }\n    .home .swiper-button-prev {\n      left: 75%; }\n    .home .custom-modal .modal-dialog .modal-content button.close {\n      top: 5px;\n      right: 5px;\n      font-size: 22px;\n      width: 25px; } }\n  @media only screen and (max-width: 600px) {\n    .home section.section {\n      padding: 50px 0px; } }\n  .home .collection .card {\n    background: #fff; }\n  .home .collection .card:hover {\n      opacity: 1; }\n  .home .collection .card:hover .card-body {\n        -webkit-transform: scale(0.75);\n        transform: scale(0.75); }\n  @media only screen and (max-width: 992px) {\n        .home .collection .card:hover .card-body {\n          -webkit-transform: none;\n          transform: none; } }\n  .home .collection .card .card-body h3 {\n      font-size: 26px; }\n  .home .collection .card .card-body p {\n      font-size: 16px; }\n  .home .collection .card .card-body img {\n      width: 60%; }\n  @media only screen and (max-width: 992px) {\n      .home .collection .card .card-body {\n        -webkit-transition: all 0.3s ease-in-out;\n        transition: all 0.3s ease-in-out;\n        -webkit-transform: scale(0.65);\n        transform: scale(0.65); }\n      .home .collection .card .btn-primary {\n        z-index: 1;\n        -webkit-transform: translateY(0px);\n        transform: translateY(0px);\n        -webkit-transition: all 0.3s ease-in-out;\n        transition: all 0.3s ease-in-out; } }\n  .home .social-img .social-img-content {\n    position: absolute;\n    top: 0;\n    bottom: 0;\n    left: 0;\n    right: 0;\n    max-height: 350px;\n    background-color: rgba(0, 0, 0, 0.85);\n    color: #fff;\n    text-align: center;\n    opacity: 0;\n    -webkit-transition: all ease-in-out 0.3s;\n    transition: all ease-in-out 0.3s; }\n  .home .social-img .social-img-content .content {\n      margin-top: 40%; }\n  .home .social-img .social-img-content .content svg {\n        width: 45px;\n        height: 45px; }\n  .home .social-img .social-img-content .content svg g path {\n          fill: #ffffff; }\n  .home .social-img .social-img-content .content a {\n        color: #fff;\n        outline: none; }\n  .home .social-img .social-img-content .content p {\n        margin-top: 10px; }\n  .home .social-img .social-img-content:hover {\n      opacity: 1;\n      -webkit-transition: all ease-in-out 0.3s;\n      transition: all ease-in-out 0.3s; }\n  .custom-modal .modal-dialog {\n  margin-top: 50px; }\n  .custom-modal .modal-dialog .modal-content {\n    max-height: 565px;\n    margin-top: 30px;\n    margin-bottom: 30px;\n    max-width: 465px;\n    margin-left: auto;\n    margin-right: auto; }\n  .custom-modal .modal-dialog .modal-content .modal-body img {\n      padding-top: 30px;\n      max-width: 375px; }\n  .custom-modal .modal-dialog .modal-content .social-icon {\n      padding: 15px 30px; }\n  .custom-modal .modal-dialog .modal-content .social-icon p {\n        font-size: 13px;\n        margin-bottom: 0.5rem;\n        margin-left: auto;\n        margin-right: auto; }\n  .custom-modal .modal-dialog .modal-content .social-icon a {\n        color: #000; }\n  .custom-modal .modal-dialog .modal-content .social-icon a svg {\n          width: 25px;\n          height: 15px; }\n  .custom-modal .modal-dialog .modal-content .social-icon a svg .cls-1 {\n            fill: #000000; }\n  .custom-modal .modal-dialog .modal-content .social-icon a #Layer_4-1 g path {\n          fill: #000000; }\n  .custom-modal .modal-dialog .modal-content .close {\n      position: absolute;\n      top: 8px;\n      right: 8px;\n      font-size: 30px;\n      z-index: 9999;\n      width: 30px;\n      border-radius: 50px; }\n  .is-out {\n  -webkit-transform: rotateX(0) rotateY(0) !important;\n          transform: rotateX(0) rotateY(0) !important;\n  -webkit-transition-duration: 1s;\n          transition-duration: 1s; }\n  /* Portrait and Landscape */\n  @media only screen and (max-width: 480px) and (min-width: 320px) {\n  .plr0 {\n    padding-left: 15px;\n    padding-right: 15px; }\n  .banner {\n    background: #ffffff !important;\n    background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), color-stop(12%, #ffffff), color-stop(25%, #ffffff), color-stop(39%, #ffffff), color-stop(50%, #ffffff), color-stop(55%, #ffffff), color-stop(74%, #ffffff), color-stop(78%, #ffffff), color-stop(82%, #ffffff), color-stop(86%, #ffffff), color-stop(86%, #ffffff), color-stop(87%, #000000), color-stop(87%, #000000), color-stop(87%, #000000), color-stop(87%, #000000), color-stop(89%, #000000), color-stop(89%, #000000), color-stop(90%, #000000), color-stop(90%, #000000), color-stop(91%, #000000), color-stop(91%, #000000), color-stop(91%, #000000), color-stop(93%, #000000), color-stop(93%, #000000), color-stop(96%, #000000), color-stop(96%, #000000), color-stop(97%, #000000), to(#000000)) !important;\n    background: linear-gradient(to bottom, #ffffff 0%, #ffffff 12%, #ffffff 25%, #ffffff 39%, #ffffff 50%, #ffffff 55%, #ffffff 74%, #ffffff 78%, #ffffff 82%, #ffffff 86%, #ffffff 86%, #000000 87%, #000000 87%, #000000 87%, #000000 87%, #000000 89%, #000000 89%, #000000 90%, #000000 90%, #000000 91%, #000000 91%, #000000 91%, #000000 93%, #000000 93%, #000000 96%, #000000 96%, #000000 97%, #000000 100%) !important;\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#000000', GradientType=0) !important; }\n  section.section {\n    background: #ffffff;\n    /* Old browsers */\n    /* FF3.6-15 */\n    /* Chrome10-25,Safari5.1-6 */\n    background: -webkit-gradient(linear, left top, left bottom, color-stop(52%, #ffffff), color-stop(52%, #0e0e0e));\n    background: linear-gradient(to bottom, #ffffff 52%, #0e0e0e 52%);\n    /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#0e0e0e', GradientType=0);\n    /* IE6-9 */\n    padding: 0px 0px !important; }\n    section.section.fp-section .fp-tableCell {\n      display: table-footer-group;\n      padding-bottom: 0px; }\n  .custom-modal .modal-dialog .modal-content {\n    width: 95%;\n    margin: auto; }\n    .custom-modal .modal-dialog .modal-content button.close {\n      top: 5px;\n      right: 5px;\n      font-size: 22px;\n      width: 25px; } }\n  @media only screen and (max-width: 768px) {\n  .banner {\n    background: #ffffff;\n    /* Old browsers */\n    /* FF3.6-15 */\n    /* Chrome10-25,Safari5.1-6 */\n    background: -webkit-gradient(linear, left top, left bottom, color-stop(76%, #ffffff), color-stop(76%, #000000)) !important;\n    background: linear-gradient(to bottom, #ffffff 76%, #000000 76%) !important;\n    /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#000000', GradientType=0) !important; }\n  section.section {\n    background: #ffffff;\n    /* Old browsers */\n    /* FF3.6-15 */\n    /* Chrome10-25,Safari5.1-6 */\n    background: -webkit-gradient(linear, left top, left bottom, color-stop(52%, #ffffff), color-stop(52%, #0e0e0e));\n    background: linear-gradient(to bottom, #ffffff 52%, #0e0e0e 52%);\n    /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#0e0e0e', GradientType=0);\n    /* IE6-9 */\n    padding: 0px 0px !important; }\n    section.section.fp-section .fp-tableCell {\n      display: table-footer-group;\n      padding-bottom: 0px; }\n  .swiper-wrapper {\n    height: auto; }\n  .swiper-button-prev {\n    left: 35%; }\n    .swiper-button-prev i {\n      font-size: 40px; }\n  .swiper-button-next {\n    right: 35%; }\n    .swiper-button-next i {\n      font-size: 40px; } }\n  @media only screen and (max-width: 1024px) {\n  .banner {\n    background: #ffffff !important;\n    background: -webkit-gradient(linear, left top, left bottom, color-stop(90%, #ffffff), color-stop(90%, #0e0e0e)) !important;\n    background: linear-gradient(to bottom, #ffffff 90%, #0e0e0e 90%) !important;\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#0e0e0e', GradientType=0);\n    /* IE6-9 */ }\n    .banner .content {\n      margin-bottom: 0px !important;\n      margin-top: 50px !important; }\n      .banner .content h1 {\n        font-size: 80px !important; }\n        .banner .content h1 span {\n          font-size: 100px !important; }\n    .banner .banner-img {\n      top: 20% !important; }\n  section.section {\n    background: #ffffff;\n    /* Old browsers */\n    /* FF3.6-15 */\n    /* Chrome10-25,Safari5.1-6 */\n    background: -webkit-gradient(linear, left top, left bottom, color-stop(52%, #ffffff), color-stop(52%, #0e0e0e));\n    background: linear-gradient(to bottom, #ffffff 52%, #0e0e0e 52%);\n    /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#0e0e0e', GradientType=0);\n    /* IE6-9 */\n    padding: 0px 0px !important; }\n    section.section.fp-section .fp-tableCell {\n      display: table-footer-group;\n      padding-bottom: 0px; } }\n  @media only screen and (min-width: 1025px) and (max-width: 1366px) {\n  .banner {\n    background: #ffffff !important;\n    background: -webkit-gradient(linear, left top, left bottom, color-stop(90%, #ffffff), color-stop(90%, #0e0e0e)) !important;\n    background: linear-gradient(to bottom, #ffffff 90%, #0e0e0e 90%) !important;\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#0e0e0e', GradientType=0);\n    /* IE6-9 */ }\n    .banner .content {\n      margin-bottom: 0px !important;\n      margin-top: 50px !important; }\n      .banner .content h1 {\n        font-size: 80px !important; }\n        .banner .content h1 span {\n          font-size: 100px !important; }\n    .banner .banner-img {\n      top: 20% !important; }\n    .banner .custom-modal .modal-dialog .modal-content button.close {\n      top: 5px;\n      right: 5px;\n      font-size: 22px;\n      width: 25px; }\n  section.section {\n    background: #ffffff;\n    /* Old browsers */\n    /* FF3.6-15 */\n    /* Chrome10-25,Safari5.1-6 */\n    background: -webkit-gradient(linear, left top, left bottom, color-stop(52%, #ffffff), color-stop(52%, #0e0e0e));\n    background: linear-gradient(to bottom, #ffffff 52%, #0e0e0e 52%);\n    /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#0e0e0e', GradientType=0);\n    /* IE6-9 */\n    padding: 0px 0px !important; }\n    section.section.fp-section .fp-tableCell {\n      display: table-footer-group;\n      padding-bottom: 0px; }\n  .custom-modal .modal-dialog .modal-content button.close {\n    top: 5px;\n    right: 5px;\n    font-size: 22px;\n    width: 25px; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__collections_view_collection_service__ = __webpack_require__("../../../../../src/app/collections/view-collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__global_service__ = __webpack_require__("../../../../../src/app/global.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomeComponent = /** @class */ (function () {
    function HomeComponent(viewcollection, http, global) {
        this.viewcollection = viewcollection;
        this.http = http;
        this.global = global;
        this.config = {
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            spaceBetween: 30,
            slidesPerView: 3,
            slidesPerGroup: 3,
            loopedSlides: 3,
            breakpoints: {
                480: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: 10
                },
                768: {
                    slidesPerView: 2,
                    slidesPerGroup: 1,
                    spaceBetween: 10
                },
                960: {
                    slidesPerView: 3,
                    spaceBetween: 10
                }
            }
        };
        this.homeUrl = global.baseURL + '/pages/home';
    }
    HomeComponent.prototype.getData = function () {
        return this.http.get(this.homeUrl);
    };
    HomeComponent.prototype.getHomeData = function () {
        var _this = this;
        this.getData().subscribe(function (data) {
            _this.home = data;
            _this.bannerHeadOne = _this.home.homeData.bannerH2;
            _this.bannerHeadTwo = _this.home.homeData.bannerH1;
            _this.bannerPara = _this.home.homeData.bannerPara;
            _this.bannerImg = _this.home.homeData.bannerImg;
            _this.collectionsHead = _this.home.homeData.collHead;
            _this.collectionsPara = _this.home.homeData.collPara;
            _this.accessoriesHead = _this.home.homeData.accesHead;
            _this.accessoriesPara = _this.home.homeData.accesPara;
            _this.socialHead = _this.home.homeData.socHead;
            _this.socialPara = _this.home.homeData.socPara;
            _this.homeCollections = _this.home.collections;
            _this.homeAccessories = _this.home.accessories;
            _this.homeSocial = _this.home.socialFeed;
            // this.spinnerService.hide();
        });
    };
    HomeComponent.prototype.ngOnInit = function () {
        this.getHomeData();
        $(document).ready(function () {
            fullpageFun();
        });
    };
    HomeComponent.prototype.routeCollection = function (name) {
        this.cName = name;
        this.viewcollection.viewCollection(this.cName);
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("../../../../../src/app/home/home.component.html"),
            styles: [__webpack_require__("../../../../../src/app/home/home.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__collections_view_collection_service__["a" /* ViewCollectionService */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__global_service__["a" /* GlobalService */]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/page-not-found/page-not-found.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container pg-not\">\n  <div class=\"row h-100 align-items-center\">\n    <div class=\"col-12 text-center\">\n      <h1>404</h1>\n      <p>Go back to <br/><a [routerLink]=\"['/home']\" class=\"btn theme-btn-red\">HOME</a></p>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/page-not-found/page-not-found.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".pg-not {\n  height: 100vh; }\n  .pg-not h1 {\n    font-family: 'us', sans-serif;\n    font-size: 150px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/page-not-found/page-not-found.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageNotFoundComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
        $(document).ready(function () {
            $.fn.fullpage.destroy('all');
        });
    };
    PageNotFoundComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-page-not-found',
            template: __webpack_require__("../../../../../src/app/page-not-found/page-not-found.component.html"),
            styles: [__webpack_require__("../../../../../src/app/page-not-found/page-not-found.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "../../../../../src/app/seo.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SeoService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/filter.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SeoService = /** @class */ (function () {
    function SeoService(titleService, metaService, router) {
        this.titleService = titleService;
        this.metaService = metaService;
        this.router = router;
    }
    SeoService.prototype.addSeoData = function () {
        var _this = this;
        this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* NavigationEnd */]; }).subscribe(function () {
            var root = _this.router.routerState.snapshot.root;
            while (root) {
                if (root.children && root.children.length) {
                    root = root.children[0];
                }
                else if (root.data && root.data["title"]) {
                    _this.titleService.setTitle(root.data["title"] + " | Blade Watches");
                    var tags = root.data["metatags"];
                    for (var tag in tags) {
                        _this.metaService.addTag({ name: tag, content: tags[tag] });
                    }
                    return;
                }
                else {
                    return;
                }
            }
        });
    };
    SeoService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["b" /* Meta */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["f" /* Router */]])
    ], SeoService);
    return SeoService;
}());



/***/ }),

/***/ "../../../../../src/assets/images/footer-background.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "footer-background.8e33acb4acf9cfaed734.png";

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map