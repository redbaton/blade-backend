-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 21, 2018 at 07:36 AM
-- Server version: 5.5.58-cll
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bladewa7_bw`
--

-- --------------------------------------------------------

--
-- Table structure for table `bw_enquiry`
--

CREATE TABLE `bw_enquiry` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company` text,
  `email` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `cont` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `message` longtext,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_enquiry`
--

INSERT INTO `bw_enquiry` (`id`, `name`, `company`, `email`, `city`, `cont`, `country`, `type`, `message`, `time`) VALUES
(1, 'Merlin Xavier', '', '', '', '', '', 'General Feedback', 'Test ', '2018-11-29 05:29:36'),
(2, 'Merlin Xavier', 'Basel', 'merlin@baselwatch.com', 'Dubai', '0551048924', 'Uae', 'General Feedback', 'Test ', '2018-11-29 05:30:23'),
(3, 'Merlin Xavier', '', '', 'Dubai', '+971551048924', 'United Arab Emirates', 'Watch Repair', 'Repair need for a watch', '2018-12-04 22:24:20'),
(4, 'Merlin Xavier', '', '', 'Dubai', '+971551048924', 'United Arab Emirates', 'Watch Repair', 'Repair need for a watch', '2018-12-04 22:24:31'),
(5, 'Merlin Xavier', '', 'merlin@baselwatch.com', 'Dubai', '+971551048924', 'United Arab Emirates', 'Watch Repair', 'Repair needed', '2018-12-04 22:27:30'),
(6, ' ', '', '', '', '', '', '', '', '2018-12-21 05:46:39'),
(7, 'Pavan Shahu', '', '', 'BENGALURU', '9595929404', '', '', '', '2018-12-21 05:46:45'),
(8, 'Pavan Shahu', '', '', 'BENGALURU', '9595929404', '', '', '', '2018-12-21 05:47:02');

-- --------------------------------------------------------

--
-- Table structure for table `bw_newsletter`
--

CREATE TABLE `bw_newsletter` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_newsletter`
--

INSERT INTO `bw_newsletter` (`id`, `email`, `time`) VALUES
(1, 'merlin@baselwatch.com', '2018-11-26 22:01:16'),
(2, 'vineet@baselwatch.com', '2018-11-26 22:08:48'),
(3, 'simpson@baselwatch.com', '2018-11-26 22:13:23'),
(4, 'merlin@baselwatch.com', '2018-11-26 23:30:15'),
(5, 'merlin@baselwatch.com', '2018-11-26 23:30:15'),
(6, 'merlin@baselwatch.com', '2018-11-26 23:30:32'),
(7, 'merlin@baselwatch.com', '2018-11-28 23:57:08'),
(8, 'suhasht6@gmail.com', '2018-11-29 02:31:13'),
(9, 'merlin@baselwatch.com', '2018-12-04 22:25:40'),
(10, 'pavanshahu@redbaton.co', '2018-12-05 00:28:06'),
(11, 'pavanshahu@redbaton.co', '2018-12-05 00:39:47'),
(12, 'pavanshahu@redbaton.co', '2018-12-05 00:41:24'),
(13, 'pavanshahu@redbaton.co', '2018-12-05 00:42:48'),
(14, 'pavanshahu@redbaton.co', '2018-12-05 00:50:11'),
(15, 'oiuy@kjh.com', '2018-12-21 05:46:15');

-- --------------------------------------------------------

--
-- Table structure for table `bw_offers`
--

CREATE TABLE `bw_offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text,
  `cont` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_offers`
--

INSERT INTO `bw_offers` (`id`, `name`, `cont`, `email`, `time`) VALUES
(1, 'Merlin ', '551048924', 'merlin@baselwatch.com', '2018-11-26 21:49:36'),
(2, 'jwabkj', 'kdck', 'kjbc', '2018-11-28 00:40:06'),
(3, 'ehgafuy', 'jehrbsu', 'hewfbsdc', '2018-11-28 02:30:41'),
(4, 'Suhash', '813313', '8106087692', '2018-11-28 03:22:54'),
(5, 'Suhash', '8106087692', 'suhasht5@gmail.com', '2018-11-28 03:27:07'),
(6, 'rfejdhiu', '8799823493', 'eoiewn@nkrj.ooinw', '2018-11-28 04:43:39'),
(7, 'rfejdhiu', '8799823493', 'eoiewn@nkrj.ooinw', '2018-11-28 04:43:40'),
(8, 'hhedsh', '7937276191', 'hedh@re.in', '2018-11-28 22:22:14'),
(9, 'Merlin', '0551048924', 'merlin@baselwatch.com', '2018-11-28 23:53:55'),
(10, 'Merlin ', '0551048924', 'merlin@baselwatch.com', '2018-12-04 22:22:33'),
(11, 'Pavan Shahu', '9595929404', 'pavanshahu@redbaton.co', '2018-12-05 00:20:10'),
(12, 'Pavan Shahu', '9595929404', 'pavanshahu@redbaton.co', '2018-12-05 00:20:10'),
(13, 'Pavan Shahu', '9595929404', 'pavanshahu@redbaton.co', '2018-12-05 00:20:10'),
(14, 'Pavan Shahu', '9595929404', 'dk966512@gmail.com', '2018-12-21 05:37:50'),
(15, 'qerty', '9658741230', 'dk966512@gmail.com', '2018-12-21 05:42:33'),
(16, 'qwertytrewq', '9595929404', 'dk966512@gmail.com', '2018-12-21 05:44:02');

-- --------------------------------------------------------

--
-- Table structure for table `bw_pages`
--

CREATE TABLE `bw_pages` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `contents` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_pages`
--

INSERT INTO `bw_pages` (`id`, `name`, `slug`, `contents`) VALUES
(1, 'Home', 'home', '{\"metadesc\":\"\",\"metakeys\":\"\",\"bannerH2\":\"THE ULTIMATE\",\"bannerH1\":\"TIME MACHINE\",\"bannerPara\":\"BLADE has been making an impact since its launch in 2000, and continues to triumph among today\'s modish men and women. Created to provide outstanding quality at affordable prices, BLADE\'s collection symbolize vigor and vivacity through each of its carefully designed and crafted watches. Its bold yet elegant designs express an aura of free-spirited individuality.\",\"bannerImg\":\"Blade-watch-banner-image_1600_1600_compressed.png\",\"collHead\":\"WATCHES\",\"collPara\":\"I  Discover our world of time  I\",\"accesHead\":\"ACCESSORIES\",\"accesPara\":\"I  Roll up your sleeves or button those cuffs, we\'ve got your wrists covered  I\",\"socHead\":\"#BLADEWATCHES\",\"socPara\":\" I    Stalk us\\r\\n  ... don\'t miss a second  I\"}'),
(2, 'AboutUs', 'aboutus', '{\"metadesc\":\"\",\"metakeys\":\"\",\"aboutPara\":\"BLADE has been making an impact since its launch in 2000, and continues to triumph among today\'s modish men and women. Created to provide outstanding quality at affordable prices, BLADE\'s collection symbolize vigor and vivacity through each of its carefully designed and crafted watches. Its bold yet elegant designs express an aura of free-spirited individuality.\\r\\n\\r\\nThe brand highlights itself as \'The Ultimate Time Machine,\' - Swiss movements cosseted in surgical grade solid steel casing, striking and suitable for any occasion, be it formal, evening dress, casual, or sports wear. Bands are available in solid stainless steel, genuine leather, or anti-allergenic rubber. Day-date, Big-date, Chronograph with alarm, Multi-function with moon-phase and retrograde are a part of the movements used in BLADE watches. The collection also includes titanium and ceramic watches, sapphire glass with and without magnifier, genuine Mother-of-Pearl dials, bezels fitted with Swarovski crystals, and 50-100 meters water resistance.\\r\\n\\r\\nBLADE is a registered trademark of exclusive agents, Basel Watch Company, L.L.C. Its distribution network begins in Dubai, United Arab Emirates, where the head office is located and covers all regions of the country. BLADE watches are also available in Oman, Qatar, Bahrain, Saudi Arabia, Kuwait, India, Bangladesh, Sri Lanka and USA.\",\"abtImg\":\"aboutus1.png\"}'),
(3, 'ContactUs', 'contactus', '{\"metadesc\":\"\",\"metakeys\":\"\",\"contPara\":\"All the components of your watch have been manufactured using high-quality materials and subjected to rigorous testing. However, superior quality also requires maintenance. Treat your Blade watch with care and attention, and you will be rewarded with precision timekeeping.\",\"contImg\":\"contact-banner-img1.png\"}');

-- --------------------------------------------------------

--
-- Table structure for table `bw_popup`
--

CREATE TABLE `bw_popup` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` varchar(255) DEFAULT NULL,
  `subhead` text,
  `img` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_popup`
--

INSERT INTO `bw_popup` (`id`, `heading`, `subhead`, `img`, `status`) VALUES
(1, 'Don\'t Miss Out! ', 'Be the first to learn about our promotions, new arrivals, competitions and more. ', 'blade-pop-up.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bw_products`
--

CREATE TABLE `bw_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `metadesc` text,
  `metakeys` text,
  `excerpt` text,
  `attributes` longtext,
  `collections` text,
  `accessories` text,
  `status` tinyint(4) DEFAULT NULL,
  `image1` varchar(255) DEFAULT NULL,
  `hover1` varchar(255) DEFAULT NULL,
  `image2` varchar(255) DEFAULT NULL,
  `hover2` varchar(255) DEFAULT NULL,
  `image3` varchar(255) DEFAULT NULL,
  `hover3` varchar(255) DEFAULT NULL,
  `image4` varchar(255) DEFAULT NULL,
  `hover4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_products`
--

INSERT INTO `bw_products` (`id`, `name`, `slug`, `metadesc`, `metakeys`, `excerpt`, `attributes`, `collections`, `accessories`, `status`, `image1`, `hover1`, `image2`, `hover2`, `image3`, `hover3`, `image4`, `hover4`) VALUES
(8, 'Wallet', 'wallet', '', '', 'Roaring and relentless', '{\"key1\":\"Color\",\"value1\":\"Black\",\"key2\":\"Material\",\"value2\":\"Leather\",\"key3\":\"Targeted Group\",\"value3\":\"Men\",\"key4\":\"\",\"value4\":\"\",\"key5\":\"\",\"value5\":\"\",\"key6\":\"\",\"value6\":\"\",\"key7\":\"\",\"value7\":\"\",\"key8\":\"\",\"value8\":\"\"}', '', '11', 1, 'Wallet-1.jpg', 'Wallet-11.jpg', 'Wallet-12.jpg', 'Wallet-13.jpg', 'Wallet-14.jpg', 'Wallet-15.jpg', 'Wallet-16.jpg', 'Wallet-17.jpg'),
(9, 'Cufflink', 'cufflink', '', '', 'Roaring and relentless', '{\"key1\":\"Targeted Group\",\"value1\":\"Men\",\"key2\":\"Material\",\"value2\":\"Stainless Steel\",\"key3\":\"Details\",\"value3\":\"Actual watch movement inside\",\"key4\":\"Color\",\"value4\":\"Rose Gold/Black\",\"key5\":\"Jewlery Type\",\"value5\":\"Cufflinks\",\"key6\":\"\",\"value6\":\"\",\"key7\":\"\",\"value7\":\"\",\"key8\":\"\",\"value8\":\"\"}', '', '12', 1, 'Cufflinks-1-1.jpg', 'Cufflinks-1-11.jpg', 'Cufflinks-1-12.jpg', 'Cufflinks-1-13.jpg', 'Cufflinks-1-14.jpg', 'Cufflinks-1-15.jpg', NULL, NULL),
(10, 'Bracelet', 'bracelet', '', '', 'Roaring and relentless', '{\"key1\":\"Jewlery Type\",\"value1\":\"Bracelet\",\"key2\":\"Targeted Group\",\"value2\":\"Men\",\"key3\":\"Color\",\"value3\":\"Silver/Gold\",\"key4\":\"Material \",\"value4\":\"Solid Stainless Steel\",\"key5\":\"Details\",\"value5\":\"Adjustable links\",\"key6\":\"\",\"value6\":\"\",\"key7\":\"\",\"value7\":\"\",\"key8\":\"\",\"value8\":\"\"}', '', '13', 1, 'Bracelet-1-1.jpg', 'Bracelet-1-11.jpg', 'Bracelet-1-12.jpg', 'Bracelet-1-13.jpg', 'Bracelet-1-14.jpg', 'Bracelet-1-15.jpg', NULL, NULL),
(11, 'Bracelet 1', 'bracelet-1', '', '', 'Roaring and relentless', '{\"key1\":\"Jewlery Type\",\"value1\":\"Bracelet\",\"key2\":\"Targeted Group\",\"value2\":\"Men\",\"key3\":\"Color\",\"value3\":\"Silver\",\"key4\":\"Material \",\"value4\":\"Solid Stainless Steel\",\"key5\":\"Details\",\"value5\":\"Adjustable links\",\"key6\":\"\",\"value6\":\"\",\"key7\":\"\",\"value7\":\"\",\"key8\":\"\",\"value8\":\"\"}', '', '13', 1, 'Bracelet-2-1.jpg', 'Bracelet-2-11.jpg', 'Bracelet-2-12.jpg', 'Bracelet-2-13.jpg', 'Bracelet-2-14.jpg', 'Bracelet-2-15.jpg', NULL, NULL),
(12, 'Bracelet 2', 'bracelet-2', '', '', 'Roaring and relentless', '{\"key1\":\"Jewlery Type\",\"value1\":\"Bracelet\",\"key2\":\"Targeted Group\",\"value2\":\"Men\",\"key3\":\"Color\",\"value3\":\"Silver/Black\",\"key4\":\"Material \",\"value4\":\"Solid Stainless Steel and PU material\",\"key5\":\"Details\",\"value5\":\"Adjustable links\",\"key6\":\"\",\"value6\":\"\",\"key7\":\"\",\"value7\":\"\",\"key8\":\"\",\"value8\":\"\"}', '', '13', 1, 'Bracelet-3-1.jpg', 'Bracelet-3-11.jpg', 'Bracelet-3-12.jpg', 'Bracelet-3-13.jpg', 'Bracelet-3-14.jpg', 'Bracelet-3-15.jpg', NULL, NULL),
(13, '10-3493G-TWO', '10-3493g-two', 'Love the look of an automatic watch, but hate having to wind it regularly. ', '', 'Roaring and relentless', '{\"key1\":\"Watch Shape\",\"value1\":\"Round\",\"key2\":\"Color\",\"value2\":\"Brown (Band), White (Dial)\",\"key3\":\"Material\",\"value3\":\"22k Gold Plated Stainless Steel (Case), Leather (Band)\",\"key4\":\"Dial Case Diameter Size\",\"value4\":\"46 mm\",\"key5\":\"Case Thickness\",\"value5\":\"7 mm\",\"key6\":\"Band width\",\"value6\":\"22\",\"key7\":\"Water resistant\",\"value7\":\"5 ATM\",\"key8\":\"Gender\",\"value8\":\"Men\"}', '4', '', 1, '10-3493G-TWO-1.jpg', '10-3493G-TWO-11.jpg', '10-3493G-TWO-12.jpg', '10-3493G-TWO-13.jpg', '10-3493G-TWO-14.jpg', '10-3493G-TWO-15.jpg', NULL, NULL),
(14, '10-3493G-TNN', '10-3493g-tnn', '', '', 'Roaring and relentless', '{\"key1\":\"Watch Shape\",\"value1\":\"Round\",\"key2\":\"Color\",\"value2\":\"Black (Band), Black & Rose Gold (Dial)\",\"key3\":\"Material\",\"value3\":\"18K Rose Gold Plated Stainless Steel (Case), Leather (Band)\",\"key4\":\"Dial Case Diameter Size\",\"value4\":\"46 mm\",\"key5\":\"Case Thickness\",\"value5\":\"7 mm\",\"key6\":\"Band width\",\"value6\":\"22\",\"key7\":\"Water resistant\",\"value7\":\"5 ATM\",\"key8\":\"Gender\",\"value8\":\"Men\"}', '4', '', 1, '10-3493G-TNN-1.jpg', '10-3493G-TNN-11.jpg', '10-3493G-TNN-12.jpg', '10-3493G-TNN-13.jpg', '10-3493G-TNN-14.jpg', '10-3493G-TNN-15.jpg', NULL, NULL),
(15, '30-3388G-NNB', '30-3388g-nnb', '', '', 'Roaring and relentless', '{\"key1\":\"Watch Shape\",\"value1\":\"Round\",\"key2\":\"Color\",\"value2\":\"Blue (Band), Black (Dial)\",\"key3\":\"Material\",\"value3\":\"Stainless Steel (Case), Leather (Band)\",\"key4\":\"Dial Case Diameter Size\",\"value4\":\"44 mm\",\"key5\":\"Case Thickness\",\"value5\":\"8 mm\",\"key6\":\"Band width\",\"value6\":\"22\",\"key7\":\"Water resistant\",\"value7\":\"5 ATM\",\"key8\":\"Gender\",\"value8\":\"Men\"}', '4', '', 1, '30-3388G-NNB-1.jpg', '30-3388G-NNB-11.jpg', '30-3388G-NNB-12.jpg', '30-3388G-NNB-13.jpg', '30-3388G-NNB-14.jpg', '30-3388G-NNB-15.jpg', NULL, NULL),
(16, '30-3479G-NGB', '30-3479g-ngb', '', '', 'Roaring and relentless', '{\"key1\":\"Watch Shape\",\"value1\":\"Round\",\"key2\":\"Color\",\"value2\":\"Teal (Band), Grey & Blue (Dial)\",\"key3\":\"Material\",\"value3\":\"Stainless Steel (Case), Leather (Band)\",\"key4\":\"Dial Case Diameter Size\",\"value4\":\"44 mm\",\"key5\":\"Case Thickness\",\"value5\":\"9 mm\",\"key6\":\"Band width\",\"value6\":\"20\",\"key7\":\"Water resistant\",\"value7\":\"5 ATM\",\"key8\":\"Gender\",\"value8\":\"Men\"}', '10', '', 1, '30-3479G-NGB-1.jpg', '30-3479G-NGB-11.jpg', '30-3479G-NGB-12.jpg', '30-3479G-NGB-13.jpg', '30-3479G-NGB-14.jpg', '30-3479G-NGB-15.jpg', NULL, NULL),
(17, '10-3210G-SW', '10-3210g-sw', '', '', 'Roaring and relentless', '{\"key1\":\"Watch Shape\",\"value1\":\"Round\",\"key2\":\"Color\",\"value2\":\"Silver (Band), White (Dial)\",\"key3\":\"Material\",\"value3\":\"Stainless Steel (Case), Stainless Steel (Band)\",\"key4\":\"Dial Case Diameter Size\",\"value4\":\"42 mm\",\"key5\":\"Case Thickness\",\"value5\":\"8 mm\",\"key6\":\"Band width\",\"value6\":\"22\",\"key7\":\"Water resistant\",\"value7\":\"5 ATM\",\"key8\":\"Gender\",\"value8\":\"Men\"}', '10', '', 1, '10-3210G-SW-1.jpg', '10-3210G-SW-11.jpg', '10-3210G-SW-12.jpg', '10-3210G-SW-13.jpg', '10-3210G-SW-14.jpg', '10-3210G-SW-15.jpg', NULL, NULL),
(18, '10-3494G-SN', '10-3494g-sn', '', '', 'Roaring and relentless', '{\"key1\":\"Watch Shape\",\"value1\":\"Round\",\"key2\":\"Color\",\"value2\":\"Silver (Band), Black & Silver (Dial)\",\"key3\":\"Material\",\"value3\":\"Stainless Steel (Case), Stainless Steel (Band)\",\"key4\":\"Dial Case Diameter Size\",\"value4\":\"46 mm\",\"key5\":\"Case Thickness\",\"value5\":\"9 mm\",\"key6\":\"Band width\",\"value6\":\"25\",\"key7\":\"Water resistant\",\"value7\":\"5 ATM\",\"key8\":\"Gender\",\"value8\":\"Men\"}', '1', '', 1, '10-3494G-SN-1.jpg', '10-3494G-SN-11.jpg', '10-3494G-SN-12.jpg', '10-3494G-SN-13.jpg', '10-3494G-SN-14.jpg', '10-3494G-SN-15.jpg', NULL, NULL),
(19, '10-3352G-NN', '10-3352g-nn', '', '', 'Roaring and relentless', '{\"key1\":\"Watch Shape\",\"value1\":\"Round\",\"key2\":\"Color\",\"value2\":\"Black (Band), Black (Dial)\",\"key3\":\"Material\",\"value3\":\"Stainless Steel & Hi-Tech Ceramic (Case), Stainless Steel (Band)\",\"key4\":\"Dial Case Diameter Size\",\"value4\":\"44 mm\",\"key5\":\"Case Thickness\",\"value5\":\"10 mm\",\"key6\":\"Band width\",\"value6\":\"22\",\"key7\":\"Water resistant\",\"value7\":\"5 ATM\",\"key8\":\"Gender\",\"value8\":\"Men\"}', '3', '', 1, '10-3352G-NN-3.jpg', '10-3352G-NN-31.jpg', '10-3352G-NN-32.jpg', '10-3352G-NN-33.jpg', '10-3352G-NN-34.jpg', '10-3352G-NN-35.jpg', NULL, NULL),
(20, '10-3245G-SSW', '10-3245g-ssw', '', '', 'Roaring and relentless', '{\"key1\":\"Watch Shape\",\"value1\":\"Round\",\"key2\":\"Color\",\"value2\":\"Silver (Band), White (Dial)\",\"key3\":\"Material\",\"value3\":\"Stainless Steel (Case), Stainless Steel (Band)\",\"key4\":\"Dial Case Diameter Size\",\"value4\":\"44 mm\",\"key5\":\"Case Thickness\",\"value5\":\"10 mm\",\"key6\":\"Band width\",\"value6\":\"22\",\"key7\":\"Water resistant\",\"value7\":\"50 M\",\"key8\":\"Gender\",\"value8\":\"Men\"}', '2', '', 1, '10-3245G-SSW-1.png', '10-3245G-SSW-11.png', '10-3245G-SSW-12.jpg', '10-3245G-SSW-13.jpg', '10-3245G-SSW-14.jpg', '10-3245G-SSW-15.jpg', NULL, NULL),
(21, '25-3381LSS-GW', '25-3381lss-gw', '', '', 'Roaring and relentless', '{\"key1\":\"Watch Shape\",\"value1\":\"Round\",\"key2\":\"Color\",\"value2\":\"Gold (Band), White (Dial)\",\"key3\":\"Material\",\"value3\":\"22k Gold Plated Stainless Steel (Case), 22k Gold Plated Stainless Steel (Band)\",\"key4\":\"Dial Case Diameter Size\",\"value4\":\"41.5mm\",\"key5\":\"Case Thickness\",\"value5\":\"9 mm\",\"key6\":\"Band width\",\"value6\":\"20\",\"key7\":\"Water resistant\",\"value7\":\"30 M\",\"key8\":\"Gender\",\"value8\":\"Women\"}', '9', '', 1, '25-3381LSS-GW-1.jpg', '25-3381LSS-GW-11.jpg', '25-3381LSS-GW-12.jpg', '25-3381LSS-GW-13.jpg', '25-3381LSS-GW-14.jpg', '25-3381LSS-GW-15.jpg', NULL, NULL),
(22, '3529G', '3529g', '', '', 'Roaring and relentless', '{\"key1\":\"Watch Shape\",\"value1\":\"Round\",\"key2\":\"Color\",\"value2\":\"Blue (Band), White (Dial)\",\"key3\":\"Material\",\"value3\":\"Stainless Steel (Case), Leather (Band)\",\"key4\":\"Dial Case Diameter Size\",\"value4\":\"44 mm\",\"key5\":\"Case Thickness\",\"value5\":\"9 mm\",\"key6\":\"Band width\",\"value6\":\"22\",\"key7\":\"Water resistant\",\"value7\":\"5 ATM\",\"key8\":\"Gender\",\"value8\":\"Men\"}', '1', '', 1, '3529G-1.jpg', '3529G-11.jpg', '3529G-12.jpg', '3529G-13.jpg', '3529G-14.jpg', '3529G-15.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bw_product_categories`
--

CREATE TABLE `bw_product_categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `metadesc` text,
  `metakeys` text,
  `excerpt` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `banimage` varchar(255) DEFAULT NULL,
  `mbanimage` varchar(255) DEFAULT NULL,
  `type` smallint(6) DEFAULT NULL,
  `linktofooter` smallint(6) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_product_categories`
--

INSERT INTO `bw_product_categories` (`id`, `name`, `slug`, `metadesc`, `metakeys`, `excerpt`, `image`, `banimage`, `mbanimage`, `type`, `linktofooter`, `status`) VALUES
(1, 'AGENDA', 'agenda', '', '', 'All Day Meetings', 'Watch-1.jpg', 'AGENDA_WEB-1005.jpg', 'AGENDA_WEB-1006.jpg', 1, 1, 1),
(2, 'DELTA', 'delta', '', '', 'Roaring & relentless', '14918359246.png', 'DELTA_WEB-1002.jpg', 'DELTA_WEB-1003.jpg', 1, 1, 1),
(3, 'DINAMICO', 'dinamico', '', '', 'Roaring & relentless', '1491835975.png', 'DINAMICO_WEB-1002.jpg', 'DINAMICO_WEB-1003.jpg', 1, 1, 1),
(4, 'FALCO', 'falco', '', '', 'Roaring & relentless', '14918359248.png', 'FALCO_WEB-1002.jpg', 'FALCO_WEB-1003.jpg', 1, 1, 1),
(5, 'MAVERICK', 'maverick', '', '', 'Roaring & relentless', '7.png', 'blade_maverik_1600x400.jpg', 'maverick_desktop3.jpg', 1, 2, 1),
(9, 'MYSTIQUE', 'mystique', '', '', 'MYSTIQUE watches', '14918359752.png', 'MYSTIQUE_WEB-100.jpg', 'MYSTIQUE_WEB-1001.jpg', 1, 2, 1),
(10, 'INTREPID', 'intrepid', '', '', 'INTREPID COLLECTION', '72.png', 'INTERPID_WEB_copy-100.jpg', 'INTERPID_WEB_copy-1001.jpg', 1, 2, 1),
(11, 'Wallet', 'wallet', '', '', 'Roaring & relentless', 'Wallet-1.jpg', 'wallet-web.png', 'wallet-web1.png', 2, 0, 1),
(12, 'Cufflinks', 'cufflinks', '', '', 'Roaring & relentless', 'Cufflinks-1-1.jpg', 'cufflinks-web.png', 'cufflinks-web1.png', 2, 0, 1),
(13, 'Bracelet', 'bracelet', '', '', 'Roaring & relentless', 'Bracelet-1-1.jpg', 'bracelet-web.png', 'bracelet-web1.png', 2, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bw_settings`
--

CREATE TABLE `bw_settings` (
  `ID` int(11) UNSIGNED NOT NULL,
  `meta_key` text,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_settings`
--

INSERT INTO `bw_settings` (`ID`, `meta_key`, `meta_value`) VALUES
(1, 'favicon', 'blade-favicon.ico'),
(2, 'main_logo', 'Blade-Logo.svg'),
(3, 'site_title', 'Blade Watches'),
(4, 'reset_pw_key', ''),
(5, 'reset_pw_time', ''),
(6, 'admin_fname', 'admin'),
(7, 'admin_lname', ''),
(8, 'menu_1', 'HOME'),
(9, 'menu_2', 'ABOUT '),
(10, 'menu_3', 'WATCHES'),
(11, 'menu_4', 'ACCESSORIES'),
(12, 'menu_2_link', '/aboutus'),
(13, 'menu_3_link', '/collections/agenda'),
(14, 'menu_4_link', '/accessories/wallet'),
(15, 'footer_logo', 'Blade-Logo.svg'),
(16, 'facebook', 'https://www.facebook.com/BladeWatches/'),
(17, 'gmail', ''),
(18, 'twitter', 'https://twitter.com/bladewatches'),
(19, 'linkedin', ''),
(20, 'insta', 'https://www.instagram.com/bladewatches/'),
(21, 'gplus', ''),
(22, 'pinterest', ''),
(23, 'copyrights', 'Terms Of Use & Privacy Policy Al Wafaa | Copyright © 2013 Blade Watches'),
(24, 'contemail', 'info@baselwatch.com'),
(26, 'addr', ''),
(27, 'cont', ''),
(28, 'email', 'customercare@bladewatches.com'),
(37, 'scroll_logo', 'Blade-Logo.svg'),
(38, 'youtube', 'https://www.youtube.com/watch?v=wcP6nLEMWq8&list=PLuZFlOpap4drL6mcEblDnMx62npy7OWJD'),
(39, 'fax', ''),
(40, 'menu_1_link', '/home'),
(41, 'foot_menu_1', 'About'),
(42, 'foot_menu_2', 'Store Locator'),
(43, 'foot_menu_3', 'Contact'),
(44, 'foot_menu_1_link', '/aboutus'),
(45, 'foot_menu_2_link', '/contactus'),
(46, 'foot_menu_3_link', '/contactus#contactForm'),
(47, 'menu_5', 'CONTACT'),
(48, 'menu_5_link', '/contactus');

-- --------------------------------------------------------

--
-- Table structure for table `bw_socialfeed`
--

CREATE TABLE `bw_socialfeed` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `img_heading` varchar(255) DEFAULT NULL,
  `fb_link` varchar(255) DEFAULT NULL,
  `twitter_link` varchar(255) DEFAULT NULL,
  `ytube_link` varchar(255) DEFAULT NULL,
  `insta_link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_socialfeed`
--

INSERT INTO `bw_socialfeed` (`id`, `image`, `img_heading`, `fb_link`, `twitter_link`, `ytube_link`, `insta_link`) VALUES
(1, '1.jpg', 'Red Eye #30-3508G  Chronograph watch / Ionic black / Stainless Steel / Falco Series / Genuine Leather \' \' \' \' \' \' \' \' #blade #bladewatches #wristassured #falco #watchesformen #watchesofinstagram', '', '', '', 'https://www.instagram.com/p/BfNHpLgFXS6/?taken-by=bladewatches'),
(2, '2.jpg', 'Here\'s to all those who think different.???? --------------------------------------  We owe most of our great inventions and achievements to those who wanted to do things differently.', 'https://www.facebook.com/BladeWatches/photos/a.394921388261/10155606963358262/?type=3&theater', '', '', ''),
(3, '3.jpg', 'Ladies.. this one\'s got the good looks of a classy watch and is clever too! Kinda like \'No-makeup\' makeup? ????  #Smartwatch #HiddenAgenda #traditionalyetsmart #hiddenscreen #nomakeupmakeup', '', '', '', 'https://www.instagram.com/p/BUyW7t9gqC3/?taken-by=bladewatches'),
(4, '4.jpg', 'This ain\'t no simpleton ???? Premium edition men\'s watch by BLADE. The rose gold plated stainless steel case features a green intricate dial with chronograph movement, date and tachymeter', 'https://www.facebook.com/BladeWatches/photos/a.394921388261/10155307435168262/?type=3&theater', '', '', ''),
(5, '5.jpg', 'Introducing the all new Agenda OFF-CENTER. Independent Seconds designed to break free - 3529G  #offcenter #individuality #agenda #Independent #bedifferent #breakfree #outofthenorm #blade2018', '', '', '', 'https://www.instagram.com/p/BgvPt_MFlm6/?taken-by=bladewatches'),
(6, '6.jpg', 'Carry your style on your wrist. #bladewatches #dinamico #wristwatch #blade #uae #pictureoftheday #me #amazing #wristfashion #fashion #bestdeals #Ceramic', 'https://www.facebook.com/BladeWatches/photos/a.394921388261/10154173704433262/?type=3&theater', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `bw_stores`
--

CREATE TABLE `bw_stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `addr` text,
  `tel` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_stores`
--

INSERT INTO `bw_stores` (`id`, `name`, `addr`, `tel`, `fax`, `email`, `country`, `category`) VALUES
(1, 'Basel Watch Company', 'Dubai Outlet Mall, F83A, First Floor, Dubai, UAE', '971 4 445 8540', '', '', 'UAE', 'Store'),
(2, 'Basel Watch House', 'Shop No. F-10, 1st Floor, Capitol Mall, Kannur-2', '0497-2762000', '', 'bwh@baselwatch.com', 'India', 'Store'),
(3, 'Classic Trading Centre WLL', 'P O Box 1492, Round about Bab-Al-Bahrain, Manama', '', '', 'clasicwatches@gmail.com', 'Bahrain', 'Store'),
(4, 'Al Basel Watch ', 'Shop No. 11, Souk Nazda, P.O.Box – 8898', '', '', 'qtrsales@baselwatch.com', 'Qatar', 'Store'),
(5, 'Hour Choice L.L.C', 'Lulu Hyper Market, Doha', '974 4466 9731', '', '', 'Qatar', 'Store'),
(6, 'Asad Muscat Trading ', 'Ruwi High Street Muscat', '968 9 231 7236', '', 'omansales@baselwatch.com', 'Oman', 'Store'),
(7, 'Basel Watch House', 'Basel Plaza, KSEB Junction, Kattappana, Idukki, Kerala', '+91 8086 904811', '', 'bwh@baselwatch.com', 'India', 'Store'),
(8, 'Basel Watch Co. L.L.C', 'Dubai', ' 971 4 326 1818', '971 4 392 6795', 'info@baselwatch.com', 'UAE', 'Store'),
(9, 'Basel Watch Co.(Br.)', 'Ground Floor Near, Entrance 1, Al Ghurair Centre, Dubai', '', '', 'info@baselwatch.com', 'UAE', 'Store'),
(10, 'Basel Watch Co.(Br.)', 'Mezzanine Floor,City Centre, Sharjah', '', '', 'info@baselwatch.com', 'UAE', 'Store'),
(11, 'Basel Watch Co.(Br.)', 'Lulu Express-Mazyad Mall, Abu Dhabi', '', '', 'info@baselwatch.com', 'UAE', 'Store'),
(12, 'Time Square Est.', 'P O Box 29951, Office 5, 8th Floor, Al Rashed Complex, Fahad Al Saleem Street, Safat – 13106, Kuwait', '', '', '', 'Kuwait', 'Store'),
(13, 'Sunwatch Distribution', '11, Rue Dayet Aoua, Appt. 9, Agdal, Rabat', '', '', '', 'Morocco', 'Store'),
(14, 'Basel Watch Co. L.L.C', 'Dubai', '+971 4 326 1818', '+971 4 392 6795', 'info@baselwatch.com', 'UAE', 'Service'),
(15, 'Classic Trading Centre WLL', 'P O Box 1492, Round about Bab-Al-Bahrain, Manama', '', '', 'clasicwatches@gmail.com', 'Bahrain', 'Service'),
(16, 'Asad Muscat Trading', 'Ruwi High Street, Muscat', '+968 9 231 7236', '', 'omnsales@baselwatch.com', 'Oman', 'Service'),
(17, 'Nasayem Al-Khaleej Trading Est.', 'P. O. Box 30697, Al-Khobar 31952, K.S.A.', '+966 3 898 4463 ', '', '', 'KSA', 'Service'),
(18, 'Al-Basel Watches', 'Shop No. 28, Building 14, Barwa Village, Doha - Qatar', '+974 4456 7070', '', 'qtrsales@baselwatch.com', 'Qatar', 'Service'),
(19, 'Chronosoft Pte. Ltd.', '05-15 Henderson Building, Singapore – 159554', '(65) 6339 3700', '', '', 'Singapore', 'Service'),
(20, 'Basel Watch Co.', '7009 Brockton Ct, Rowlett, TX 75089', '', '', '', 'USA', 'Service'),
(21, 'Time Square Est.', 'P. O. Box 29951 Safat, 13106 - Kuwait', '+965 97962344 – 97774075', '', '', 'Kuwait', 'Service'),
(22, 'Arikan Ithalat Saat San.Ve Dis Tic.Ltd Sti.', 'Tahtakale Cad No.:69(34460), Eminonu Istanbul,Turkey\r\n', '+90 212 5209203', '', '', 'Turkey', 'Service'),
(23, 'Basel Watch House', 'Basel Plaza, KSEB Junction, Kattappana, Idukki, Kerala', '+91 8086 904811', '', 'bwh@baselwatch.com', 'India', 'Service'),
(24, 'Basel Watch House', 'Shop No. F-10, 1st Floor, Capitol Mall, Kannur-2, Kerala', '0497-2762000', '', 'bwh@baselwatch.com', 'India', 'Service'),
(25, 'Rainbow Arcade', 'No. 21-23 , Shop No.G23, Sri Thayangaraya Road, Pondy Bazaar, T- Nagar, Chennai – 600 017, Tamil Nadu', '', '', '', 'India', 'Service'),
(26, 'Anglo-Swiss Watch Co.', '6, B.B.D. Bag (East), Kolkata - 700 001, West Bengal', '91 20 263 43685  91 40 278 46251', '', '', 'India', 'Service'),
(27, 'Ramesh Watch Co.', '2, 142, P.G. Road, Yellammagutta, (Satguru Building), Secunderabad H.O., Hyderabad 500003, Andhra Pradesh', '91 40 2784 6251', '', '', 'India', 'Service'),
(28, 'Kismet Sales & Service', 'Opp Majestic Theater, Bangalore - 560 009, Karnataka', '91 80 2238 6917', '', '', 'India', 'Service'),
(29, 'Nagpals', '208, Rangoli Time Complex, Dr.Ambedkar Road, Shop #38, Ground Floor, Parel E, Mumbai 400 012, Maharashtra', '91 22 2413 0693', '', '', 'India', 'Service');

-- --------------------------------------------------------

--
-- Table structure for table `bw_timeline`
--

CREATE TABLE `bw_timeline` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `description` text,
  `excerpt` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_timeline`
--

INSERT INTO `bw_timeline` (`id`, `image`, `year`, `description`, `excerpt`) VALUES
(1, 'aboutus18.png', 2006, '', 'Lorem ipsum dolor sit amet.'),
(2, NULL, 2007, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius perferendis vitae, facere accusantium magni, explicabo mollitia quidem odio autem, iste optio? Consequuntur ratione dolorum velit maiores quam odit odio suscipit.', ''),
(3, NULL, 2008, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius perferendis vitae, facere accusantium magni, explicabo mollitia quidem odio autem, iste optio? Consequuntur ratione dolorum velit maiores quam odit odio suscipit.', ''),
(4, 'aboutus19.png', 2009, '', 'Lorem ipsum dolor sit amet.'),
(5, NULL, 2010, 'Lorem dolor sit amet, consectetur adipisicing elit. Eius perferendis vitae, facere accusantium magni, explicabo mollitia quidem odio autem, iste optio? Consequuntur ratione dolorum velit maiores quam odit odio suscipit.', ''),
(6, NULL, 2020, 'Lorem dolor sit amet, consectetur adipisicing elit. Eius perferendis vitae, facere accusantium magni, explicabo mollitia quidem odio autem, iste optio? Consequuntur ratione dolorum velit maiores quam odit odio suscipit.', '');

-- --------------------------------------------------------

--
-- Table structure for table `bw_users`
--

CREATE TABLE `bw_users` (
  `ID` int(11) UNSIGNED NOT NULL,
  `uemail` varchar(255) DEFAULT NULL,
  `upass` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `ustatus` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_users`
--

INSERT INTO `bw_users` (`ID`, `uemail`, `upass`, `role`, `ustatus`) VALUES
(1, 'pavanshahu@redbaton.co', '9a31808a000ce031e8f983ba779bb896', 'admin', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bw_enquiry`
--
ALTER TABLE `bw_enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bw_newsletter`
--
ALTER TABLE `bw_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bw_offers`
--
ALTER TABLE `bw_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bw_pages`
--
ALTER TABLE `bw_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bw_popup`
--
ALTER TABLE `bw_popup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bw_products`
--
ALTER TABLE `bw_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bw_product_categories`
--
ALTER TABLE `bw_product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bw_settings`
--
ALTER TABLE `bw_settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `bw_socialfeed`
--
ALTER TABLE `bw_socialfeed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bw_stores`
--
ALTER TABLE `bw_stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bw_timeline`
--
ALTER TABLE `bw_timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bw_users`
--
ALTER TABLE `bw_users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bw_enquiry`
--
ALTER TABLE `bw_enquiry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bw_newsletter`
--
ALTER TABLE `bw_newsletter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `bw_offers`
--
ALTER TABLE `bw_offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `bw_pages`
--
ALTER TABLE `bw_pages`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bw_popup`
--
ALTER TABLE `bw_popup`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bw_products`
--
ALTER TABLE `bw_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `bw_product_categories`
--
ALTER TABLE `bw_product_categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `bw_settings`
--
ALTER TABLE `bw_settings`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `bw_socialfeed`
--
ALTER TABLE `bw_socialfeed`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bw_stores`
--
ALTER TABLE `bw_stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `bw_timeline`
--
ALTER TABLE `bw_timeline`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bw_users`
--
ALTER TABLE `bw_users`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
